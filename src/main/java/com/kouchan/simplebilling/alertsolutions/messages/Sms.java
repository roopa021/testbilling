package com.kouchan.simplebilling.alertsolutions.messages;

public class Sms {

	private String message;

	private String to;

	private String sender;

	private String custom;

	public String getMessage() {
		return message;
	}

	public void setMessage(final String message) {
		this.message = message;
	}

	public String getTo() {
		return to;
	}

	public void setTo(final String to) {
		this.to = to;
	}

	public String getCustom() {
		return custom;
	}

	public void setCustom(final String custom) {
		this.custom = custom;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(final String sender) {
		this.sender = sender;
	}

	@Override
	public String toString() {
		return "ClassPojo [message = " + message + ", to = " + to + ", custom = " + custom + "]";
	}

}
