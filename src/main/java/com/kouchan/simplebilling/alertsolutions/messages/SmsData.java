package com.kouchan.simplebilling.alertsolutions.messages;

import java.util.List;

public class SmsData {

	private List<Sms> sms;

	public List<Sms> getSms() {
		return sms;
	}

	public void setSms(final List<Sms> listOfSms) {
		this.sms = listOfSms;
	}

	@Override
	public String toString() {
		return "ClassPojo [sms = " + sms + "]";
	}

}
