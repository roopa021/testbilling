package com.kouchan.simplebilling.constant;

/**
 * @author satish s k
 *
 */
public class CommonConstants {
	public static final String ALL_FIELDS_REQUIRED = "All fields are required";
	public static final String FIRST_NAME_EMPTY = "First name cannot be empty";
	public static final String EMAIL_EMPTY = "Email cannot be empty";
	public static final String MOBILE_NUMBER_EMPTY = "Mobile number cannot be empty";
	public static final String PASSWORD_EMPTY = "PAssword cannot be empty";
}
