package com.kouchan.simplebilling.constant;

public interface SMSCONSTANTS {

	String REGISTRATION_MESSAGE = "registration-message";

	String MERCHANT_REGISTRATION = "merchant-registration";

	String REGISTRATION_OTP = "registration_otp";

	String VISIT_MSG = "visit_msg";

}
