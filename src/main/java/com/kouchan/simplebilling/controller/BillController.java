package com.kouchan.simplebilling.controller;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kouchan.simplebilling.exception.SimplebillingException;
import com.kouchan.simplebilling.request.BillRequest;
import com.kouchan.simplebilling.response.SuccessResponse;

@RestController
public class BillController {
	
	
	public BillController()
	{
		System.out.println("BillController()");
	}


	
	@PostMapping
    @RequestMapping(value = "/generate/bill")
    public ResponseEntity<SuccessResponse> getBill(
            @RequestBody @Valid BillRequest billRequest, BindingResult bindingresult)
            throws SimplebillingException {
        if (bindingresult.hasErrors()) {
            throw new SimplebillingException(bindingresult);
        }
      return null;
    }
	
	protected SuccessResponse getSuccessResponse(final Object data, final String message) {
        final SuccessResponse sucessrespone = new SuccessResponse();
        sucessrespone.setStatus("Sucesss");
        sucessrespone.setData(data);
        sucessrespone.setMessage(message);
        return sucessrespone;
    
	
}

	 
	
	
}
