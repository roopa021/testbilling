package com.kouchan.simplebilling.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kouchan.simplebilling.constant.WebConstants;
import com.kouchan.simplebilling.model.ChangePasswordModel;
import com.kouchan.simplebilling.model.CustomerModel;
import com.kouchan.simplebilling.model.DeliveryBoyModel;
import com.kouchan.simplebilling.model.MerchantModel;
import com.kouchan.simplebilling.response.CommonResponse;
import com.kouchan.simplebilling.response.DeliveryBoyResponse;
import com.kouchan.simplebilling.response.MerchantResponse;
import com.kouchan.simplebilling.service.CustomerService;
import com.kouchan.simplebilling.service.DeliveryBoyService;
import com.kouchan.simplebilling.service.MerchantService;
import com.kouchan.simplebilling.utils.EncryptDescrypt;
import com.kouchan.simplebilling.utils.RestUtils;

@RestController
public class ChangePasswordController {

	private static final Logger LOG = LoggerFactory.getLogger(ChangePasswordController.class);
	
	@Autowired
	private MerchantService merchantService;

	@Autowired
	private CustomerService customerService;

	@Autowired
	private DeliveryBoyService deliveryBoyService;

	public ChangePasswordController() {
		System.out.println("ChangePasswordController()");
	}
   
	//NOTE:Encryption and descryption concept commented 
	@RequestMapping(value = "merchant/changePassword", method = RequestMethod.PUT)
	public CommonResponse newPassword(@RequestBody ChangePasswordModel changePasswordModel) throws Exception {
		CommonResponse response = null;
		EncryptDescrypt e = new EncryptDescrypt();
		MerchantModel merchantModel = merchantService.getByMerchantId(changePasswordModel.getId());
		if (merchantModel == null) {
			response = RestUtils.wrapObjectForFailure(changePasswordModel, "302", WebConstants.MERCHANT_DOESNOT_EXIST);
		} else {
			//String presentPassword = e.decrypt(merchantModel.getPassword());
			String presentPassword = (merchantModel.getPassword());
			String enteredPassword = changePasswordModel.getOldPassword();
			if (presentPassword.equals(enteredPassword)) {
				MerchantModel merchModel = merchantService.updatePassword(merchantModel, changePasswordModel);
				MerchantResponse merchantResponse = new MerchantResponse();
				BeanUtils.copyProperties(merchModel, merchantResponse);
				//merchantResponse.setPassword(e.decrypt(merchModel.getPassword()));
				merchantResponse.setPassword((merchModel.getPassword()));
				response = RestUtils.wrapObjectForSuccess(merchantResponse);
			} else {
				response = RestUtils.wrapObjectForFailure(changePasswordModel, "402", WebConstants.PASSWORD);
			}
		}
		return response;
	}

	@RequestMapping(value = "customer/changePassword", method = RequestMethod.PUT)
	public CommonResponse newPasswordForCustomer(@RequestBody ChangePasswordModel changePasswordModel)
			throws Exception {
		CommonResponse response = null;
		EncryptDescrypt e = new EncryptDescrypt();
		CustomerModel customerModel = customerService.getByCustomerId(changePasswordModel.getCustomer_id());
		if (customerModel == null) {
			response = RestUtils.wrapObjectForFailure(changePasswordModel, "302", WebConstants.CUSTOMER_DOESNOT_EXIST);
		} else {
			String presentPassword = customerModel.getPassword();
			String enteredPassword = changePasswordModel.getOldPassword();
			if (presentPassword.equals(enteredPassword)) {
				CustomerModel m = customerService.updatePassword(customerModel, changePasswordModel);
				//m.setPassword(e.decrypt(m.getPassword()));
				m.setPassword((m.getPassword()));
				// MerchantResponse merchantResponse = new MerchantResponse();
				// BeanUtils.copyProperties(m, merchantResponse);
				response = RestUtils.wrapObjectForSuccess(m);
			} else {
				response = RestUtils.wrapObjectForFailure(changePasswordModel, "402", WebConstants.PASSWORD);
			}
		}
		return response;
	}

	@RequestMapping(value = "deliveryBoy/changePassword", method = RequestMethod.PUT)
	public CommonResponse newPasswordDeliveryboy(@RequestBody ChangePasswordModel changePasswordModel)
			throws Exception {
		CommonResponse response = null;
		DeliveryBoyModel deliveryBoyModel = deliveryBoyService.getByDeliveryBoyId(changePasswordModel.getId());
		if (deliveryBoyModel == null) {
			response = RestUtils.wrapObjectForFailure(changePasswordModel, "302", WebConstants.MERCHANT_DOESNOT_EXIST);
		} else {
			String presentPassword = deliveryBoyModel.getPassword();
			String enteredPassword = changePasswordModel.getOldPassword();
			if (presentPassword.equals(enteredPassword)) {
				DeliveryBoyModel deliveryBoyModel1 = deliveryBoyService.updatePassword(deliveryBoyModel,
						changePasswordModel);
				DeliveryBoyResponse deliveryBoyResponse = new DeliveryBoyResponse();
				BeanUtils.copyProperties(deliveryBoyModel1, deliveryBoyResponse);
				response = RestUtils.wrapObjectForSuccess(deliveryBoyResponse);
			} else {
				response = RestUtils.wrapObjectForFailure(changePasswordModel, "402", WebConstants.PASSWORD);
			}
		}
		return response;
	}
}
