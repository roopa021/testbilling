package com.kouchan.simplebilling.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;



import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kouchan.simplebilling.constant.CommonConstants;
import com.kouchan.simplebilling.constant.WebConstants;
import com.kouchan.simplebilling.coredata.CodeMessage;
import com.kouchan.simplebilling.exception.BillingException;
import com.kouchan.simplebilling.exception.BusinessException;
import com.kouchan.simplebilling.exception.MerchantNotFoundException;
import com.kouchan.simplebilling.model.CustomerModel;
import com.kouchan.simplebilling.model.CustomerOrderDetails;
import com.kouchan.simplebilling.model.MerchantCustomerModel;
import com.kouchan.simplebilling.model.MerchantModel;
import com.kouchan.simplebilling.model.ProductListModel;
import com.kouchan.simplebilling.repository.MerchantRepository;
import com.kouchan.simplebilling.repository.ProductListRepository;
import com.kouchan.simplebilling.request.CustomerOrderDetailsVO;
import com.kouchan.simplebilling.request.CustomerVO;
import com.kouchan.simplebilling.request.MonthDatas;
import com.kouchan.simplebilling.response.CommonResponse;
import com.kouchan.simplebilling.response.SuccessResponse;
import com.kouchan.simplebilling.service.CustomerService;
import com.kouchan.simplebilling.service.MerchantCustomerService;
import com.kouchan.simplebilling.service.ProductListService;
import com.kouchan.simplebilling.utils.RestUtils;
import com.mysql.jdbc.StringUtils;

@RestController
public class CustomerController {

	@Autowired
	private CustomerService customerService;

	@Autowired
	private MerchantCustomerService merchantCustomerService;

	/*@Autowired
	private JavaMailSender javaMailSender;*/

	Calendar calendar = Calendar.getInstance();

	@Autowired
	ProductListService productListService;

	@Autowired
	private ProductListRepository r;

	String[] strMonths = new String[] { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov",
			"Dec" };

	String pass, mob;
	String msg = "Sucess";

	List<ProductListModel> productListModelclass;
	
	
	@Autowired
	private MerchantRepository merchantRepository;
	byte inactiveStatus = 1;


	private static final Logger LOG = LoggerFactory.getLogger(CustomerController.class);
	
	//NOTE: Wrong Code New code is Customer Reg Controller
	@RequestMapping(value = "/customer/register", method = RequestMethod.POST)
	public CommonResponse registerCustomer(@RequestBody CustomerVO customerVO) throws Exception {
		CustomerModel customerModel;
		try {
			int id=customerVO.getMerchantId();
			/*MerchantModel merchatExit = merchantRepository.findOne(id);
			if (merchatExit == null || merchatExit.getIsDeleted() == inactiveStatus) {
				final List<CodeMessage> list = new ArrayList<>();
				list.add(new CodeMessage("302", "Merchant Does't Exit"));
				throw new MerchantNotFoundException(list);
			}*/
			customerModel = customerService.isCustomerExist(customerVO.getMobileNumber());
			//String Mobile=customerModel.getMobileNumber();
			if (customerModel == null) {
				customerModel = customerService.saveNewCustomer(customerVO);
				mob = customerModel.getMobileNumber();
				pass = customerModel.getPassword();
				// sendEmail(mob,pass);
			}
			
			MerchantModel merchantModel = new MerchantModel();
			merchantModel.setId(customerVO.getMerchantId());
			MerchantCustomerModel merchantCustomerModel = new MerchantCustomerModel();
			merchantCustomerModel.setAddress(customerVO.getAddress());
			merchantCustomerModel.setCustomerModel(customerModel);
			merchantCustomerModel.setMerchantModel(merchantModel);
			merchantCustomerService.save(merchantCustomerModel);
			ProductListModel productListModel = new ProductListModel();
			// int p1= productListModel.getProduct_id();

			CustomerOrderDetails customerOrderDetails = new CustomerOrderDetails();
			customerOrderDetails.setMerchantModel(merchantModel);
			customerOrderDetails.setCustomerModel(customerModel);

			ProductListModel pm = r.findby(customerVO.getMerchantId(), customerVO.getSubproduct());
			int p2 = pm.getProduct_id();
			ProductListModel pid = r.findOne(p2);
			customerOrderDetails.setProductListModel(pid);
			productListModel.setProduct_id(pm.getProduct_id());

			customerOrderDetails.setProductListModel(productListModel);

			customerOrderDetails.setRate(pm.getRate());
			// productListModel.setProductName(customerOrderDetails.getProductListModel().getSubProduct());
			// productListModel.setRate(customerOrderDetails.getProductListModel().getRate());

			// customerOrderDetails.setProductListModel(productListModel); 
			// customerOrderDetails.setRate(productListModel.getRate());
			customerOrderDetails.setMonthYear((calendar.get(Calendar.MONTH) + 1) + "-" + calendar.get(Calendar.YEAR));
			customerOrderDetails
					.setMonthYear(strMonths[calendar.get(Calendar.MONTH)] + "-" + calendar.get(Calendar.YEAR));

			customerOrderDetails.setMonthYear(getOrderDetailsData());
			customerOrderDetails.setMonth(strMonths[calendar.get(Calendar.MONTH)] + "-" + calendar.get(Calendar.YEAR));

			customerOrderDetails = customerService.customerOrderDetailssave(customerOrderDetails);
			return RestUtils.wrapObjectForSuccess(customerOrderDetails);

		} catch (Exception exception) {
			return RestUtils.wrapObjectForFailure("error", "500", exception.getMessage());
		}
	}

	
	
	/* Update Customer NAME and NUMBER API */

	@PutMapping
	@RequestMapping(value = "/customer/update/{id}")
	public ResponseEntity<CustomerModel> updateMerchant(@PathVariable("id") int id,
			@RequestBody CustomerModel customerModel) throws MerchantNotFoundException {
		CustomerModel customer = customerService.getByCustomerId(id);
		if (customer == null) {
			throw new MerchantNotFoundException();
		}
		CustomerModel customerModeltype = customerService.updateCustomerNameAndNumber(customerModel, id);
		return new ResponseEntity<CustomerModel>(customerModeltype, new HttpHeaders(), HttpStatus.OK);
	}

	/* TO DO List Delivery schedule of customer by delivery boy */
	@PostMapping
	@RequestMapping(value = "/customerorderdetails/update")
	public CommonResponse updatedelivarydetails(@RequestBody CustomerOrderDetailsVO customerOrderDetailsVO)
			throws Exception {
		CustomerOrderDetails customer = customerService.updateOrderDetails(customerOrderDetailsVO.getOrderId(), customerOrderDetailsVO.getDate(),customerOrderDetailsVO.getStatus());
		if (customer == null) {
			final List<CodeMessage> list = new ArrayList<>();
			list.add(new CodeMessage("302", " InValid Data"));
			throw new BillingException(list);

		}

		return RestUtils.wrapObjectForSuccess(customer);

	}

	/* TO DO List List of merchant and List of product by category wise. */
	//Wrong code
	@GetMapping
	@RequestMapping(value = "/getproductlistbymerchant/{id}")
	public ResponseEntity<List<String>> listAllProducts(@PathVariable("id") int id) {
		List<String> subproduct = new ArrayList<>();
		List<ProductListModel> merchants = productListService.findAllMerchantproducts(id);
		if (merchants.isEmpty()) {
			return new ResponseEntity<>(subproduct, HttpStatus.NOT_FOUND);
		}

		for (ProductListModel i : merchants) {
			subproduct.add(i.getSubProduct());

		}
		return new ResponseEntity<>(subproduct, HttpStatus.OK);

	}

	@GetMapping
	@RequestMapping(value = "/getCustomerDetails/{id}")
	public ResponseEntity<SuccessResponse> getMerchantDetails(
			@PathVariable("id") int id/* , MerchantCustomerModel merchantCustomerModel */) {
		final Set<CustomerModel> merchant = merchantCustomerService.getMerchnatDetails(id);
		// MerchantCustomerResponse merchantResponse = new MerchantCustomerResponse();
		// BeanUtils.copyProperties(merchant, merchantResponse);
		return new ResponseEntity<SuccessResponse>(getSuccessResponse(merchant, "All Customer Details of Merchat !!"),
				HttpStatus.OK);
	}

	/* Login Api Customer */
	@RequestMapping(value = "customer/login", method = RequestMethod.POST)
	public CommonResponse login(@RequestBody CustomerVO customerVO) {
		CommonResponse response = null;

		Map<String, Object> map = validateLogin(customerVO);
		if (!map.isEmpty()) {
			response = RestUtils.wrapObjectForFailure(map, "validation error", WebConstants.WEB_RESPONSE_ERORR);
			LOG.error("Login Validation missing");
		}
		try {
			CustomerModel customVO = customerService.login(customerVO);

			if (customVO == null) {
				return response = RestUtils.wrapObjectForFailure(customVO, "302", WebConstants.CUSTOMER_DOESNOT_EXIST);
			}

			CustomerVO customer = new CustomerVO();
			BeanUtils.copyProperties(customVO, customer);
			response = RestUtils.wrapObjectForSuccess(customer);
			LOG.info("Logged in successfully " + customVO.getMobileNumber());

		} catch (BusinessException be) {
			response = RestUtils.wrapObjectForFailure(null, be.getErrorCode(), be.getErrorMsg());
		} catch (Exception e) {
			response = RestUtils.wrapObjectForFailure(null, WebConstants.WEB_RESPONSE_ERROR,
					WebConstants.INTERNAL_SERVER_ERROR_MESSAGE);
			e.printStackTrace();
		}
		return response;
	}

	/* Validate a login customer */
	private Map<String, Object> validateLogin(CustomerVO customerVO) {
		Map<String, Object> map = new HashMap<>();
		if (customerVO == null) {
			map.put("All", CommonConstants.ALL_FIELDS_REQUIRED);
		} else {
			if (StringUtils.isNullOrEmpty(customerVO.getMobileNumber())
					|| StringUtils.isEmptyOrWhitespaceOnly(customerVO.getMobileNumber())) {
				map.put("mobileNumber", CommonConstants.MOBILE_NUMBER_EMPTY);
			}
			if (StringUtils.isNullOrEmpty(customerVO.getPassword())
					|| StringUtils.isEmptyOrWhitespaceOnly(customerVO.getPassword())) {
				map.put("password", CommonConstants.PASSWORD_EMPTY);
			}
		}
		return map;
	}

	/* Genatare otp number of customer */
	@RequestMapping(value = "customer/generateOtp", method = RequestMethod.POST)
	public int generateOtp(@RequestParam String mobile) {
		CommonResponse response = null;
		int generated = customerService.generateOtp(mobile);
		if (generated != 0) {
			response = RestUtils.wrapObjectForSuccess("success");
		}

		else {
			response = RestUtils.wrapObjectForFailure("Otp not generated, invalid user", "error",
					WebConstants.WEB_RESPONSE_ERORR);
		}
		return generated;
	}

	/* Validate otp number of customer */
	@RequestMapping(value = "customer/validateOtp", method = RequestMethod.POST)
	public CommonResponse validateOtp(@RequestParam String mobile, @RequestParam String otp) {
		CommonResponse response = null;
		CustomerVO customerVO = customerService.validateOtp(mobile, otp);
		if (customerVO != null)
			response = RestUtils.wrapObjectForSuccess(customerVO);
		else
			response = RestUtils.wrapObjectForFailure("invalid otp", "error", WebConstants.WEB_RESPONSE_ERORR);
		return response;
	}

/*	void sendEmail(String mobile, String Password) {
		Password = pass;
		mobile = mob;

		SimpleMailMessage msg = new SimpleMailMessage();
		 FileSystemResource file = new FileSystemResource(new File(fileToAttach));
		 MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
         helper.addAttachment("logo.jpg", file);
		msg.setTo("satish@kouchanindia.com");
		msg.setSubject("Simple Billing");
		msg.setText("Wel-Come to Kouchan simple Billing Application" + "\n" + "Login ID=" + mobile + "\n"
				+ "Password is=" + Password);
		
		javaMailSender.send(msg);
	}
*/
	public String getOrderDetailsData() {
		Gson gson = new Gson();
		List<MonthDatas> dayList = new ArrayList<MonthDatas>();
		Calendar c = Calendar.getInstance();
		int monthMaxDays = c.getActualMaximum(Calendar.DAY_OF_MONTH);
		for (int i = 1; i <= monthMaxDays; i++) {
			MonthDatas monthDatas = new MonthDatas();
			monthDatas.setDay(i + "-" + (c.get(Calendar.MONTH) + 1) + "-" + c.get(Calendar.YEAR));
			monthDatas.setQuantity("1");
			monthDatas.setStatus("0");
			dayList.add(monthDatas);
		}
		return gson.toJson(dayList);
	}

	/* Success Response message . */
	protected SuccessResponse getSuccessResponse(final Object data, final String message) {
		final SuccessResponse sucessrespone = new SuccessResponse();
		sucessrespone.setStatus("Sucesss");
		sucessrespone.setData(data);
		sucessrespone.setMessage(message);
		return sucessrespone;
	}

}
