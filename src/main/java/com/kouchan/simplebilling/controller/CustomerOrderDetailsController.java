package com.kouchan.simplebilling.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kouchan.simplebilling.model.CustomerOrderDetails;
import com.kouchan.simplebilling.request.InvoiceVO;
import com.kouchan.simplebilling.response.CustomerOrderDetailsResponse;
import com.kouchan.simplebilling.response.SuccessResponse;
import com.kouchan.simplebilling.service.CustomerOrderDetailsService;

@RestController
public class CustomerOrderDetailsController {

	
	public CustomerOrderDetailsController() {
		System.out.println("CustomerOrderDetailsController()");
	}
	
	@Autowired
	private CustomerOrderDetailsService customerOrderDetailsService;

	/*NOTE: status  -- 0 -- upcoming delivery
	                -- 1 -- Delivered
	                -- 2 -- canceled by customer
*/
    @PostMapping
	@RequestMapping(value = "/getOrderDetails")
	public ResponseEntity<SuccessResponse> getMerchantDetails(@RequestBody InvoiceVO invoiceVO) {
		CustomerOrderDetails c=customerOrderDetailsService.getOrderSchedle(invoiceVO.getMerchantId(),invoiceVO.getCustomerId(),invoiceVO.getBillMonth());
		return new ResponseEntity<SuccessResponse>(getSuccessResponse(c, " OrderDetails Of The Customer !!"),
				HttpStatus.OK);
	}

    @PostMapping
   	@RequestMapping(value = "/getUpcomingOrderDetails")
    public ResponseEntity<SuccessResponse>upcomingDelivery(@RequestBody InvoiceVO invoiceVO)
    {
    	CustomerOrderDetailsResponse upComingOrderDetials=customerOrderDetailsService.upcomingOrderDetails(invoiceVO.getMerchantId(),invoiceVO.getCustomerId(),invoiceVO.getBillMonth());
    	return new ResponseEntity<SuccessResponse>(getSuccessResponse(upComingOrderDetials, "UpComing OrderDetails Of The Customer !!"),
				HttpStatus.OK);
    }
    
    

    @PostMapping
   	@RequestMapping(value = "/getDeliveredOrderDetails")
    public ResponseEntity<SuccessResponse>delivered(@RequestBody InvoiceVO invoiceVO)
    {
    	CustomerOrderDetailsResponse upComingOrderDetials=customerOrderDetailsService.delivredOrderDetails(invoiceVO.getMerchantId(),invoiceVO.getCustomerId(),invoiceVO.getBillMonth());
    	return new ResponseEntity<SuccessResponse>(getSuccessResponse(upComingOrderDetials, " Delivered OrderDetails Of The Customer !!"),
				HttpStatus.OK);
    }
    

    @PostMapping
   	@RequestMapping(value = "/getCanceledOrderDetails")
    public ResponseEntity<SuccessResponse>Cancled(@RequestBody InvoiceVO invoiceVO)
    {
    	CustomerOrderDetailsResponse upComingOrderDetials=customerOrderDetailsService.canceledOrderDetails(invoiceVO.getMerchantId(),invoiceVO.getCustomerId(),invoiceVO.getBillMonth());
    	return new ResponseEntity<SuccessResponse>(getSuccessResponse(upComingOrderDetials, "Canceled OrderDetails Of The Customer !!"),
				HttpStatus.OK);
    }
    
    
    
	protected SuccessResponse getSuccessResponse(final Object data, final String message) {
		final SuccessResponse sucessrespone = new SuccessResponse();
		sucessrespone.setStatus("Sucesss");
		sucessrespone.setData(data);
		sucessrespone.setMessage(message);
		return sucessrespone;

	}

}
