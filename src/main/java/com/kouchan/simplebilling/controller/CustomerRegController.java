package com.kouchan.simplebilling.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.kouchan.simplebilling.model.CustomerModel;
import com.kouchan.simplebilling.model.CustomerOrderDetails;
import com.kouchan.simplebilling.model.MerchantCustomerModel;
import com.kouchan.simplebilling.model.MerchantModel;
import com.kouchan.simplebilling.model.ProductListModel;
import com.kouchan.simplebilling.repository.ProductListRepository;
import com.kouchan.simplebilling.request.CustomerVO;
import com.kouchan.simplebilling.request.MonthDatas;
import com.kouchan.simplebilling.response.SuccessResponse;
import com.kouchan.simplebilling.service.CustomerService;
import com.kouchan.simplebilling.service.MerchantCustomerService;
import com.kouchan.simplebilling.service.ProductListService;

@RestController
public class CustomerRegController {

	public CustomerRegController() 
	{
		System.out.println("customerRegController");
	}
	
	@Autowired
	private CustomerService customerService;

	@Autowired
	private MerchantCustomerService merchantCustomerService;
	
	Calendar calendar = Calendar.getInstance();
	
	@Autowired
	private JavaMailSender javaMailSender;

	@Autowired
	ProductListService productListService;
	
	@Autowired
	private ProductListRepository productListRepository;
	
	String pass, mob;
	String msg = "Sucess";


	
	String[] strMonths = new String[] { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov",
	"Dec" };

	
	@PostMapping(value = "customerRegister")
	public ResponseEntity<SuccessResponse> customerReg(@RequestBody CustomerVO customerVO) throws Exception {
       CustomerModel customerModel;
       customerModel = customerService.isCustomerExist(customerVO.getMobileNumber());
       //String mobile=customerModel.getMobileNumber();
       if (customerModel == null) {
			customerModel = customerService.saveNewCustomer(customerVO);
			mob = customerModel.getMobileNumber();
			pass = customerModel.getPassword();
			// sendEmail(mob,pass);
			// sendEmail(mob,pass);
		}
       
		MerchantModel merchantModel = new MerchantModel();
		merchantModel.setId(customerVO.getMerchantId());
		MerchantCustomerModel merchantCustomerModel = new MerchantCustomerModel();
		merchantCustomerModel.setAddress(customerVO.getAddress());
		merchantCustomerModel.setCustomerModel(customerModel);
		merchantCustomerModel.setMerchantModel(merchantModel);
		merchantCustomerService.save(merchantCustomerModel);
		ProductListModel productListModel = new ProductListModel();
		
		
		CustomerOrderDetails customerOrderDetails = new CustomerOrderDetails();
		customerOrderDetails.setMerchantModel(merchantModel);
		customerOrderDetails.setCustomerModel(customerModel);
        
		ProductListModel pm = productListRepository.findby(customerVO.getMerchantId(), customerVO.getSubproduct());
		int p2 = pm.getProduct_id();
		ProductListModel pid = productListRepository.findOne(p2);
		customerOrderDetails.setProductListModel(pid);
		productListModel.setProduct_id(pm.getProduct_id());

		customerOrderDetails.setProductListModel(productListModel);
		customerOrderDetails.setRate(pm.getRate());
		
		customerOrderDetails.setMonthYear((calendar.get(Calendar.MONTH) + 1) + "-" + calendar.get(Calendar.YEAR));
		customerOrderDetails
				.setMonthYear(strMonths[calendar.get(Calendar.MONTH)] + "-" + calendar.get(Calendar.YEAR));

		customerOrderDetails.setMonthYear(getOrderDetailsData());
		customerOrderDetails.setMonth(strMonths[calendar.get(Calendar.MONTH)] + "-" + calendar.get(Calendar.YEAR));

		customerOrderDetails = customerService.customerOrderDetailssave(customerOrderDetails);
		
		return new ResponseEntity<SuccessResponse>(getSuccessResponse(customerOrderDetails, "Customer Resg Sucessfully"),
				HttpStatus.OK);

	}

	public String getOrderDetailsData() {
		Gson gson = new Gson();
		List<MonthDatas> dayList = new ArrayList<MonthDatas>();
		Calendar c = Calendar.getInstance();
		int monthMaxDays = c.getActualMaximum(Calendar.DAY_OF_MONTH);
		for (int i = 1; i <= monthMaxDays; i++) {
			MonthDatas monthDatas = new MonthDatas();
			monthDatas.setDay(i + "-" + (c.get(Calendar.MONTH) + 1) + "-" + c.get(Calendar.YEAR));
			monthDatas.setQuantity("1");
			monthDatas.setStatus("0");
			dayList.add(monthDatas);
		}
		return gson.toJson(dayList);
	}

	void sendEmail(String mobile, String Password) {
		Password = pass;
		mobile = mob;

		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo("satish@kouchanindia.com");
		msg.setSubject("Simple Billing");
		msg.setText("Wel-Come to Kouchan simple Billing Application" + "\n" + "Login ID=" + mobile + "\n"
				+ "Password is=" + Password);
		javaMailSender.send(msg);
	}
	
	
	protected SuccessResponse getSuccessResponse(final Object data, final String message) {
		final SuccessResponse sucessrespone = new SuccessResponse();
		sucessrespone.setStatus("Sucesss");
		sucessrespone.setData(data);
		sucessrespone.setMessage(message);
		return sucessrespone;
	}

	

}
