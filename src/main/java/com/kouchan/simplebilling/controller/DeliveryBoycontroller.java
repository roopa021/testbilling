package com.kouchan.simplebilling.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kouchan.simplebilling.constant.CommonConstants;
import com.kouchan.simplebilling.constant.WebConstants;
import com.kouchan.simplebilling.coredata.CodeMessage;
import com.kouchan.simplebilling.exception.BusinessException;
import com.kouchan.simplebilling.exception.CustomerDetailssNotFoundException;
import com.kouchan.simplebilling.exception.ErrorCodes;
import com.kouchan.simplebilling.exception.MerchantNotFoundException;
import com.kouchan.simplebilling.exception.SimplebillingException;
import com.kouchan.simplebilling.model.DeliveryBoyModel;
import com.kouchan.simplebilling.model.MerchantModel;
import com.kouchan.simplebilling.repository.MerchantRepository;
import com.kouchan.simplebilling.request.DeliveryBoyRegistrationRequest;
import com.kouchan.simplebilling.response.CommonResponse;
import com.kouchan.simplebilling.response.DeleteResponse;
import com.kouchan.simplebilling.response.DeliveryBoyResponse;
import com.kouchan.simplebilling.response.GetDeliveryBoyRepsonse;
import com.kouchan.simplebilling.response.SuccessResponse;
import com.kouchan.simplebilling.service.DeliveryBoyService;
import com.kouchan.simplebilling.utils.RestUtils;
import com.mysql.jdbc.StringUtils;

@RestController
public class DeliveryBoycontroller {

	public DeliveryBoycontroller() {
		System.out.println("DeliveryBoycontroller()");
	}

	private static final Logger LOG = LoggerFactory.getLogger(DeliveryBoycontroller.class);

	@Autowired
	private DeliveryBoyService deliveryBoyService;

	@Autowired
	private MerchantRepository merchantRepository;

	@RequestMapping(value = "/deliveryBoy/login", method = RequestMethod.POST)
	public CommonResponse login(@RequestBody DeliveryBoyRegistrationRequest deliveryboyRegrequest) {
		CommonResponse response = null;
		Map<String, Object> map = validateLogin(deliveryboyRegrequest);
		if (!map.isEmpty()) {
			response = RestUtils.wrapObjectForFailure(map, "validation error", WebConstants.WEB_RESPONSE_ERORR);

			LOG.error("Login Validation missing");
		}
		try {
			DeliveryBoyModel deliveryReg = deliveryBoyService.deliveryBoylogin(deliveryboyRegrequest);
			if (deliveryReg == null) {
				throw new BusinessException(ErrorCodes.MOBNOTEXIST.name(), ErrorCodes.MOBNOTEXIST.value());
			}
			DeliveryBoyResponse deliveryBoyResponse = new DeliveryBoyResponse();
			BeanUtils.copyProperties(deliveryReg, deliveryBoyResponse);
			response = RestUtils.wrapObjectForSuccess(deliveryBoyResponse);
			LOG.info("Logged in successfully " + deliveryBoyResponse.getMobileNumber());
		} catch (BusinessException be) {
			response = RestUtils.wrapObjectForFailure(null, be.getErrorCode(), be.getErrorMsg());
		} catch (Exception e) {
			response = RestUtils.wrapObjectForFailure(null, WebConstants.WEB_RESPONSE_ERROR,
					WebConstants.INTERNAL_SERVER_ERROR_MESSAGE);
			e.printStackTrace();
		}
		return response;
	}

	// NOTE:Group Concept is commented
	@PostMapping
	@RequestMapping(value = "/createDeliveryBoy")
	public ResponseEntity<SuccessResponse> createDeliveryBoy(
			@RequestBody @Valid DeliveryBoyRegistrationRequest deliveryBoyRegistrationRequest,
			BindingResult bindingresult) throws SimplebillingException {
		if (bindingresult.hasErrors()) {
			throw new SimplebillingException(bindingresult);
		}
		MerchantModel m1 = merchantRepository.findOne(deliveryBoyRegistrationRequest.getMerchantId());
		// GroupModel g1=g.findOne(deliveryBoyRegistrationRequest.getGroupId());
		if (m1 == null /* || g1==null */) {
			final List<CodeMessage> list = new ArrayList<>();
			list.add(new CodeMessage("302", "Merchant Does't Exit"));
			throw new MerchantNotFoundException(list);
		}
		// long groupId = deliveryBoyRegistrationRequest.getGroupId();
		MerchantModel merchnatModel = new MerchantModel();
		// GroupModel groupModel = new GroupModel();

		merchnatModel.setId(deliveryBoyRegistrationRequest.getMerchantId());
		// groupModel.setGroupId(deliveryBoyRegistrationRequest.getGroupId());

		DeliveryBoyModel deliveryBoy = new DeliveryBoyModel();
		BeanUtils.copyProperties(deliveryBoyRegistrationRequest, deliveryBoy);
		deliveryBoy.setAddress(deliveryBoyRegistrationRequest.getAddress());
		deliveryBoy.setMerchantModel(merchnatModel);
		// deliveryBoy.setGroupModel(groupModel);
		DeliveryBoyModel createDeliveryBoy = deliveryBoyService.save(deliveryBoy, merchnatModel);
		return new ResponseEntity<SuccessResponse>(
				getSuccessResponse(createDeliveryBoy, "DeliveryBoy Registered Suessfully !!"), HttpStatus.OK);
	}

	@PutMapping
	@RequestMapping(value = "/deliveryBoy/update/{id}")
	public ResponseEntity<DeliveryBoyModel> updateMerchant(@PathVariable int id,
			@RequestBody DeliveryBoyModel deliveryBoy) throws SimplebillingException {
		DeliveryBoyModel delivery = deliveryBoyService.getByDeliveryBoyId(id);
		if (delivery == null) {
			throw new MerchantNotFoundException();
		}
		DeliveryBoyModel m = deliveryBoyService.update(deliveryBoy, id);
		return new ResponseEntity<DeliveryBoyModel>(m, new HttpHeaders(), HttpStatus.OK);
	}

	@RequestMapping(value = "/deleteDeliveryBoy/{Id}", method = RequestMethod.DELETE)
	public ResponseEntity<DeleteResponse> deleteDeliveryBoy(@PathVariable int Id) {

		final DeleteResponse DeliveryBoyResponse = deliveryBoyService.deleteDelivery(Id);
		return new ResponseEntity<DeleteResponse>(DeliveryBoyResponse, HttpStatus.OK);

	}

	@GetMapping
	@RequestMapping(value = "/getAllDeliveryBoys/details/{merchant_id}")
	public ResponseEntity<GetDeliveryBoyRepsonse> getDeliverBoyDetails(@PathVariable int merchant_id,
			DeliveryBoyRegistrationRequest deliveryBoyRegistrationRequest) throws CustomerDetailssNotFoundException {
		final GetDeliveryBoyRepsonse getAllDeliveryBoyDetails = deliveryBoyService.getAllDeliveryBoys(merchant_id,
				deliveryBoyRegistrationRequest);
		return new ResponseEntity<GetDeliveryBoyRepsonse>(getAllDeliveryBoyDetails, HttpStatus.OK);
	}

	@RequestMapping(value = "/deliveryBoy/Availability", method = RequestMethod.POST)
	public ResponseEntity<SuccessResponse> deliveryBoyAvaliablity(
			@RequestBody @Valid DeliveryBoyRegistrationRequest deliveryBoyRegistrationRequest,
			BindingResult bindingresult) {
		DeliveryBoyModel deliveryBoyStatus = deliveryBoyService.deliveryBoyAvaliablity(deliveryBoyRegistrationRequest);

		return new ResponseEntity<SuccessResponse>(getSuccessResponse(deliveryBoyStatus, "Success!!"), HttpStatus.OK);

	}

	private Map<String, Object> validateLogin(DeliveryBoyRegistrationRequest deliveryBoyRegrequest) {
		Map<String, Object> map = new HashMap<>();
		if (deliveryBoyRegrequest == null) {
			map.put("All", CommonConstants.ALL_FIELDS_REQUIRED);
		} else {
			if (StringUtils.isNullOrEmpty(deliveryBoyRegrequest.getMobileNumber())
					|| StringUtils.isEmptyOrWhitespaceOnly(deliveryBoyRegrequest.getMobileNumber())) {
				map.put("mobileNumber", CommonConstants.MOBILE_NUMBER_EMPTY);
			}
			if (StringUtils.isNullOrEmpty(deliveryBoyRegrequest.getPassword())
					|| StringUtils.isEmptyOrWhitespaceOnly(deliveryBoyRegrequest.getPassword())) {
				map.put("password", CommonConstants.PASSWORD_EMPTY);
			}

		}
		return map;
	}

	protected SuccessResponse getSuccessResponse(final Object data, final String message) {
		final SuccessResponse sucessrespone = new SuccessResponse();
		sucessrespone.setStatus("Sucesss");
		sucessrespone.setData(data);
		sucessrespone.setMessage(message);
		return sucessrespone;

	}
}
