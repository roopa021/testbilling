package com.kouchan.simplebilling.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kouchan.simplebilling.constant.WebConstants;
import com.kouchan.simplebilling.coredata.CodeMessage;
import com.kouchan.simplebilling.exception.MerchantNotFoundException;
import com.kouchan.simplebilling.model.ChangePasswordModel;
import com.kouchan.simplebilling.model.CustomerModel;
import com.kouchan.simplebilling.model.DeliveryBoyModel;
import com.kouchan.simplebilling.model.ForgotPasswordModel;
import com.kouchan.simplebilling.model.MerchantModel;
import com.kouchan.simplebilling.repository.CustomerRepository;
import com.kouchan.simplebilling.repository.DeliveryBoyRepository;
import com.kouchan.simplebilling.repository.ForgotPasswordRepository;
import com.kouchan.simplebilling.repository.MerchantRepository;
import com.kouchan.simplebilling.request.ForgotPasswordRequest;
import com.kouchan.simplebilling.response.CommonResponse;
import com.kouchan.simplebilling.response.MerchantResponse;
import com.kouchan.simplebilling.response.SuccessResponse;
import com.kouchan.simplebilling.service.CustomerService;
import com.kouchan.simplebilling.service.DeliveryBoyService;
import com.kouchan.simplebilling.service.ForgotPasswordService;
import com.kouchan.simplebilling.service.MerchantService;
import com.kouchan.simplebilling.service.SmsService;
import com.kouchan.simplebilling.utils.EncryptDescrypt;
import com.kouchan.simplebilling.utils.RestUtils;

@RestController
public class ForgotPasswordController {

	public ForgotPasswordController() {
		System.out.println("ForgotPasswordController()");
	}

	@Autowired
	private SmsService smsService;

	@Autowired
	private MerchantService merchantService;
	
	@Autowired
	private CustomerService customerService;
	
	@Autowired
	private DeliveryBoyService deliveryBoyService;

	@Autowired
	private ForgotPasswordService forgotPasswordService;

	@Autowired
	private ForgotPasswordRepository forgotPasswordRepository;
	
	@Autowired
	private MerchantRepository m;
	
	@Autowired
	private CustomerRepository c;
	
	@Autowired
	private DeliveryBoyRepository d;
	
	
	byte inactiveStatus = 1;

	@PostMapping(value = "/sendOtp")
	public ResponseEntity<SuccessResponse> sendOtp(@RequestBody ForgotPasswordRequest forgotPasswordRequest) {
		int otp = (int) (Math.random() * 9000) + 1000;
		ForgotPasswordModel forgotPasswordModel = new ForgotPasswordModel();
		forgotPasswordModel.setOtp(otp);
		BeanUtils.copyProperties(forgotPasswordRequest, forgotPasswordModel);
		ForgotPasswordModel forgotPassword = forgotPasswordService.saveOtp(forgotPasswordModel);
		// smsService.sendOtpToVisitor(forgotPasswordRequest.getMobileNumber(), otp);
		return new ResponseEntity<SuccessResponse>(getSuccessResponse(forgotPassword, "Otp Sent Sucessfully !!"),
				HttpStatus.OK);
	}

	@SuppressWarnings("unused")
	@RequestMapping(value = "/forgotPassword/validateOtp", method = RequestMethod.POST)
	public CommonResponse validateOtp(@RequestParam String mobile, @RequestParam int otp) {
		CommonResponse response = null;
		ForgotPasswordModel m = forgotPasswordRepository.findByMobileAndOtp(mobile, otp);
		if (m == null) {
			final List<CodeMessage> list = new ArrayList<>();
			list.add(new CodeMessage("302", "Invalid Credential"));
			throw new MerchantNotFoundException(list);
		}
		/*
		 * if (m.getIsDeleted() == inactiveStatus) { final List<CodeMessage> list = new
		 * ArrayList<>(); list.add(new CodeMessage("303", "Merchant Not Found")); throw
		 * new MerchantNotFoundException(list); }
		 */
		int verifyOtp = m.getOtp();
		if (verifyOtp == otp || m != null) {
			response = RestUtils.wrapObjectForOTP("OTP verfied Successfully ", "error", WebConstants.OTP);
		} else {
			response = RestUtils.wrapObjectForFailure("invalid otp", "error", WebConstants.WEB_RESPONSE_ERORR);
		}
		return response;
	}

	@RequestMapping(value = "merchant/resetPassword", method = RequestMethod.PUT)
	public CommonResponse newPasswordForMerchant(@RequestBody ForgotPasswordRequest forgotPasswordRequest) throws Exception {
		CommonResponse response = null;
		EncryptDescrypt e = new EncryptDescrypt();
		MerchantModel merchantModel = merchantService.getByMerchantId(forgotPasswordRequest.getMerchat_id());
		if (merchantModel == null) {
			response = RestUtils.wrapObjectForFailure(forgotPasswordRequest, "402", WebConstants.MERCHANT_DOESNOT_EXIST);

		} else {
			String presentPassword = forgotPasswordRequest.getNewPassword();
			String enteredPassword = forgotPasswordRequest.getConfirmPassword();
			if (presentPassword.equals(enteredPassword)) {
				MerchantModel merchModel = forgotPasswordService.updatePassword(merchantModel, forgotPasswordRequest);
				response = RestUtils.wrapObjectForSuccess(merchModel);
			}
			else 
			{
				response = RestUtils.wrapObjectForFailure(forgotPasswordRequest, "402", WebConstants.PASSWORD_MISMATCH);
			}
		}
		return response;
	}

	
	@RequestMapping(value = "customer/resetPassword", method = RequestMethod.PUT)
	public CommonResponse newPasswordForCustomer(@RequestBody ForgotPasswordRequest forgotPasswordRequest) throws Exception {
		CommonResponse response = null;
		EncryptDescrypt e = new EncryptDescrypt();
	CustomerModel merchantModel = customerService. getByCustomerId(forgotPasswordRequest.getCustomer_id());
		if (merchantModel == null) {
			response = RestUtils.wrapObjectForFailure(forgotPasswordRequest, "402", WebConstants.CUSTOMER_DOESNOT_EXIST);

		} else {
			String presentPassword = forgotPasswordRequest.getNewPassword();
			String enteredPassword = forgotPasswordRequest.getConfirmPassword();
			if (presentPassword.equals(enteredPassword)) {
				CustomerModel merchModel = forgotPasswordService.updatePassword(merchantModel, forgotPasswordRequest);
				response = RestUtils.wrapObjectForSuccess(merchModel);
			}
			else 
			{
				response = RestUtils.wrapObjectForFailure(forgotPasswordRequest, "402", WebConstants.PASSWORD_MISMATCH);
			}
		}
		return response;
	}
	@RequestMapping(value = "deliveryBoy/resetPassword", method = RequestMethod.PUT)
	public CommonResponse newPasswordForDeliveryBoy(@RequestBody ForgotPasswordRequest forgotPasswordRequest) throws Exception {
		CommonResponse response = null;
		EncryptDescrypt e = new EncryptDescrypt();
	    DeliveryBoyModel merchantModel = deliveryBoyService.getByDeliveryBoyId(forgotPasswordRequest.getDeliveryBoy_id());
		if (merchantModel == null) {
			response = RestUtils.wrapObjectForFailure(forgotPasswordRequest, "402", WebConstants.DELEIVERY_BOY_DOESNOT_EXIST);

		} else {
			String presentPassword = forgotPasswordRequest.getNewPassword();
			String enteredPassword = forgotPasswordRequest.getConfirmPassword();
			if (presentPassword.equals(enteredPassword)) {
				DeliveryBoyModel merchModel = forgotPasswordService.updatePassword(merchantModel, forgotPasswordRequest);
				response = RestUtils.wrapObjectForSuccess(merchModel);
			}
			else 
			{
				response = RestUtils.wrapObjectForFailure(forgotPasswordRequest, "402", WebConstants.PASSWORD_MISMATCH);
			}
		}
		return response;
	}
	
	

	protected SuccessResponse getSuccessResponse(final Object data, final String message) {
		final SuccessResponse sucessrespone = new SuccessResponse();
		sucessrespone.setStatus("Sucesss");
		sucessrespone.setData(data);
		sucessrespone.setMessage(message);
		return sucessrespone;

	}
}
