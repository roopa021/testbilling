package com.kouchan.simplebilling.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kouchan.simplebilling.exception.CustomerDetailssNotFoundException;
import com.kouchan.simplebilling.exception.SimplebillingException;
import com.kouchan.simplebilling.model.FrequentOrderBillModel;
import com.kouchan.simplebilling.request.FrequentOrderBillRequest;
import com.kouchan.simplebilling.response.SuccessResponse;
import com.kouchan.simplebilling.service.FrequentOrderRequestBillService;

@RestController
public class FrequentOrderRequestBillController {

	@Autowired
	public FrequentOrderRequestBillService frequentOrderRequestBillService;

	public FrequentOrderRequestBillController() {
		System.out.println("FrequentOrderRequestBillController");
	}

	@PostMapping
	@RequestMapping(value = "/createBill/newrequest")
	public ResponseEntity<SuccessResponse> createBillNewOrderRequst(@RequestBody FrequentOrderBillRequest frequentOrder,
			BindingResult bindingresult) throws SimplebillingException {

		if (bindingresult.hasErrors()) {
			throw new SimplebillingException(bindingresult);
		}
		// int productId = l.getProduct_Id();
		FrequentOrderBillModel createBill = frequentOrderRequestBillService.getBillData(frequentOrder);
		return new ResponseEntity<SuccessResponse>(getSuccessResponse(createBill, "Success!!"), HttpStatus.OK);

	}

	@GetMapping
	@RequestMapping(value = "/sendBill/{customer_id}")
	public ResponseEntity<SuccessResponse> getBill(@PathVariable Long customer_id) throws CustomerDetailssNotFoundException {
		
		final FrequentOrderBillModel freuqentOrderBill=frequentOrderRequestBillService.getCustomerBill(customer_id);
    
		return new ResponseEntity<SuccessResponse>(getSuccessResponse(freuqentOrderBill, "Success!!"), HttpStatus.OK);

	}
	
	@PutMapping
	@RequestMapping(value="/billStatus/update/{merchat_id}")
	public ResponseEntity<FrequentOrderBillModel>billStatusUpdate(@PathVariable int merchat_id,@RequestBody FrequentOrderBillModel frequentOrderBillModel )throws SimplebillingException
 	{
		FrequentOrderBillModel billModel=frequentOrderRequestBillService.updateBillStatus(frequentOrderBillModel,merchat_id);
		return new ResponseEntity<FrequentOrderBillModel>(billModel,new HttpHeaders(), HttpStatus.OK);
	}
	



	protected SuccessResponse getSuccessResponse(final Object data, final String message) {
		final SuccessResponse sucessrespone = new SuccessResponse();
		sucessrespone.setStatus("Sucesss");
		sucessrespone.setData(data);
		sucessrespone.setMessage(message);
		return sucessrespone;
	}
}
