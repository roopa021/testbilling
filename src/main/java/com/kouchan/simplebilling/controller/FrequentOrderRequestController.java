package com.kouchan.simplebilling.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kouchan.simplebilling.coredata.CodeMessage;
import com.kouchan.simplebilling.exception.BillingException;
import com.kouchan.simplebilling.exception.MerchantNotFoundException;
import com.kouchan.simplebilling.exception.SimplebillingException;
import com.kouchan.simplebilling.model.CustomerModel;
import com.kouchan.simplebilling.model.DeliveryBoyModel;
import com.kouchan.simplebilling.model.FrequentOrderRequestModel;
import com.kouchan.simplebilling.model.MerchantModel;
import com.kouchan.simplebilling.model.ProductListModel;
import com.kouchan.simplebilling.repository.CustomerRepository;
import com.kouchan.simplebilling.repository.MerchantRepository;
import com.kouchan.simplebilling.repository.ProductListRepository;
import com.kouchan.simplebilling.request.FrequentOrderRequest;
import com.kouchan.simplebilling.response.FrequentOrderRequestResponse;
import com.kouchan.simplebilling.response.SuccessResponse;
import com.kouchan.simplebilling.service.FrequentOrderRequestService;


@RestController
public class FrequentOrderRequestController {

	@Autowired
	private FrequentOrderRequestService frequentOrderRequestService;

	@Autowired
	private MerchantRepository merchantRepository;
	
	@Autowired
	private ProductListRepository productListRepository;
	
	@Autowired
	private CustomerRepository customerRepository;

	public FrequentOrderRequestController() {
		System.out.println("FrequentOrderRequestController()");
	}

	@PostMapping
	@RequestMapping(value = "/frequentOrder/request")
	public ResponseEntity<SuccessResponse> createNewOrderRequst(
			@RequestBody @Valid FrequentOrderRequest frequentOrderRequest, BindingResult bindingresult)
			throws SimplebillingException {
		if (bindingresult.hasErrors()) {
			throw new SimplebillingException(bindingresult);
		}
		MerchantModel merchantModel = new MerchantModel();
		ProductListModel productListModel = new ProductListModel();
		CustomerModel customerModel = new CustomerModel();
		DeliveryBoyModel deliveryBoyModel=new DeliveryBoyModel();
		
		merchantModel.setId(frequentOrderRequest.getMerchant_Id());
		productListModel.setProduct_id(frequentOrderRequest.getProduct_Id());
		customerModel.setId(frequentOrderRequest.getCustomer_Id());
		deliveryBoyModel.setDeliveryBoyId(frequentOrderRequest.getDeliveryBoyId());
		
		int merchantId = frequentOrderRequest.getMerchant_Id();
		int productId=frequentOrderRequest.getProduct_Id();
		Long customerId=frequentOrderRequest.getCustomer_Id();
		ProductListModel p=productListRepository.findOne(productId);
		MerchantModel m = merchantRepository.findOne(merchantId);
		CustomerModel c=customerRepository.findById(customerId);
		if (m == null || p == null || c ==null) {
			final List<CodeMessage> list = new ArrayList<>();
			list.add(new CodeMessage("302", "invalid credentials"));
			throw new BillingException(list);		
			}

		FrequentOrderRequestModel frequentOrderRequestModel = new FrequentOrderRequestModel();
		BeanUtils.copyProperties(frequentOrderRequest, frequentOrderRequestModel);

		frequentOrderRequestModel.setMerchantModel(merchantModel);
		frequentOrderRequestModel.setCustomerModel(customerModel);
		frequentOrderRequestModel.setProductListModel(productListModel);
		frequentOrderRequestModel.setDeliveryBoyModel(deliveryBoyModel);

		FrequentOrderRequestModel createNewRequest = frequentOrderRequestService.save(frequentOrderRequestModel);

		FrequentOrderRequestResponse frequentOrderRequestResponse = new FrequentOrderRequestResponse();
		BeanUtils.copyProperties(createNewRequest, frequentOrderRequestResponse);

		return new ResponseEntity<SuccessResponse>(
				getSuccessResponse(frequentOrderRequestResponse, "Success!!"), HttpStatus.OK);

	}

	protected SuccessResponse getSuccessResponse(final Object data, final String message) {
		final SuccessResponse sucessrespone = new SuccessResponse();
		sucessrespone.setStatus("Sucesss");
		sucessrespone.setData(data);
		sucessrespone.setMessage(message);
		return sucessrespone;

	}

}
