package com.kouchan.simplebilling.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.kouchan.simplebilling.coredata.CodeMessage;
import com.kouchan.simplebilling.exception.BillingException;
import com.kouchan.simplebilling.exception.CustomerDetailssNotFoundException;
import com.kouchan.simplebilling.exception.DeliveryBoyAlreadyExist;
import com.kouchan.simplebilling.exception.DeliveryBoyNotFoundException;
import com.kouchan.simplebilling.exception.GroupAlreadyExitException;
import com.kouchan.simplebilling.exception.GroupNotFoundException;
import com.kouchan.simplebilling.exception.MerchantAlreadyExistException;
import com.kouchan.simplebilling.exception.MerchantNotFoundException;
import com.kouchan.simplebilling.exception.SimplebillingException;
import com.kouchan.simplebilling.response.GlobalErrorResponse;

@ControllerAdvice
public class GlobalExceptionHandler {
	
	private static final String ERROR="error";
	
	
	@ExceptionHandler(SimplebillingException.class)
	public ResponseEntity<GlobalErrorResponse> userDetailsExceptionHandler(final SimplebillingException ex,
			final Locale locale) {
		return new ResponseEntity<GlobalErrorResponse>(populateException(ex.getBindingResult(), locale),
				HttpStatus.BAD_REQUEST);
	}
	
	
	@ExceptionHandler(BillingException.class)
	public ResponseEntity<GlobalErrorResponse> billingExceptionHandler(final BillingException ex,
			final Locale locale) {
		return new ResponseEntity<GlobalErrorResponse>(populateException(ex.getErrorMessage(), locale),
				HttpStatus.BAD_REQUEST);

	}


	@ExceptionHandler(MerchantAlreadyExistException.class)
	public ResponseEntity<GlobalErrorResponse> merchnatAlreadyExistExceptionHandler(final MerchantAlreadyExistException ex,
			final Locale locale) {
		return new ResponseEntity<GlobalErrorResponse>(populateException(ex.getErrorMessages(), locale),
				HttpStatus.BAD_REQUEST);

	}

	@ExceptionHandler(MerchantNotFoundException.class)
	public ResponseEntity<GlobalErrorResponse> merchnatNotFoundExceptionHandler(final MerchantNotFoundException ex,
			final Locale locale) {
		return new ResponseEntity<GlobalErrorResponse>(populateException(ex.getErrorMessages(), locale),
				HttpStatus.BAD_REQUEST);

	}


	@ExceptionHandler(CustomerDetailssNotFoundException.class)
	public ResponseEntity<GlobalErrorResponse> customerDetailssNotFoundExceptionHandler(final CustomerDetailssNotFoundException ex,
			final Locale locale) {
		return new ResponseEntity<GlobalErrorResponse>(populateException(ex.getErrorMessages(), locale),
				HttpStatus.BAD_REQUEST);
	}
	

	@ExceptionHandler(GroupNotFoundException .class)
	public ResponseEntity<GlobalErrorResponse> groupNotFoundExceptionHandler(final GroupNotFoundException ex,
			final Locale locale) {
		return new ResponseEntity<GlobalErrorResponse>(populateException(ex.getErrorMessages(), locale),
				HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(GroupAlreadyExitException .class)
	public ResponseEntity<GlobalErrorResponse> groupAlreadyExitExceptionHandler(final GroupAlreadyExitException ex,
			final Locale locale) {
		return new ResponseEntity<GlobalErrorResponse>(populateException(ex.getErrormessage(), locale),
				HttpStatus.BAD_REQUEST);
	}

	
	@ExceptionHandler(DeliveryBoyNotFoundException.class)
	public ResponseEntity<GlobalErrorResponse> deliveryBoyNotFoundExceptionHandler(final DeliveryBoyNotFoundException ex,
			final Locale locale) {
		return new ResponseEntity<GlobalErrorResponse>(populateException(ex.getErrorMessages(), locale),
				HttpStatus.BAD_REQUEST);

	}
    
	@ExceptionHandler(DeliveryBoyAlreadyExist.class)
	public ResponseEntity<GlobalErrorResponse> deliveryBoyAlreadyExistExceptionHandler(final DeliveryBoyAlreadyExist ex,
			final Locale locale) {
		return new ResponseEntity<GlobalErrorResponse>(populateException(ex.getErrorMessages(), locale),
				HttpStatus.BAD_REQUEST);

	}

	private GlobalErrorResponse populateException(List<CodeMessage> errorMessages, Locale locale) {
		final List<CodeMessage> allMessage = new ArrayList<>();
		for (final CodeMessage error : errorMessages) {
			final CodeMessage codeMessage = new CodeMessage();
			codeMessage.setCode(error.getCode());
			codeMessage.setMessage(error.getMessage());
			allMessage.add(codeMessage);
		}
		return new GlobalErrorResponse(ERROR, allMessage);
	}

	private GlobalErrorResponse populateException(final BindingResult result, final Locale locale) {
		final List<CodeMessage> allMessage = new ArrayList<>();
		for (final FieldError error : result.getFieldErrors()) {
			final CodeMessage codeMessage = new CodeMessage();
			codeMessage.setMessage(error.getDefaultMessage());
			codeMessage.setCode(error.getCode());
			codeMessage.setField(error.getField());
			allMessage.add(codeMessage);
		}
		return new GlobalErrorResponse(ERROR, allMessage);
	}
	}
	
	
	
	
	


