package com.kouchan.simplebilling.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kouchan.simplebilling.coredata.CodeMessage;
import com.kouchan.simplebilling.exception.CustomerDetailssNotFoundException;
import com.kouchan.simplebilling.exception.GroupNotFoundException;
import com.kouchan.simplebilling.exception.MerchantNotFoundException;
import com.kouchan.simplebilling.model.GroupModel;
import com.kouchan.simplebilling.request.GroupRequest;
import com.kouchan.simplebilling.response.GroupDeleteResponse;
import com.kouchan.simplebilling.response.GroupResponse;
import com.kouchan.simplebilling.response.SuccessResponse;
import com.kouchan.simplebilling.service.GroupService;
import com.kouchan.simplebilling.service.GroupService2;

@RestController
public class GroupController {

	@Autowired
	private GroupService groupService;

	@Autowired
	private GroupService2 groupService2;

	public GroupController() {
		System.out.println("GroupController()");
	}

	@PostMapping
	@RequestMapping(value = "/create/group")
	public ResponseEntity<GroupResponse> createUserGroup(@RequestBody final GroupRequest groupRequest) throws CustomerDetailssNotFoundException {

		final GroupResponse groupResponse = groupService.createUserGroup(groupRequest);
		return new ResponseEntity<GroupResponse>(groupResponse, HttpStatus.OK);

	}

	@RequestMapping(value = "/addMember/{groupId}", method = RequestMethod.PUT)
	public ResponseEntity<GroupResponse> addMembersToGroup(@PathVariable final Long groupId,
			@RequestBody final GroupRequest groupRequest) throws CustomerDetailssNotFoundException {
		final GroupResponse groupResponse = groupService.addMembersToGroup(groupId, groupRequest);
		return new ResponseEntity<GroupResponse>(groupResponse, HttpStatus.OK);

	}

	@RequestMapping(value = "/updateGroupName/{groupId}", method = RequestMethod.PATCH)
	public ResponseEntity<GroupResponse> updateGroupName(@PathVariable final Long groupId,
			@RequestBody final GroupRequest groupRequest) throws GroupNotFoundException {
		final String groupName = groupRequest.getGroupName();
		final String mobileNumber = groupRequest.getMobileNumber();
		final Long merchantId = groupRequest.getMerchantId();
		final GroupResponse groupResponse = groupService2.updateUserGroupNameService(groupId, mobileNumber, groupName,
				merchantId);
		return new ResponseEntity<GroupResponse>(groupResponse, HttpStatus.OK);

	}

	@RequestMapping(value = "/getGroup/{groupId}", method = RequestMethod.GET)
	public ResponseEntity<GroupResponse> getAllGroupMemberDetails(final Long merchantId,
			@PathVariable final Long groupId, final GroupRequest groupRequest) throws GroupNotFoundException {
		final GroupResponse groupResponse = groupService2.getGroupDetailsService(merchantId, groupId);
		return new ResponseEntity<GroupResponse>(groupResponse, HttpStatus.OK);

	}

	@RequestMapping(value = "/{merchantId}/getMemberDetails/{groupId}", method = RequestMethod.GET)
	public ResponseEntity<GroupResponse> getUserGroupDetails(@PathVariable final Long groupId,
			@PathVariable final Long merchantId, final GroupRequest groupRequest) throws GroupNotFoundException {
		// Long groupId=groupRequest.getMerchantId();
		// Long merchantId=groupRequest.getMerchantId();
		final GroupResponse groupResponse = groupService2.getAllGroupMemberDetailsService(groupId, merchantId);
		return new ResponseEntity<GroupResponse>(groupResponse, HttpStatus.OK);

	}

	@RequestMapping(value = "/deleteGroup/{groupId}", method = RequestMethod.DELETE)
	public ResponseEntity<GroupDeleteResponse> deleteUserGroup(@PathVariable final Long groupId,
			final GroupRequest groupRequest) {
		// final Long groupOwnerId = Long.valueOf(groupRequest.getMobileNumber());
		final GroupDeleteResponse groupResponse = groupService2.deleteUserGroupService(groupId);
		return new ResponseEntity<GroupDeleteResponse>(groupResponse, HttpStatus.OK);

	}

	@GetMapping
	@RequestMapping(value = "/getAllGroupNames/{merchant_id}")
	public ResponseEntity<SuccessResponse> getAllGroupNames(@PathVariable Long merchant_id)
			throws CustomerDetailssNotFoundException {
		final List<GroupModel> getAllGroupNames = groupService2.getAllGroupNames(merchant_id);

		return new ResponseEntity<SuccessResponse>(getSuccessResponse(getAllGroupNames, "Success!!"), HttpStatus.OK);

	}

	//TODO: already assigned this delivery boy to gp validation
	@RequestMapping(value = "/assigndDeliveryBoy/{groupId}")
	public ResponseEntity<SuccessResponse> assignDeliveryBoy(@PathVariable final Long groupId,
			@RequestBody final GroupRequest groupRequest) throws GroupNotFoundException {
		final Long merchantId = groupRequest.getMerchantId();
		final GroupResponse groupResponse = groupService2.assignDeliveryBoy(groupId, merchantId, groupRequest);
		if (groupResponse == null) {
			final List<CodeMessage> list = new ArrayList<>();
			list.add(new CodeMessage("Null value"));
			throw new MerchantNotFoundException(list);
		}
		return new ResponseEntity<SuccessResponse>(getSuccessResponse(groupResponse, "Delivery Boy Assigned To Group Sucessfully !!"), HttpStatus.OK);

	}

	protected SuccessResponse getSuccessResponse(final Object data, final String message) {
		final SuccessResponse sucessrespone = new SuccessResponse();
		sucessrespone.setStatus("Sucesss");
		sucessrespone.setData(data);
		sucessrespone.setMessage(message);
		return sucessrespone;

	}
}
