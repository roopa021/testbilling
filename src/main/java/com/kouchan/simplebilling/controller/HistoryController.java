package com.kouchan.simplebilling.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.kouchan.simplebilling.model.InvoiceModel;
import com.kouchan.simplebilling.repository.CustomerHistoryRepo;
import com.kouchan.simplebilling.request.OrderDetailsRequest;
import com.kouchan.simplebilling.response.SuccessResponse;
import com.kouchan.simplebilling.service.CustomerHistoryService;

@RestController
public class HistoryController {

	public HistoryController() {
		System.out.println("HistoryController()");
	}

	@Autowired
	public CustomerHistoryService customerHistoryService;

	@PostMapping(value = "bill/history/betweenDates/{merchantId}")
	public ResponseEntity<SuccessResponse> billHistory(
			@PathVariable("merchantId") @RequestBody @Valid OrderDetailsRequest OrderDetailsRequest) {
		List customerhistory = null;/* = customerHistoryRepo.findAllCustomer(); */
		// InvoiceModel invoiceModel=new InvoiceModel();
		int customerId = OrderDetailsRequest.getCustomerId();
		String startDate = OrderDetailsRequest.getFromMonth();
		String endDate = OrderDetailsRequest.getToMonth();
		List<InvoiceModel> invoiceModel = customerHistoryService.getHistoryBetweenDate(customerId, startDate, endDate);
		return new ResponseEntity<SuccessResponse>(getSuccessResponse(customerhistory, "Customer History "),
				HttpStatus.OK);

	}

	protected SuccessResponse getSuccessResponse(final Object data, final String message) {
		final SuccessResponse sucessrespone = new SuccessResponse();
		sucessrespone.setStatus("Sucesss");
		sucessrespone.setData(data);
		sucessrespone.setMessage(message);
		return sucessrespone;
	}

}
