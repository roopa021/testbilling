package com.kouchan.simplebilling.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kouchan.simplebilling.request.InvoiceVO;
import com.kouchan.simplebilling.response.CommonResponse;
import com.kouchan.simplebilling.service.InvoiceService;
import com.kouchan.simplebilling.utils.RestUtils;

@RestController
public class InvoiceController {

	@Autowired
	public InvoiceService invoiceService;
	
	String msg = "sucess";

	@PostMapping
	@RequestMapping(value = "/invoice/monthwise")
	public CommonResponse generateInvoice(@RequestBody InvoiceVO invoiceVO) throws Exception {

		String s = invoiceService.generateInvoice(invoiceVO);
		if (s.equals("Succes")) {
			return RestUtils.wrapObjectForSuccess(s);
		}

		return RestUtils.wrapObjectForFailure(s, "error", "Invoice not generated");

	}
	
    //Correct code in customer Controller
	@PostMapping
	@RequestMapping(value = "/updateDeliveryStatus")
	public CommonResponse udateDeliveryStatus(@RequestBody InvoiceVO invoiceVO) throws Exception {

		String s = invoiceService.udapteDeliveryStatus(invoiceVO);
		if (s.equals("Succes")) {
			return RestUtils.wrapObjectForSuccess(s);
		}

		return RestUtils.wrapObjectForFailure(s, "error", "Delivery Status Updated");

	}

}
