package com.kouchan.simplebilling.controller;

import java.util.Calendar;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.kouchan.simplebilling.model.MasterTimeSlotModel;
import com.kouchan.simplebilling.request.MasterTimeSlotRequest;
import com.kouchan.simplebilling.response.CommonResponse;
import com.kouchan.simplebilling.service.MasterTimeSlotService;
import com.kouchan.simplebilling.utils.RestUtils;

@RestController
public class MasterTimeSlotController {

	public MasterTimeSlotController() {
		System.out.println("MasterTimeSlot()");
	}

	@Autowired
	private MasterTimeSlotService masterTimeSlotService;
	

	// TODO:1.create time slot for merchant
	// 2.give the timeslot of merchant in drop down
	// 3.Update the timeslot

	Calendar c = Calendar.getInstance();

	@PostMapping(name = "/timeSlot/creation")
	public CommonResponse crreateMasterSlot1(@RequestBody MasterTimeSlotRequest masterTimeSlotRequest)
			throws Exception {
		try {
			int merchantId=masterTimeSlotRequest.getMerchantId();
			final Date startDate=masterTimeSlotService.stringToDate(masterTimeSlotRequest.getStartDate());
           MasterTimeSlotModel m=masterTimeSlotService.createMasterSlot(merchantId, startDate,masterTimeSlotRequest);
			return RestUtils.wrapObjectForSuccess(m);

		} catch (

		Exception exception) {
			return RestUtils.wrapObjectForFailure("error", "500", exception.getMessage());
		}
	}
	
}
