package com.kouchan.simplebilling.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kouchan.simplebilling.repository.MerchantCancelReasonsRepo;
import com.kouchan.simplebilling.response.SuccessResponse;

@RestController
public class MerchantCancelReasonController
{
	
	private static final Logger LOG = LoggerFactory.getLogger(CustomerCancelController.class);
	
	@Autowired
	MerchantCancelReasonsRepo  merchantCustomerRepository; 
	
	@GetMapping(value = "cancel/merchantregion")
	public  ResponseEntity<SuccessResponse> cancelreason() 
	{  
			
		//	List<CommonResponse> response = null;

			List customerreasons = merchantCustomerRepository.findAll();
			
		//	response = (List<CommonResponse>) RestUtils.wrapObjectForSuccess(customerreasons);
		
			  return new ResponseEntity<SuccessResponse>(getSuccessResponse(customerreasons, "Customer Cancel REASON"),
		                HttpStatus.OK);						
	}
	
	
	protected SuccessResponse getSuccessResponse(final Object data, final String message)
	{
        final SuccessResponse sucessrespone = new SuccessResponse();
        sucessrespone.setStatus("Sucesss");
        sucessrespone.setData(data);
        sucessrespone.setMessage(message);
        return sucessrespone;
    }

}
