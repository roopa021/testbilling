
package com.kouchan.simplebilling.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kouchan.simplebilling.constant.CommonConstants;
import com.kouchan.simplebilling.constant.WebConstants;
import com.kouchan.simplebilling.coredata.CodeMessage;
import com.kouchan.simplebilling.exception.BusinessException;
import com.kouchan.simplebilling.exception.MerchantNotFoundException;
import com.kouchan.simplebilling.exception.SimplebillingException;
import com.kouchan.simplebilling.model.MerchantModel;
import com.kouchan.simplebilling.repository.MerchantRepository;
import com.kouchan.simplebilling.request.MerchantRegistrationRequest;
import com.kouchan.simplebilling.response.CommonResponse;
import com.kouchan.simplebilling.response.DeleteResponse;
import com.kouchan.simplebilling.response.MerchantResponse;
import com.kouchan.simplebilling.response.SuccessResponse;
import com.kouchan.simplebilling.service.MerchantService;
import com.kouchan.simplebilling.utils.EncryptDescrypt;
import com.kouchan.simplebilling.utils.RestUtils;
import com.kouchan.simplebilling.validator.MechantRegisterRequestValidator;
import com.mysql.jdbc.StringUtils;

@RestController
public class MerchantController {

	@Autowired
	private MerchantService merchantService;

	@Autowired
	private MechantRegisterRequestValidator mechantRegisterRequestValidator;
	
	@Autowired
	private MerchantRepository merchantRepository;

	String msg = "Sucess";
	byte activeStatus = 0;
	byte inactiveStatus = 1;

	private static final Logger LOG = LoggerFactory.getLogger(CustomerController.class);

	Calendar calendar = Calendar.getInstance();

	public MerchantController() {
		System.out.println("MerchantController()");

	}

	@PostMapping(value = "/merchant/registration")
	public ResponseEntity<SuccessResponse> merchantRegistration(
			@RequestBody @Valid MerchantRegistrationRequest merchantRegistrationRequest, BindingResult bindingresult)
			throws Exception {
		mechantRegisterRequestValidator.validate(merchantRegistrationRequest, bindingresult);
		if (bindingresult.hasErrors()) {
			throw new SimplebillingException(bindingresult);
		}
		MerchantModel merchant = new MerchantModel();
		BeanUtils.copyProperties(merchantRegistrationRequest, merchant);
		MerchantModel createMerchant = merchantService.save(merchant);
		MerchantResponse merchantResponse = new MerchantResponse();
		BeanUtils.copyProperties(createMerchant, merchantResponse);
		return new ResponseEntity<SuccessResponse>(
				getSuccessResponse(merchantResponse, "Merchant Registered Suessfully !!"), HttpStatus.OK);
	}

	@RequestMapping(value = "merchant/generateOtp", method = RequestMethod.POST)
	public int generateOtp(@RequestParam String mobile) {
		CommonResponse response = null;
		MerchantModel m = merchantService.generateOtp(mobile);
		int generated = m.getOtp();
		if (generated == 0) {
			response = RestUtils.wrapObjectForSuccess("success");
		}

		else {
			response = RestUtils.wrapObjectForFailure("Otp not generated, invalid user", "error",
					WebConstants.WEB_RESPONSE_ERORR);
		}
		return generated;
	}

	@SuppressWarnings("unused")
	@RequestMapping(value = "merchant/validateOtp", method = RequestMethod.POST)
	public CommonResponse validateOtp(@RequestParam String mobile, @RequestParam int otp) {
		CommonResponse response = null;
		MerchantModel m=merchantRepository.findByMobileAndOtp(mobile,otp);
		if (m == null ) 
		{
			final List<CodeMessage> list = new ArrayList<>();
			list.add(new CodeMessage("302", "Invalid Credential"));
			throw new MerchantNotFoundException(list);
		}
			if(m.getIsDeleted()==inactiveStatus)
			{
				final List<CodeMessage> list = new ArrayList<>();
				list.add(new CodeMessage("303", "Merchant Not Found"));
				throw new MerchantNotFoundException(list);
			}
		
		int verifyOtp=m.getOtp();
		if(verifyOtp==otp||m != null)
		{	
				response = RestUtils.wrapObjectForOTP("OTP verfied Successfully ", "error", WebConstants.OTP);
		}	
		else
		{
			response = RestUtils.wrapObjectForFailure("invalid otp", "error", WebConstants.WEB_RESPONSE_ERORR);
		}
		return response;
	}

	@PatchMapping
	@RequestMapping(value = "/merchant/update/{id}")
	public ResponseEntity<SuccessResponse> updateMerchant(@PathVariable("id") int id,
			@RequestBody MerchantModel merchantModel) throws Exception {
		MerchantModel merchant = merchantService.getByMerchantId(id);
		if (merchant == null) {
			throw new MerchantNotFoundException();
		}
		MerchantModel m = merchantService.update12(merchantModel, id);
		MerchantResponse merchantResponse = new MerchantResponse();
		EncryptDescrypt e = new EncryptDescrypt();

		BeanUtils.copyProperties(m, merchantResponse);
		merchantResponse.setPassword(e.decrypt(merchantResponse.getPassword()));
		return new ResponseEntity<SuccessResponse>(
				getSuccessResponse(merchantResponse, "Merchant Updated Suessfully !!"), HttpStatus.OK);
	}

	@GetMapping
	@RequestMapping(value = "/getMerchantDetails/{id}")
	public ResponseEntity<SuccessResponse> getMerchantDetails(@PathVariable("id") int id, MerchantModel merchanModel) {
		final MerchantModel merchant = merchantService.getMerchnatDetails(id);
		MerchantResponse merchantResponse = new MerchantResponse();
		BeanUtils.copyProperties(merchant, merchantResponse);
		return new ResponseEntity<SuccessResponse>(getSuccessResponse(merchantResponse, "Merchnat Details !!"),
				HttpStatus.OK);
	}
	



	/* Merchant Delete APi */
	@RequestMapping(value = "/deleteMerchant/{Id}", method = RequestMethod.DELETE)
	public ResponseEntity<DeleteResponse> deleteMerchant(@PathVariable int Id) {
		final DeleteResponse merchantResponse = merchantService.deleteMerchant(Id);
		return new ResponseEntity<DeleteResponse>(merchantResponse, HttpStatus.OK);
	}

	/* Login Api Merchant */
	@RequestMapping(value = "merchant/login", method = RequestMethod.POST)
	public CommonResponse login(@RequestBody MerchantRegistrationRequest merchantRegrequest) {
		CommonResponse response = null;
		Map<String, Object> map = validateLogin(merchantRegrequest);
		if (!map.isEmpty()) {
			response = RestUtils.wrapObjectForFailure(map, "validation error", WebConstants.WEB_RESPONSE_ERORR);
			LOG.error("Login Validation missing");
		}
		try {
			MerchantModel merchantReg = merchantService.login(merchantRegrequest);
			if (merchantReg == null) {
				return response = RestUtils.wrapObjectForFailure(merchantReg, "302",
						WebConstants.MERCHANT_DOESNOT_EXIST);
			}
			MerchantResponse merchantResponse = new MerchantResponse();
			BeanUtils.copyProperties(merchantReg, merchantResponse);
			response = RestUtils.wrapObjectForSuccess(merchantResponse);
			LOG.info("Logged in successfully " + merchantResponse.getMobileNumber());
		}

		catch (BusinessException be) {
			response = RestUtils.wrapObjectForFailure(null, be.getErrorCode(), be.getErrorMsg());
		} catch (Exception e) {
			response = RestUtils.wrapObjectForFailure(null, WebConstants.WEB_RESPONSE_ERROR,
					WebConstants.INTERNAL_SERVER_ERROR_MESSAGE);
			e.printStackTrace();
		}
		return response;
	}

	/* Validate a login merchant */
	private Map<String, Object> validateLogin(MerchantRegistrationRequest merchantRegrequest) {

		Map<String, Object> map = new HashMap<>();
		if (merchantRegrequest == null) {
			map.put("All", CommonConstants.ALL_FIELDS_REQUIRED);
		} else {
			if (StringUtils.isNullOrEmpty(merchantRegrequest.getMobileNumber())
					|| StringUtils.isEmptyOrWhitespaceOnly(merchantRegrequest.getMobileNumber())) {
				map.put("mobileNumber", CommonConstants.MOBILE_NUMBER_EMPTY);
			}
			if (StringUtils.isNullOrEmpty(merchantRegrequest.getPassword())
					|| StringUtils.isEmptyOrWhitespaceOnly(merchantRegrequest.getPassword())) {
				map.put("password", CommonConstants.PASSWORD_EMPTY);
			}
		}
		return map;
	}

	protected SuccessResponse getSuccessResponse(final Object data, final String message) {
		final SuccessResponse sucessrespone = new SuccessResponse();
		sucessrespone.setStatus("Sucesss");
		sucessrespone.setData(data);
		sucessrespone.setMessage(message);
		return sucessrespone;

	}

}
