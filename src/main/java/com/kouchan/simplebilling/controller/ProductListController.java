package com.kouchan.simplebilling.controller;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kouchan.simplebilling.constant.WebConstants;
import com.kouchan.simplebilling.exception.CustomerDetailssNotFoundException;
import com.kouchan.simplebilling.exception.SimplebillingException;
import com.kouchan.simplebilling.model.MerchantModel;
import com.kouchan.simplebilling.model.ProductListModel;
import com.kouchan.simplebilling.request.ProductListRequest;
import com.kouchan.simplebilling.response.CommonResponse;
import com.kouchan.simplebilling.response.ProductListResponse;
import com.kouchan.simplebilling.response.SuccessResponse;
import com.kouchan.simplebilling.service.ProductListService;
import com.kouchan.simplebilling.utils.RestUtils;

@RestController
// TODO: need to delete n update of product
public class ProductListController {

	@SuppressWarnings("unused")
	private static final Logger LOG = LoggerFactory.getLogger(ProductListController.class);

	@Autowired
	ProductListService productListService;

	/*
	 * Default class constructor
	 * 
	 */

	public ProductListController() {
		System.out.println("productListController()");
	}

	@PostMapping
	@RequestMapping(value = "/merchant/productlist")
	public ResponseEntity<SuccessResponse> productList(@RequestBody @Valid ProductListRequest productListRequest,
			BindingResult bindingresult) throws SimplebillingException {
		if (bindingresult.hasErrors()) {
			throw new SimplebillingException(bindingresult);
		}
		ProductListModel product = new ProductListModel();
		BeanUtils.copyProperties(productListRequest, product);
		MerchantModel merchantmodel = new MerchantModel();
		merchantmodel.setId(productListRequest.getMerchantid());
		product.setMerchantModel(merchantmodel);
		int merchantId = productListRequest.getMerchantid();
		// product.setRate(productListRequest.getRate());

		ProductListModel createProduct = productListService.saveProducts(product, merchantId);

		ProductListResponse productResponse = new ProductListResponse();
		BeanUtils.copyProperties(createProduct, productResponse);

		productResponse.setMerchantid(createProduct.getMerchantModel().getId());

		return new ResponseEntity<SuccessResponse>(getSuccessResponse(productResponse, "Products Added Suessfully!!"),
				HttpStatus.OK);

	}

	@GetMapping
	@RequestMapping(value = "/merchant/getsubproductnames/{id}")
	public ResponseEntity<SuccessResponse> getSubProductNames(@PathVariable int id) {
		try {		
			MerchantModel m=productListService.getByMerchantId(id);
			List<ProductListModel> merchant = productListService.getSubproductName(id);
			if (merchant.isEmpty()) {
				return new ResponseEntity<SuccessResponse>(
						getSuccessResponse(null, "Merchant Yet Not Created Any Products"), HttpStatus.NOT_FOUND);
			}
			return new ResponseEntity<SuccessResponse>(getSuccessResponse(merchant, "Sub ProductList Names!!"),
					HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<SuccessResponse>(getSuccessResponse(null, "Merchant Does't Exit"),
					HttpStatus.NOT_FOUND);
		}
	}
	
	
	@GetMapping
	@RequestMapping(value = "/merchant/getproductnames/{id}")
	public ResponseEntity<SuccessResponse> getproductNames(@PathVariable int id) {
		try {
			List<ProductListModel> merchant = productListService.getproductNames(id);
			if (merchant.isEmpty()) {
				return new ResponseEntity<SuccessResponse>(
						getSuccessResponse(null, "Merchant Yet Not Created Any Products"), HttpStatus.NOT_FOUND);
			}
			return new ResponseEntity<SuccessResponse>(getSuccessResponse(merchant, "Sub ProductList Names!!"),
					HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<SuccessResponse>(getSuccessResponse(null, "Merchant Does't Exit"),
					HttpStatus.NOT_FOUND);
		}
	}

	
	//NOTE: 500--Error
	@PostMapping
	@RequestMapping(value = "/merchant/allProducts")
	public CommonResponse merchantAllProducts(@RequestParam(name = "merchId") int merchId,
			@RequestParam(name = "category") String category) {
		System.out.println("looop");
		CommonResponse response = null;
		System.out.println("looop");
		List<ProductListModel> productList = productListService.getAllProducts(merchId, category);

		if (productList != null)
			response = RestUtils.wrapObjectForSuccess(productList);
		else
			response = RestUtils.wrapObjectForFailure(null, "error", WebConstants.WEB_RESPONSE_ERORR);
		return response;
	}

	@RequestMapping(value = "/deleteProduct", method = RequestMethod.DELETE)
	public CommonResponse deleteProduct(@RequestParam(name = "merchantId") int merchantId,
			@RequestParam(name = "subProduct") String subProduct) {
		CommonResponse response = null;

		List<ProductListModel> productList = productListService.deleteProduct(merchantId, subProduct);

		if (productList != null)
			response = RestUtils.wrapObjectForSuccess(productList);
		else
			response = RestUtils.wrapObjectForFailure(null, "error", WebConstants.WEB_RESPONSE_ERORR);
		return response;
	}

	@RequestMapping(value = "/getAllProductDetails/merchant/{merchant_id}", method = RequestMethod.GET)
	public ResponseEntity<SuccessResponse> getAllProductDetails(@PathVariable int merchant_id)
			throws CustomerDetailssNotFoundException {
		final List<ProductListModel> getAllProductDetails = productListService.getAllProductDetails(merchant_id);
		return new ResponseEntity<SuccessResponse>(getSuccessResponse(getAllProductDetails, "Success!!"),
				HttpStatus.OK);

	}

	protected SuccessResponse getSuccessResponse(final Object data, final String message) {
		final SuccessResponse sucessrespone = new SuccessResponse();
		sucessrespone.setStatus("Sucesss");
		sucessrespone.setData(data);
		sucessrespone.setMessage(message);
		return sucessrespone;
	}

}
