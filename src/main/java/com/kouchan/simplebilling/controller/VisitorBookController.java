package com.kouchan.simplebilling.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.kouchan.simplebilling.constant.WebConstants;
import com.kouchan.simplebilling.coredata.CodeMessage;
import com.kouchan.simplebilling.emailattachment.EmailSender;
import com.kouchan.simplebilling.excelview.ExcelReportView;
import com.kouchan.simplebilling.exception.MerchantAlreadyExistException;
import com.kouchan.simplebilling.exception.SimplebillingException;
import com.kouchan.simplebilling.model.PurposeModel;
import com.kouchan.simplebilling.model.ToMeetModel;
import com.kouchan.simplebilling.model.VisitorModel;
import com.kouchan.simplebilling.model.VisitorOtpModel;
import com.kouchan.simplebilling.repository.PurposeRepository;
import com.kouchan.simplebilling.repository.ToMeetRepository;
import com.kouchan.simplebilling.repository.VisitorDao;
import com.kouchan.simplebilling.repository.VisitorOtpDao;
import com.kouchan.simplebilling.request.VisitorOtprequest;
import com.kouchan.simplebilling.request.VistorRequest;
import com.kouchan.simplebilling.response.CommonResponse;
import com.kouchan.simplebilling.response.SuccessResponse;
import com.kouchan.simplebilling.service.ReportGenService;
import com.kouchan.simplebilling.service.SmsService;
import com.kouchan.simplebilling.service.VistitorService;
import com.kouchan.simplebilling.utils.RestUtils;

@RestController
public class VisitorBookController {

	public VisitorBookController() {
		System.out.println("VisitorBookController()");
	}

	@Autowired
	private VistitorService vistitorService;

	@Autowired
	private SmsService smsService;

	@Autowired
	private VisitorDao visitorDao;

	@Autowired
	private ReportGenService reportGenService;

	@Autowired
	private ToMeetRepository toMeet;

	@Autowired
	private PurposeRepository pupose;

	@Autowired
	private VisitorOtpDao visitorOtpDao;

	@Autowired(required = true)
	@Qualifier(value = "emailSender")
	private EmailSender emailSender;

	@PostMapping(value = "/createVisitor") // saveVisitor
	public ResponseEntity<SuccessResponse> createVistitor(@RequestBody @Valid VistorRequest visitorRequest,
			BindingResult bindingresult) throws Exception {
		if (bindingresult.hasErrors()) {
			throw new SimplebillingException(bindingresult);
		}
		VisitorModel visitorModel = new VisitorModel();
		BeanUtils.copyProperties(visitorRequest, visitorModel);
		VisitorModel saveVisitor = vistitorService.save(visitorModel);
		return new ResponseEntity<SuccessResponse>(getSuccessResponse(saveVisitor, "Visitor Registered Suessfully !!"),
				HttpStatus.OK);
	}

	@SuppressWarnings("unused")
	@RequestMapping(value = "visitor/validateOtp", method = RequestMethod.POST)
	public CommonResponse validateOtp(@RequestParam String mobile, @RequestParam int otp) {
		CommonResponse response = null;
		VisitorModel m = visitorDao.findByMobileAndOTP(mobile, otp);
		// VisitorModel m=visitorDao.findOne(otp);
		if (m == null) {
			final List<CodeMessage> list = new ArrayList<>();
			list.add(new CodeMessage("302", "Invalid Credential"));
			throw new MerchantAlreadyExistException(list);
		}

		int verifyOtp = m.getOtp();
		if (verifyOtp == otp || m != null) {
			response = RestUtils.wrapObjectForOTP("OTP verfied Successfully ", "error", WebConstants.OTP);
			smsService.sendSucessMsg(mobile);
		} else {
			response = RestUtils.wrapObjectForFailure("invalid otp", "error", WebConstants.WEB_RESPONSE_ERORR);
		}
		return response;

	}

	/************************** New Code *****************************/
	@PostMapping(value = "/visitorDetails")
	public ResponseEntity<SuccessResponse> merchantRegistration(@RequestBody @Valid VisitorOtprequest visitorRequest,
			BindingResult bindingresult) throws Exception {
		if (bindingresult.hasErrors()) {
			throw new SimplebillingException(bindingresult);
		}
		VisitorOtpModel visitorOtpModel = new VisitorOtpModel();
		BeanUtils.copyProperties(visitorRequest, visitorOtpModel);
		VisitorOtpModel saveVisitor = vistitorService.saveOtp(visitorOtpModel);
		// MerchantResponse merchantResponse = new MerchantResponse();
		// BeanUtils.copyProperties(createMerchant, merchantResponse);
		return new ResponseEntity<SuccessResponse>(getSuccessResponse(saveVisitor, "Visitor Registered Suessfully !!"),
				HttpStatus.OK);
	}

	@SuppressWarnings("unused")
	@RequestMapping(value = "visitor/validateOtp1", method = RequestMethod.POST)
	public CommonResponse validateOtp1(@RequestParam String mobileNumber, @RequestParam int otp) {
		CommonResponse response = null;
		VisitorOtpModel m = visitorOtpDao.findByMobileAndOTP(mobileNumber, otp);
		// VisitorModel m=visitorDao.findOne(otp);
		if (m == null) {
			final List<CodeMessage> list = new ArrayList<>();
			list.add(new CodeMessage("302", "Invalid Credential"));
			throw new MerchantAlreadyExistException(list);
		}

		int verifyOtp = m.getOtp();
		if (verifyOtp == otp || m != null) {
			response = RestUtils.wrapObjectForOTP("OTP verfied Successfully ", "error", WebConstants.OTP);
			// smsService.sendSucessMsg(mobileNumber);
		} else {
			response = RestUtils.wrapObjectForFailure("invalid otp", "error", WebConstants.WEB_RESPONSE_ERORR);
		}
		return response;

	}

	// NOTE:To Download Report Locally
	@RequestMapping(value = "/report", method = RequestMethod.GET)
	public ModelAndView walletOutStandingBalanceXmlReport(
			@RequestParam(value = "reportPeriod", required = true) final String reportPeriod,
			final HttpServletRequest request, final HttpServletResponse response) {
		final Map<String, Object> model = new HashMap<String, Object>();
		if (reportPeriod.equals("daily")) {
			model.put("customerDashboard", visitorDao.findAll());
			System.out.println("----" + model.toString());
			// response.setContentType("application/ms-excel");
			// response.setContentType("application/mallu.ms-excel");
			// emailSender.sendMessageWithAttachment();
			return new ModelAndView(new ExcelReportView(), model);
		}
		return null;
	}

	// NOTE:To Send The Email On The Fly
	@Scheduled(cron = "	0 0 18 * * ? ")
	@RequestMapping(value = "/visitorDetailsEmail", method = RequestMethod.GET)
	public String visitorDetailsEmail() {
		reportGenService.emailvistorDetials();
		System.out.println("cron test----------" + Calendar.getInstance().getTime());
		return "generateReport";
	}

	@GetMapping(value = "/getToMeetList")
	public ResponseEntity<SuccessResponse> getToMeetList() throws Exception {
		List<ToMeetModel> toMeeet = toMeet.findAll();
		return new ResponseEntity<SuccessResponse>(getSuccessResponse(toMeeet, "To Meet Get List"), HttpStatus.OK);
	}

	@GetMapping(value = "/getPurposeList")
	public ResponseEntity<SuccessResponse> getPurposeList() throws Exception {
		List<PurposeModel> purpose = pupose.findAll();
		return new ResponseEntity<SuccessResponse>(getSuccessResponse(purpose, "Pupose Get List"), HttpStatus.OK);
	}

	protected SuccessResponse getSuccessResponse(final Object data, final String message) {
		final SuccessResponse sucessrespone = new SuccessResponse();
		sucessrespone.setStatus("Sucesss");
		sucessrespone.setData(data);
		sucessrespone.setMessage(message);
		return sucessrespone;

	}
}