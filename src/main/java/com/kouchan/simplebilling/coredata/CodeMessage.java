package com.kouchan.simplebilling.coredata;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class CodeMessage {
	private String code;
	private String message;
	private String detail;
	private Object[] params;
	private String field;

	public CodeMessage() {

	}

	public CodeMessage(final String code, final String message, final String field) {
		this.code = code;
		this.message = message;
		this.field = field;
	}

	public CodeMessage(final String code, final String message, final String field, final Object[] params) {
		this.code = code;
		this.message = message;
		this.field = field;
		this.params = params;
	}

	public CodeMessage(final String code) {
		this.code = code;
	}

	public CodeMessage(final String code, final String message) {
		this.code = code;
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public void setCode(final String code) {
		this.code = code;
	}

	public String getField() {
		return field;
	}

	public void setField(final String field) {
		this.field = field;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(final String message) {
		this.message = message;
	}

	public Object[] getParams() {
		return params;
	}

	public void setParams(final Object[] params) {
		this.params = params;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(final String detail) {
		this.detail = detail;
	}

}
