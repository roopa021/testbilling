package com.kouchan.simplebilling.data;

import java.util.Date;
import java.util.List;
import java.util.Map;

public class EmailData {
	  private String mailFrom;

	    private String mailTo;

	    private String mailCc;

	    private String mailBcc;

	    private String mailReplyTo;

	    private String mailSubject;

	    private String mailContent;

	    private String mailBody;
	    private String contentType;

	    private List<Object> attachments;

	    private Map<String, Object> model;

	    private String templateName;

	    private String attachetMentName;

	    public String getAttachetMentName() {
	        return attachetMentName;
	    }

	    public void setAttachetMentName(final String attachetMentName) {
	        this.attachetMentName = attachetMentName;
	    }

	    public String getMailBody() {
	        return mailBody;
	    }

	    public void setMailBody(final String mailBody) {
	        this.mailBody = mailBody;
	    }

	    public EmailData() {
	        contentType = "text/plain";
	    }

	    public String getContentType() {
	        return contentType;
	    }

	    public void setContentType(final String contentType) {
	        this.contentType = contentType;
	    }

	    public String getMailBcc() {
	        return mailBcc;
	    }

	    public void setMailBcc(final String mailBcc) {
	        this.mailBcc = mailBcc;
	    }

	    public String getMailCc() {
	        return mailCc;
	    }

	    public void setMailCc(final String mailCc) {
	        this.mailCc = mailCc;
	    }

	    public String getMailFrom() {
	        return mailFrom;
	    }

	    public void setMailFrom(final String mailFrom) {
	        this.mailFrom = mailFrom;
	    }

	    public String getMailSubject() {
	        return mailSubject;
	    }

	    public void setMailSubject(final String mailSubject) {
	        this.mailSubject = mailSubject;
	    }

	    public String getMailTo() {
	        return mailTo;
	    }

	    public void setMailTo(final String mailTo) {
	        this.mailTo = mailTo;
	    }

	    public Date getMailSendDate() {
	        return new Date();
	    }

	    public String getMailContent() {
	        return mailContent;
	    }

	    public void setMailContent(final String mailContent) {
	        this.mailContent = mailContent;
	    }

	    public List<Object> getAttachments() {
	        return attachments;
	    }

	    public void setAttachments(final List<Object> attachments) {
	        this.attachments = attachments;
	    }

	    public Map<String, Object> getModel() {
	        return model;
	    }

	    public void setModel(final Map<String, Object> model) {
	        this.model = model;
	    }

	    public String getTemplateName() {
	        return templateName;
	    }

	    public void setTemplateName(final String templateName) {
	        this.templateName = templateName;
	    }

	    public String getMailReplyTo() {
	        return mailReplyTo;
	    }

	    public void setMailReplyTo(final String mailReplyTo) {
	        this.mailReplyTo = mailReplyTo;
	    }
}
