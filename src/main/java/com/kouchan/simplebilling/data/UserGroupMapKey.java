package com.kouchan.simplebilling.data;

import java.io.Serializable;

public class UserGroupMapKey implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long groupId;

	private Long groupMemberId;

	// private int deliveryBoyId;

	public UserGroupMapKey() {

	}

	public UserGroupMapKey(final Long groupId, final Long groupMemberId) {
		this.groupId = groupId;
		this.groupMemberId = groupMemberId;
	}

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(final Long groupId) {
		this.groupId = groupId;
	}

	public Long getGroupMemberId() {
		return groupMemberId;
	}

	public void setGroupMemberId(final Long groupMemberId) {
		this.groupMemberId = groupMemberId;
	}

	/*
	 * public int getDeliveryBoyId() { return deliveryBoyId; }
	 * 
	 * public void setDeliveryBoyId(int deliveryBoyId) { this.deliveryBoyId =
	 * deliveryBoyId; }
	 */

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((groupId == null) ? 0 : groupId.hashCode());
		result = prime * result + ((groupMemberId == null) ? 0 : groupMemberId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserGroupMapKey other = (UserGroupMapKey) obj;
		if (groupId == null) {
			if (other.groupId != null)
				return false;
		} else if (!groupId.equals(other.groupId))
			return false;
		if (groupMemberId == null) {
			if (other.groupMemberId != null)
				return false;
		} else if (!groupMemberId.equals(other.groupMemberId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "UserGroupMapKey [groupId=" + groupId + ", groupMemberId=" + groupMemberId + "]";
	}
	
	
	

}
