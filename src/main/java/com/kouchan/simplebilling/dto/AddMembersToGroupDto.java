package com.kouchan.simplebilling.dto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class AddMembersToGroupDto {
	
	 List<HashMap<String, String>> listOfRegUsers = new ArrayList<HashMap<String, String>>();
	    List<HashMap<String, String>> listOfUnRegUsers = new ArrayList<HashMap<String, String>>();

	    public List<HashMap<String, String>> getListOfRegUsers() {
	        return listOfRegUsers;
	    }

	    public void setListOfRegUsers(final List<HashMap<String, String>> listOfRegUsers) {
	        this.listOfRegUsers = listOfRegUsers;
	    }

	    public List<HashMap<String, String>> getListOfUnRegUsers() {
	        return listOfUnRegUsers;
	    }

	    public void setListOfUnRegUsers(final List<HashMap<String, String>> listOfUnRegUsers) {
	        this.listOfUnRegUsers = listOfUnRegUsers;
	    }


}
