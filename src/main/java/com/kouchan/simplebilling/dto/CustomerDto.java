package com.kouchan.simplebilling.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CustomerDto {

	@JsonProperty("mobileNumber")
	private String mobileNumber;
	
	@JsonProperty("name")
	private String name;

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "CustomerDto [mobileNumber=" + mobileNumber + ", name=" + name + "]";
	}

}
