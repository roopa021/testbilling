package com.kouchan.simplebilling.dto;

import java.util.ArrayList;
import java.util.List;

import com.kouchan.simplebilling.model.DeliveryBoyModel;

public class DeliveryBoyDto {
	
	
	List<DeliveryBoyModel>avalibleDeliveryboys=new ArrayList<DeliveryBoyModel>();
	
	List<DeliveryBoyModel>unAvalibleDeliveryboys=new ArrayList<DeliveryBoyModel>();

	public List<DeliveryBoyModel> getAvalibleDeliveryboys() {
		return avalibleDeliveryboys;
	}

	public void setAvalibleDeliveryboys(List<DeliveryBoyModel> avalibleDeliveryboys) {
		this.avalibleDeliveryboys = avalibleDeliveryboys;
	}

	public List<DeliveryBoyModel> getUnAvalibleDeliveryboys() {
		return unAvalibleDeliveryboys;
	}

	public void setUnAvalibleDeliveryboys(List<DeliveryBoyModel> unAvalibleDeliveryboys) {
		this.unAvalibleDeliveryboys = unAvalibleDeliveryboys;
	}	
	
	
	


}
