package com.kouchan.simplebilling.dto;

import java.util.ArrayList;
import java.util.List;

public class GroupDetailsDto {
	private Long groupId;
	private String groupName;
	//private String groupCreatedDate;
	private List<GroupMemberDetailDto> listOfRegUsers = new ArrayList<GroupMemberDetailDto>();

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(final Long groupId) {
		this.groupId = groupId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(final String groupName) {
		this.groupName = groupName;
	}

	
	public List<GroupMemberDetailDto> getListOfRegUsers() {
		return listOfRegUsers;
	}

	public void setListOfRegUsers(final List<GroupMemberDetailDto> listOfRegUsers) {
		this.listOfRegUsers = listOfRegUsers;
	}

}
