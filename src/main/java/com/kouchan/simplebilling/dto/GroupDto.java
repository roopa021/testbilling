package com.kouchan.simplebilling.dto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class GroupDto {

	Long groupId;
	String groupName;
	Long merchantId;
	List<HashMap<String, String>> listOfRegUsers = new ArrayList<HashMap<String, String>>();
	List<HashMap<String, String>> listOfUnRegUsers = new ArrayList<HashMap<String, String>>();

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public List<HashMap<String, String>> getListOfRegUsers() {
		return listOfRegUsers;
	}

	public void setListOfRegUsers(List<HashMap<String, String>> listOfRegUsers) {
		this.listOfRegUsers = listOfRegUsers;
	}

	public List<HashMap<String, String>> getListOfUnRegUsers() {
		return listOfUnRegUsers;
	}

	public void setListOfUnRegUsers(List<HashMap<String, String>> listOfUnRegUsers) {
		this.listOfUnRegUsers = listOfUnRegUsers;
	}

	public Long getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(Long merchantId) {
		this.merchantId = merchantId;
	}
	

}
