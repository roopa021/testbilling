package com.kouchan.simplebilling.dto;

public class GroupMemberDetailDto {
	
	private String groupMemberId;
	private String groupMemberName;
	private String groupMemberMobile;
	
	
	public String getGroupMemberId() {
		return groupMemberId;
	}
	public void setGroupMemberId(String groupMemberId) {
		this.groupMemberId = groupMemberId;
	}
	public String getGroupMemberName() {
		return groupMemberName;
	}
	public void setGroupMemberName(String groupMemberName) {
		this.groupMemberName = groupMemberName;
	}
	public String getGroupMemberMobile() {
		return groupMemberMobile;
	}
	public void setGroupMemberMobile(String groupMemberMobile) {
		this.groupMemberMobile = groupMemberMobile;
	}
	
	

}
