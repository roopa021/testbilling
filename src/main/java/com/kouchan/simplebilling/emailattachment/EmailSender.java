package com.kouchan.simplebilling.emailattachment;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

@Component(value="emailSender")
public class EmailSender {

	@Autowired
	private JavaMailSender javaMailSender;

	public void sendMessageWithAttachment(String to, String subject, String text, String pathToAttachment)
			throws MessagingException {

		MimeMessage message = javaMailSender.createMimeMessage();

		MimeMessageHelper helper = new MimeMessageHelper(message, true);

		helper.setTo("roopa@kouchanindia.com");
		helper.setSubject("Visitor Record");

		helper.setText(text);

		FileSystemResource file = new FileSystemResource("D:\test.xls");
		helper.addAttachment(file.getFilename(), file);
		helper.addAttachment("Visitor Details", file);

		javaMailSender.send(message);
		// ...
	}

	void sendEmail(String mobile, String Password) {
		// Password = pass;
		// mobile = mob;

		SimpleMailMessage msg = new SimpleMailMessage();

		msg.setSubject("Simple Billing");
		msg.setText("Wel-Come to Kouchan simple Billing Application" + "\n" + "Login ID=" + mobile + "\n"
				+ "Password is=" + Password);

		javaMailSender.send(msg);
	}
	public void sendMessageWithAttachment()
			throws MessagingException {

		MimeMessage message = javaMailSender.createMimeMessage();

		MimeMessageHelper helper = new MimeMessageHelper(message, true);

		helper.setTo("roopa@kouchanindia.com");
		helper.setSubject("Visitor Record");

		helper.setText("nfd,");
		//helper.setFrom("spring.mail.username");

		FileSystemResource file = new FileSystemResource("D:\\test.xls");
		helper.addAttachment(file.getFilename(), file);
	//	helper.addAttachment("Visitor Details", file);
		javaMailSender.send(message);
	  

	}}
