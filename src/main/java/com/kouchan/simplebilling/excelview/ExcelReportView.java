package com.kouchan.simplebilling.excelview;

import java.io.FileOutputStream;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import com.kouchan.simplebilling.emailattachment.EmailSender;
import com.kouchan.simplebilling.model.VisitorModel;
import com.kouchan.simplebilling.repository.VisitorDao;
import com.kouchan.simplebilling.service.EmailService;

public class ExcelReportView extends AbstractXlsView {
	
	@Autowired
	EmailSender e;

	@Override
	protected void buildExcelDocument(final Map<String, Object> model, final Workbook workbook,
			final HttpServletRequest request, final HttpServletResponse response) throws Exception {

		response.setHeader("Content-Disposition", "attachment; filename=\"customer-dashboard.xls\"");

		@SuppressWarnings("unchecked")
		List<VisitorModel> student = (List<VisitorModel>) model.get("customerDashboard");
		final String fileTitle = "Visitor Report ";
		// create excel xls sheet
		final Sheet sheet = workbook.createSheet(fileTitle);
		sheet.setDefaultColumnWidth(30);
		final Row header = sheet.createRow(0);
		header.createCell(0).setCellValue("Visitor Name");
		header.createCell(1).setCellValue("Visitor Mobile Number");
		header.createCell(2).setCellValue("To Meet");
		header.createCell(3).setCellValue("Purpose");
		header.createCell(4).setCellValue("Visitor Visiting Time");
		int rowCount = 1;
		if (student != null) {
			for (final VisitorModel customerModel : student) {
				final Row userRow = sheet.createRow(rowCount++);
				userRow.createCell(0).setCellValue(customerModel.getName());
				userRow.createCell(1).setCellValue(customerModel.getMobileNumber());
				userRow.createCell(2).setCellValue(customerModel.getToMeet());
				userRow.createCell(3).setCellValue(customerModel.getPurpose());
				userRow.createCell(4).setCellValue(customerModel.getVisitDate());
			}
		}
	 	VisitorModel v=new VisitorModel();
		//String y=v.setVisitTime("");
		
		String y = "D:\\test.xls";
		FileOutputStream fileOut = new FileOutputStream(y);

		workbook.write(fileOut);
		fileOut.close();
		
		
	}
}
