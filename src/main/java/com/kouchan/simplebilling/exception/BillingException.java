package com.kouchan.simplebilling.exception;

import java.util.List;

import com.kouchan.simplebilling.coredata.CodeMessage;

public class BillingException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	List<CodeMessage>errorMessage;
	
	public BillingException()
	{
		
	}

	public BillingException(List<CodeMessage> errorMessage) {
		super();
		this.errorMessage = errorMessage;
	}

	public List<CodeMessage> getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(List<CodeMessage> errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	// BillingException(String name,)

}
