package com.kouchan.simplebilling.exception;

import java.util.List;

import com.kouchan.simplebilling.coredata.CodeMessage;

public class CustomerDetailssNotFoundException extends Exception{
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<CodeMessage> errorMessages;



	public CustomerDetailssNotFoundException (final List<CodeMessage> errorMessages) {
		this.errorMessages = errorMessages;
	}

	public List<CodeMessage> getErrorMessages() {
		return errorMessages;
	}

	public void setErrorMessages(final List<CodeMessage> errorMessages) {
		this.errorMessages = errorMessages;
	}

	/**
	 * Instantiates a new user already exists exception.
	 *
	 * @param mobileNumber
	 *            the mobile number
	 */
	public CustomerDetailssNotFoundException (final String mobileNumber,String name) {
		super("Customer Not Exit: " + name+mobileNumber);
	}


}
