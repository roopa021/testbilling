package com.kouchan.simplebilling.exception;

import java.util.List;

import com.kouchan.simplebilling.coredata.CodeMessage;

public class DeliveryBoyAlreadyExist extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<CodeMessage> errorMessages;
	
	public DeliveryBoyAlreadyExist() {
		
	}

	public DeliveryBoyAlreadyExist(List<CodeMessage> errorMessages) {
		super();
		this.errorMessages = errorMessages;
	}

	public List<CodeMessage> getErrorMessages() {
		return errorMessages;
	}

	public void setErrorMessages(List<CodeMessage> errorMessages) {
		this.errorMessages = errorMessages;
	}
	
	public DeliveryBoyAlreadyExist(String mobileNumber,String name)
	{
		System.out.println("Delivery Boy Already Exit :"+name+mobileNumber);
	}
     

	

}
