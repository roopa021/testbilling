package com.kouchan.simplebilling.exception;

import java.util.List;

import com.kouchan.simplebilling.coredata.CodeMessage;

public class GroupAlreadyExitException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	List<CodeMessage>errormessage;
	
	public GroupAlreadyExitException()
	{
		
	}

	public GroupAlreadyExitException(List<CodeMessage> errormessage) {
		super();
		this.errormessage = errormessage;
	}

	public List<CodeMessage> getErrormessage() {
		return errormessage;
	}

	public void setErrormessage(List<CodeMessage> errormessage) {
		this.errormessage = errormessage;
	}
	
	public GroupAlreadyExitException(String groupname)
	{
		System.out.println("Group Name Already Exit "+groupname);
	}

}
