package com.kouchan.simplebilling.exception;

import java.util.List;

import com.kouchan.simplebilling.coredata.CodeMessage;

public class GroupNotFoundException extends Exception {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<CodeMessage> errorMessages;



	public GroupNotFoundException(final List<CodeMessage> errorMessages) {
		this.errorMessages = errorMessages;
	}

	public List<CodeMessage> getErrorMessages() {
		return errorMessages;
	}

	public void setErrorMessages(final List<CodeMessage> errorMessages) {
		this.errorMessages = errorMessages;
	}

	/**
	 * Instantiates a new user already exists exception.
	 *
	 * @param mobileNumber
	 *            the mobile number
	 */
	public GroupNotFoundException (final Long groupId,String name) {
		super("Group Not Found: " + name+groupId);
	}

}
