package com.kouchan.simplebilling.exception;

import java.util.List;

import com.kouchan.simplebilling.coredata.CodeMessage;

public class MerchantNotFoundException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<CodeMessage> errorMessages;

	public MerchantNotFoundException() {
		super();
	}

	public MerchantNotFoundException(final List<CodeMessage> errorMessages) {
		this.errorMessages = errorMessages;
	}

	public List<CodeMessage> getErrorMessages() {
		return errorMessages;
	}

	public void setErrorMessages(final List<CodeMessage> errorMessages) {
		this.errorMessages = errorMessages;
	}

	/**
	 * Instantiates a exception where merchant not found
	 *
	 * @param mobileNumber
	 *            the mobile number
	 */
	public MerchantNotFoundException(final String mobileNumber,String password) {
		super("Merchannt Not Exit With This Mobile Number: " + mobileNumber);
	}


}
