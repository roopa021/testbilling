package com.kouchan.simplebilling.exception;

import java.util.List;

import org.springframework.validation.BindingResult;

import com.kouchan.simplebilling.coredata.CodeMessage;

public class SimplebillingException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private BindingResult bindingResult;

	public SimplebillingException() {

	}
	
	public SimplebillingException(BindingResult bindingResult) {
		super();
		this.bindingResult = bindingResult;
	}
	
	public BindingResult getBindingResult() {
		return bindingResult;
	}

	public void setBindingResult(BindingResult bindingResult) {
		this.bindingResult = bindingResult;
	}

	public SimplebillingException(final String mobileNumber, String name) {
		super("Simple Billing Exception: " + name + mobileNumber);
	}

}
