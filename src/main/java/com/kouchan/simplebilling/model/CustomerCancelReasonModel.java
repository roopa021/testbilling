package com.kouchan.simplebilling.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="customercancelreason")
public class CustomerCancelReasonModel {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id ;
	
	@Column(name="customer_reason")
	private String customerReason;
	
	@Column(name="customer_id")
	private long customerId;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCustomerReason() {
		return customerReason;
	}

	public void setCustomerReason(String customerReason) {
		this.customerReason = customerReason;
	}

	public long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}
	

	

}
