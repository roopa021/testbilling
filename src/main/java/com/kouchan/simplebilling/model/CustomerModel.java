package com.kouchan.simplebilling.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "customers")
@NamedQuery(name = "CustomerModel.findAll", query = "select u from CustomerModel u")
public class CustomerModel {


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "customer_id")
	private Long id;

	@Column(name = "customer_name")
	private String name;

	@Column(name = "mobile_number")
	private String mobileNumber;
	
	@Column(name = "password")
    private String password;
	
	@Column(name="subproduct_name")
    private String subproduct;
    
    @Column(name="unit")
    private String unit;

	@OneToMany(mappedBy = "customerModel",cascade=CascadeType.ALL)
	private List<MerchantCustomerModel> merchantCustomerModel;
	
	@OneToMany(mappedBy = "customerModel",cascade=CascadeType.ALL)
	private List<FrequentOrderRequestModel>frequentOrderRequestModel;

    
	@Column(name = "is_deleted", columnDefinition = "TINYINT(1) default 0 ")
	private byte isDeleted;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public byte getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(byte isDeleted) {
		this.isDeleted = isDeleted;
	}


	public String getSubproduct() {
		return subproduct;
	}

	public void setSubproduct(String subproduct) {
		this.subproduct = subproduct;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}
	
    

}
