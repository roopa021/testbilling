package com.kouchan.simplebilling.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@Table(name = "customer_order_details")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
@NamedQuery(name = "CustomerOrderDetails.findAll", query = "select u from CustomerOrderDetails u")
public class CustomerOrderDetails {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "order_id")
	private int orderId;
	
	@ManyToOne
	@JoinColumn(name="customer_id")
	private CustomerModel customerModel;
	
	@ManyToOne
	@JoinColumn(name="merchant_id")
	private MerchantModel merchantModel;
	
	@ManyToOne
	@JoinColumn(name="product_id")
	private ProductListModel productListModel;
		
	
	@Column(name="rate")
	private Double rate;
	
	
	@Column(name="month_year")
	private String monthYear;
	
	@Column(name="month")
	private String month;


	public int getOrderId() {
		return orderId;
	}


	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}


	public CustomerModel getCustomerModel() {
		return customerModel;
	}


	public void setCustomerModel(CustomerModel customerModel) {
		this.customerModel = customerModel;
	}


	public MerchantModel getMerchantModel() {
		return merchantModel;
	}


	public void setMerchantModel(MerchantModel merchantModel) {
		this.merchantModel = merchantModel;
	}


	public ProductListModel getProductListModel() {
		return productListModel;
	}


	public void setProductListModel(ProductListModel productListModel) {
		this.productListModel = productListModel;
	}


	public Double getRate() {
		return rate;
	}


	public void setRate(Double rate) {
		this.rate = rate;
	}


	public String getMonthYear() {
		return monthYear;
	}


	public void setMonthYear(String monthYear) {
		this.monthYear = monthYear;
	}


	public String getMonth() {
		return month;
	}


	public void setMonth(String month) {
		this.month = month;
	}	
	
}
