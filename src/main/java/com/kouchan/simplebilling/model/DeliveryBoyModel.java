package com.kouchan.simplebilling.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="delivery_Boy")
@NamedQuery(name = "DeliveryBoyModel.findAll", query = "SELECT u FROM DeliveryBoyModel u")
public class DeliveryBoyModel {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="delivery_boy_id")
	private int deliveryBoyId;
	
	@Column(name="delivery_boy_name")
	private String name;
	
	@Column(name="delivery_boy_mobile_number")
	private String mobileNumber;
	
	@Column(name="password")
	private String password;
	
	@Column(name="address")
	private String  address;
	
	@ManyToOne
	@JoinColumn(name = "merchant_id",nullable=false)
	private MerchantModel merchantModel;

	/*@ManyToOne
	@JoinColumn(name = "group_id",nullable=false)
	private GroupModel groupModel;*/
	
	@OneToMany(mappedBy = "deliveryBoyModel")
	private List<FrequentOrderRequestModel>frequentOrderRequestModel;
	
	@Column(name = "is_deleted", columnDefinition = "TINYINT(1) default 0")
	private byte isDeleted;
	
	@Column(name="is_Avaliable",columnDefinition="TINYINT(1) default 0")
	private byte isAvaliable;

	public int getDeliveryBoyId() {
		return deliveryBoyId;
	}

	public void setDeliveryBoyId(int deliveryBoyId) {
		this.deliveryBoyId = deliveryBoyId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public MerchantModel getMerchantModel() {
		return merchantModel;
	}

	public void setMerchantModel(MerchantModel merchantModel) {
		this.merchantModel = merchantModel;
	}


	public byte getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(byte isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public byte getIsAvaliable() {
		return isAvaliable;
	}

	public void setIsAvaliable(byte isAvaliable) {
		this.isAvaliable = isAvaliable;
	}

}
