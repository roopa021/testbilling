package com.kouchan.simplebilling.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "email_template")
@NamedQuery(name = "EmailTemplateModel.findAll", query = "SELECT e FROM EmailTemplateModel e")
public class EmailTemplateModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "body")
	private String body;

	@Column(name = "from")
	private String from;

	@Column(name = "to")
	private String to;

	@Column(name = "reply_to")
	private String replyTo;
	
	@Column(name="cc")
	private String cC;

	@Column(name = "subject")
	private String subject;

	/** The template code. */
	@Column(name = "TEMPLATE_CODE", unique = true, nullable = false)
	private String templateCode;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getReplyTo() {
		return replyTo;
	}

	public void setReplyTo(String replyTo) {
		this.replyTo = replyTo;
	}

	public String getTemplateCode() {
		return templateCode;
	}

	public void setTemplateCode(String templateCode) {
		this.templateCode = templateCode;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	
	public String getcC() {
		return cC;
	}

	public void setcC(String cC) {
		this.cC = cC;
	}

	@Override
	public String toString() {
		return "EmailTemplateModel [id=" + id + ", body=" + body + ", from=" + from + ", to=" + to + ", replyTo="
				+ replyTo + ", cC=" + cC + ", subject=" + subject + ", templateCode=" + templateCode + "]";
	}

	
}
