package com.kouchan.simplebilling.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name="forgot_password")
@NamedQuery(name="ForgotPasswordModel.findAll", query = "SELECT u FROM ForgotPasswordModel u")
public class ForgotPasswordModel {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private int id;
	
	@Column(name="mobile_number")
	private String mobileNumber;
	
	@Column(name="otp")
	private int otp;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public int getOtp() {
		return otp;
	}

	public void setOtp(int otp) {
		this.otp = otp;
	}

	@Override
	public String toString() {
		return "ForgotPasswordModel [id=" + id + ", mobileNumber=" + mobileNumber + ", otp=" + otp + "]";
	}

	
	
	
	
	

}
