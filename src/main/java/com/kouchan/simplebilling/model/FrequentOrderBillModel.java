package com.kouchan.simplebilling.model;

import javax.persistence.Entity;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "frequent_order_bill")
public class FrequentOrderBillModel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int billId;

	@Column(name = "customer_id")
	private Long customer_id;

	@Column(name = "bill_number")
	private int billNumber;

	@Column(name = "bill_date")
	private String billDate;

	@Column(name = "bill_month")
	private String billMonth;

	@Column(name = "total")
	private int total;

	@Column(name = "is_gst_applicable", columnDefinition = "TINYINT(1) default 0 ")
	private byte isgstApplicable;

	@Column(name = "per_gst")
	private double pergst;

	@Column(name = "sgst")
	private int sgst;

	@Column(name = "cgst")
	private int cgst;

	@Column(name = "igst")
	private int igst;

	@Column(name = "discount")
	private int discount;

	@Column(name = "total_amount")
	private int totalAmount;

	@Column(name = "customer_name")
	private String customerName;

	@Column(name = "customer_number")
	private String customerNumber;

	@Column(name = "bill_status", columnDefinition = "TINYINT(1) default 0 ")
	private byte billStatus;
	

	@Column(name = "merchat_id")
	private int merchat_id;
	
	public int getBillId() {
		return billId;
	}

	public void setBillId(int billId) {
		this.billId = billId;
	}

	public Long getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(Long customer_id) {
		this.customer_id = customer_id;
	}

	public int getBillNumber() {
		return billNumber;
	}

	public void setBillNumber(int billNumber) {
		this.billNumber = billNumber;
	}

	public String getBillDate() {
		return billDate;
	}

	public void setBillDate(String billDate) {
		this.billDate = billDate;
	}

	public String getBillMonth() {
		return billMonth;
	}

	public void setBillMonth(String billMonth) {
		this.billMonth = billMonth;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public byte getIsgstApplicable() {
		return isgstApplicable;
	}

	public void setIsgstApplicable(byte isgstApplicable) {
		this.isgstApplicable = isgstApplicable;
	}

	public double getPergst() {
		return pergst;
	}

	public void setPergst(double pergst) {
		this.pergst = pergst;
	}

	public int getSgst() {
		return sgst;
	}

	public void setSgst(int sgst) {
		this.sgst = sgst;
	}

	public int getCgst() {
		return cgst;
	}

	public void setCgst(int cgst) {
		this.cgst = cgst;
	}

	public int getIgst() {
		return igst;
	}

	public void setIgst(int igst) {
		this.igst = igst;
	}

	public int getDiscount() {
		return discount;
	}

	public void setDiscount(int discount) {
		this.discount = discount;
	}

	public int getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(int totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerNumber() {
		return customerNumber;
	}

	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}

	public byte getBillStatus() {
		return billStatus;
	}

	public void setBillStatus(byte billStatus) {
		this.billStatus = billStatus;
	}

	public int getMerchat_id() {
		return merchat_id;
	}

	public void setMerchat_id(int merchat_id) {
		this.merchat_id = merchat_id;
	}
 
}