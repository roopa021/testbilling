package com.kouchan.simplebilling.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "frequent_order_request")
@NamedQuery(name = "FrequentOrderRequestModel.findAll", query = "select u from FrequentOrderRequestModel u")
public class FrequentOrderRequestModel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "order_id")
	private int orderId;

	@ManyToOne
	@JoinColumn(name = "merchant_id")
	private MerchantModel merchantModel;

	@ManyToOne
	@JoinColumn(name = "product_id")
	private ProductListModel productListModel;

	@ManyToOne
	@JoinColumn(name = "customer_id")
	private CustomerModel customerModel;

	@ManyToOne
	@JoinColumn(name = "delivery_boy_id")
	private DeliveryBoyModel deliveryBoyModel;

	@Column(name = "quantity")
	private int quantity;

	@Column(name = "date")
	private String date;

	@Column(name = "time_slot")
	private String timeSlot;

	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public String getTimeSlot() {
		return timeSlot;
	}

	public void setTimeSlot(String timeSlot) {
		this.timeSlot = timeSlot;
	}

	public MerchantModel getMerchantModel() {
		return merchantModel;
	}

	public void setMerchantModel(MerchantModel merchantModel) {
		this.merchantModel = merchantModel;
	}

	public ProductListModel getProductListModel() {
		return productListModel;
	}

	public void setProductListModel(ProductListModel productListModel) {
		this.productListModel = productListModel;
	}

	public CustomerModel getCustomerModel() {
		return customerModel;
	}

	public void setCustomerModel(CustomerModel customerModel) {
		this.customerModel = customerModel;
	}

	public DeliveryBoyModel getDeliveryBoyModel() {
		return deliveryBoyModel;
	}

	public void setDeliveryBoyModel(DeliveryBoyModel deliveryBoyModel) {
		this.deliveryBoyModel = deliveryBoyModel;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

}