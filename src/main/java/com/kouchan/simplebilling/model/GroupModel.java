package com.kouchan.simplebilling.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "group_name")
@NamedQuery(name = "GroupModel.findAll", query = "SELECT u FROM GroupModel u")
public class GroupModel {

	@Id
	@Column(name = "group_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long groupId;

	@Column(name = "group_name")
	private String groupName;

	@Column(name = "merchant_id")
	private Long merchantId;

	@Column(name = "is_deleted", columnDefinition = "TINYINT(1) default 0 ")
	private byte isDeleted;
   
/*	@OneToMany(mappedBy = "groupModel",cascade=CascadeType.ALL)
	private List<DeliveryBoyModel> deliveryBoyModel;*/
	
	public GroupModel() {

	}

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public void setMerchantId(Long merchantId) {
		this.merchantId = merchantId;
	}

	public Long getMerchantId() {
		return merchantId;
	}

	public byte getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(byte isDeleted) {
		this.isDeleted = isDeleted;
	}

	
}
