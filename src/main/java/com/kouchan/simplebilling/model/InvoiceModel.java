package com.kouchan.simplebilling.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "invoice_table")
public class InvoiceModel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "bill_id")
	private int billId;

	@Column(name = "merchant_id")
	private int merchantId;

	@Column(name = "merchant_address")
	private String merchantAddress;

	@Column(name = "customer_id")
	private Long customerId;

	@Column(name = "customer_name")
	private String customerName;

	@Column(name = "customer_number")
	private String customerNumber;

	@Column(name = "bill_number")
	private int billNumber;

	@Column(name = "bill_date")
	private String billDate;

	@Column(name = "bill_month")
	private String billMonth;

	@Column(name = "product_id")
	private int productid;

	@Column(name = "quantity")
	private int quantity;

	@Column(name = "rate_per_qny")
	private Double rateperqty;

	@Column(name = "total")
	private Double total;

	@Column(name = "is_gst_applicable", columnDefinition = "TINYINT(1) default 0 ")
	private byte isgstApplicable;

	@Column(name = "per_gst")
	private int pergst;

	@Column(name = "sgst")
	private int sgst;

	@Column(name = "cgst")
	private int cgst;

	@Column(name = "igst")
	private int igst;

	@Column(name = "discount")
	private int discount;

	@Column(name = "advance_payment")
	private int advancePayment;

	@Column(name = "total_amount")
	private int totalAmount;

	@Column(name = "status_bill_rise")
	private int statusBillRise;

	@Column(name = "status_bill_payment")
	private int statusBillPayment;

	@Column(name = "m3_ref_number")
	private int m3RefNumber;

	public int getBillId() {
		return billId;
	}

	public void setBillId(int billId) {
		this.billId = billId;
	}

	public int getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(int merchantId) {
		this.merchantId = merchantId;
	}

	public String getMerchantAddress() {
		return merchantAddress;
	}

	public void setMerchantAddress(String merchantAddress) {
		this.merchantAddress = merchantAddress;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerNumber() {
		return customerNumber;
	}

	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}

	public int getBillNumber() {
		return billNumber;
	}

	public void setBillNumber(int billNumber) {
		this.billNumber = billNumber;
	}

	public String getBillDate() {
		return billDate;
	}

	public void setBillDate(String billDate) {
		this.billDate = billDate;
	}

	public String getBillMonth() {
		return billMonth;
	}

	public void setBillMonth(String billMonth) {
		this.billMonth = billMonth;
	}

	public int getProductid() {
		return productid;
	}

	public void setProductid(int productid) {
		this.productid = productid;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public Double getRateperqty() {
		return rateperqty;
	}

	public void setRateperqty(Double rateperqty) {
		this.rateperqty = rateperqty;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public byte getIsgstApplicable() {
		return isgstApplicable;
	}

	public void setIsgstApplicable(byte isgstApplicable) {
		this.isgstApplicable = isgstApplicable;
	}

	public int getPergst() {
		return pergst;
	}

	public void setPergst(int pergst) {
		this.pergst = pergst;
	}

	public int getSgst() {
		return sgst;
	}

	public void setSgst(int sgst) {
		this.sgst = sgst;
	}

	public int getCgst() {
		return cgst;
	}

	public void setCgst(int cgst) {
		this.cgst = cgst;
	}

	public int getIgst() {
		return igst;
	}

	public void setIgst(int igst) {
		this.igst = igst;
	}

	public int getDiscount() {
		return discount;
	}

	public void setDiscount(int discount) {
		this.discount = discount;
	}

	public int getAdvancePayment() {
		return advancePayment;
	}

	public void setAdvancePayment(int advancePayment) {
		this.advancePayment = advancePayment;
	}

	public int getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(int totalAmount) {
		this.totalAmount = totalAmount;
	}

	public int getStatusBillRise() {
		return statusBillRise;
	}

	public void setStatusBillRise(int statusBillRise) {
		this.statusBillRise = statusBillRise;
	}

	public int getStatusBillPayment() {
		return statusBillPayment;
	}

	public void setStatusBillPayment(int statusBillPayment) {
		this.statusBillPayment = statusBillPayment;
	}

	public int getM3RefNumber() {
		return m3RefNumber;
	}

	public void setM3RefNumber(int m3RefNumber) {
		this.m3RefNumber = m3RefNumber;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	

}
