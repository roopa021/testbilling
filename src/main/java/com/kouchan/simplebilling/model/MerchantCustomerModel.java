package com.kouchan.simplebilling.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "merchant_customer")
@NamedQuery(name = "MerchantCustomerModel.findAll", query = "select m from MerchantCustomerModel m")
public class MerchantCustomerModel 
{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private int merchcustid;
	
	@Column(name="address")
	private String address;
	
	@ManyToOne
//	@JoinColumn(name = "customer_id,fetch=FetchType.EAGER,cascade = CascadeType.ALL")
	@JoinColumn(name = "customer_id")
	private CustomerModel  customerModel;
	
	@ManyToOne
	@JoinColumn(name = "merchant_id")
	private MerchantModel merchantModel;
	
	public int getMerchcustid() {
		return merchcustid;
	}

	public void setMerchcustid(int merchcustid) {
		this.merchcustid = merchcustid;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public CustomerModel getCustomerModel() {
		return customerModel;
	}

	public void setCustomerModel(CustomerModel customerModel) {
		this.customerModel = customerModel;
	}

	public MerchantModel getMerchantModel() {
		return merchantModel;
	}

	public void setMerchantModel(MerchantModel merchantModel) {
		this.merchantModel = merchantModel;
	}
}
