package com.kouchan.simplebilling.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "merchant")
@NamedQuery(name = "MerchantModel.findAll", query = "select m from MerchantModel m")
public class MerchantModel {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "merchant_id")
	private int id;

	@Column(name = "merchant_name")
	private String name;

	@Column(name = "mobile_number")
	private String mobileNumber;

	@Column(name = "aadhar_card")
	private String aadharNumber;

	@Column(name = "pan_card")
	private String panNumber;

	@Column(name = "gst_number")
	private String gstNumber;

	@Column(name = "password")
	private String password;

	@Column(name = "address")
	private String address;

	@Column(name = "created_on")
	/*
	 * @CreatedDate
	 * 
	 * @Temporal(TemporalType.TIMESTAMP)
	 * 
	 * @JsonFormat(locale = "en-IN", shape = JsonFormat.Shape.STRING, pattern =
	 * "yyyy-MM-dd HH:mm", timezone = "GMT+5:30")
	 */
	private Date created_on;

	@Column(name = "otp")
	public int otp;

	@Column(name = "created_by")
	private String created_by;

	@OneToMany(mappedBy = "merchantModel"/* ,cascade=CascadeType.ALL */)
	private List<MerchantCustomerModel> merchantCustomerModel;

	@OneToMany(mappedBy = "merchantModel")
	private List<ProductListModel> productListModel;

	@OneToMany(mappedBy = "merchantModel", cascade = CascadeType.ALL)
	private List<DeliveryBoyModel> DeliveryBoyModel;

	@OneToMany(mappedBy = "merchantModel")
	private List<FrequentOrderRequestModel> frequentOrderRequestModel;

	@Column(name = "is_deleted", columnDefinition = "TINYINT(1) default 0 ")
	private byte isDeleted;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getAadharNumber() {
		return aadharNumber;
	}

	public void setAadharNumber(String aadharNumber) {
		this.aadharNumber = aadharNumber;
	}

	public String getPanNumber() {
		return panNumber;
	}

	public void setPanNumber(String panNumber) {
		this.panNumber = panNumber;
	}

	public String getGstNumber() {
		return gstNumber;
	}

	public void setGstNumber(String gstNumber) {
		this.gstNumber = gstNumber;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getCreated_on() {
		return created_on;
	}

	public void setCreated_on(Date created_on) {
		this.created_on = created_on;
	}

	public String getCreated_by() {
		return created_by;
	}

	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}

	public byte getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(byte isDeleted) {
		this.isDeleted = isDeleted;
	}

	public int getOtp() {
		return otp;
	}

	public void setOtp(int otp) {
		this.otp = otp;
	}

}
