package com.kouchan.simplebilling.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name="purpose")
@NamedQuery(name = "PurposeModel.findAll", query = "SELECT u FROM PurposeModel u")
public class PurposeModel {
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	@Column(name="purpose")
	private String pupose;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPupose() {
		return pupose;
	}

	public void setPupose(String pupose) {
		this.pupose = pupose;
	}

	@Override
	public String toString() {
		return "PurposeModel [id=" + id + ", pupose=" + pupose + "]";
	}

	
	
	


}
