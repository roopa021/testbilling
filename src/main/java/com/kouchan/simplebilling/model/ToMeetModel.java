package com.kouchan.simplebilling.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name="to_meet")
@NamedQuery(name = "ToMeetModel.findAll", query = "SELECT u FROM ToMeetModel u")
public class ToMeetModel {
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	@Column(name="to_meet")
	private String toMeet;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getToMeet() {
		return toMeet;
	}

	public void setToMeet(String toMeet) {
		this.toMeet = toMeet;
	}

	@Override
	public String toString() {
		return "ToMeet [id=" + id + ", toMeet=" + toMeet + "]";
	}
	
	
	

}
