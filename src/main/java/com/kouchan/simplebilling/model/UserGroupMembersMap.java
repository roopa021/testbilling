package com.kouchan.simplebilling.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.kouchan.simplebilling.data.UserGroupMapKey;

@Entity
@Table(name = "group_members_table")
@NamedQuery(name = "UserGroupMembersMap.findAll", query = "SELECT u FROM UserGroupMembersMap u")
@IdClass(value = UserGroupMapKey.class)
public class UserGroupMembersMap implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "group_member_id")
	private Long groupNameId;

	@Column(name = "group_Id")
	private Long groupId;

	@Id
	@Column(name = "customer_id")
	private Long groupMemberId;

	@Column(name = "customer_name")
	private String GroupMemberName;

	@Column(name = "customer_mobile")
	private String GroupMemberMobile;

	@Column(name = "merchnat_id")
	private Long merchnatId;

	@Column(name = "deliver_boy_id")
	private Long deliverBoyId;

	@Column(name = "is_deleted", columnDefinition = "TINYINT(1) default 0 ")
	private byte isDeleted;
	
	@Column(name = "is_assignd", columnDefinition = "TINYINT(1) default 0 ")
	private byte isAssign;
	

	public UserGroupMembersMap() {

	}

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public Long getGroupMemberId() {
		return groupMemberId;
	}

	public void setGroupMemberId(Long groupMemberId) {
		this.groupMemberId = groupMemberId;
	}

	public String getGroupMemberName() {
		return GroupMemberName;
	}

	public void setGroupMemberName(String groupMemberName) {
		GroupMemberName = groupMemberName;
	}

	public String getGroupMemberMobile() {
		return GroupMemberMobile;
	}

	public void setGroupMemberMobile(String groupMemberMobile) {
		GroupMemberMobile = groupMemberMobile;
	}

	public Long getMerchnatId() {
		return merchnatId;
	}

	public void setMerchnatId(Long merchnatId) {
		this.merchnatId = merchnatId;
	}

	public byte getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(byte isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Long getGroupNameId() {
		return groupNameId;
	}

	public void setGroupNameId(Long groupNameId) {
		this.groupNameId = groupNameId;
	}

	public Long getDeliverBoyId() {
		return deliverBoyId;
	}

	public Long setDeliverBoyId(Long deliverBoyId) {
		return this.deliverBoyId = deliverBoyId;
	}

	public byte getIsAssign() {
		return isAssign;
	}

	public void setIsAssign(byte isAssign) {
		this.isAssign = isAssign;
	}

	@Override
	public String toString() {
		return "UserGroupMembersMap [groupNameId=" + groupNameId + ", groupId=" + groupId + ", groupMemberId="
				+ groupMemberId + ", GroupMemberName=" + GroupMemberName + ", GroupMemberMobile=" + GroupMemberMobile
				+ ", merchnatId=" + merchnatId + ", deliverBoyId=" + deliverBoyId + ", isDeleted=" + isDeleted
				+ ", isAssign=" + isAssign + "]";
	}



	
	

}