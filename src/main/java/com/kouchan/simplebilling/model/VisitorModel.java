package com.kouchan.simplebilling.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "visitor_details")
@NamedQuery(name = "VisitorModel.findAll", query = "SELECT v FROM VisitorModel v")
public class VisitorModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "name")
	private String name;

	@Column(name = "mobile_number")
	private String mobileNumber;

	@Column(name = "to_meet")
	private String toMeet;

	@Column(name = "purpose")
	private String purpose;

	@Column(name = "otp")
	private int otp;

	@Column(name = "visit_Date")
	private String visitDate; 

	@Column(name = "visit_Time")
	private String visitTime;
	
	/*@Column(name="excel_data")
	private String excleData;*/

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getToMeet() {
		return toMeet;
	}

	public void setToMeet(String toMeet) {
		this.toMeet = toMeet;
	}

	public String getPurpose() {
		return purpose;
	}

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	public int getOtp() {
		return otp;
	}

	public void setOtp(int otp) {
		this.otp = otp;
	}

	public String getVisitDate() {
		return visitDate;
	}

	public void setVisitDate(String visitDate) {
		this.visitDate = visitDate;
	}

	public String getVisitTime() {
		return visitTime;
	}

	public void setVisitTime(String visitTime) {
		this.visitTime = visitTime;
	}

	@Override
	public String toString() {
		return "VisitorModel [id=" + id + ", name=" + name + ", mobileNumber=" + mobileNumber + ", toMeet=" + toMeet
				+ ", purpose=" + purpose + ", otp=" + otp + ", visitDate=" + visitDate + ", visitTime=" + visitTime
				+ "]";
	}

	
	
}
