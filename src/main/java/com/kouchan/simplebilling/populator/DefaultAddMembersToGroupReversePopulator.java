package com.kouchan.simplebilling.populator;

import java.util.List;

import org.springframework.stereotype.Component;

import com.kouchan.simplebilling.dto.CustomerDto;
import com.kouchan.simplebilling.model.CustomerModel;
import com.kouchan.simplebilling.request.GroupRequest;

@Component(value = "defaultAddMembersToGroupReversePopulator")
public class DefaultAddMembersToGroupReversePopulator implements Populator<GroupRequest, List<CustomerModel>>
{

    @Override
    public void populate(final GroupRequest groupRequest, final List<CustomerModel> customerModels)
    {
        for (final CustomerDto customerDto : groupRequest.getListOfCustomer())
        {
            final CustomerModel customerModel = new CustomerModel();
            customerModel.setName(customerDto.getName());
            customerModel.setMobileNumber(customerDto.getMobileNumber());
            customerModels.add(customerModel);
        }
    }

}
