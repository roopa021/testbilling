package com.kouchan.simplebilling.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.kouchan.simplebilling.model.CustomerCancelReasonModel;

@Repository
public interface CustomerCancelReasonRepo extends JpaRepository<CustomerCancelReasonModel, Integer>
{
	
	

}
