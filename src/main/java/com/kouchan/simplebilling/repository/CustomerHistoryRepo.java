package com.kouchan.simplebilling.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.kouchan.simplebilling.model.InvoiceModel;

public interface CustomerHistoryRepo extends JpaRepository<InvoiceModel, Integer>{

	/*@Query("SELECT m from CustomerHistoryModel m WHERE m.customerId =?1")
	List findAllCustomer(int cid);

	@Query("SELECT m from CustomerHistoryModel m WHERE m.customerId =?1 and m.startDate=?2 and m.endDate")
	List<InvoiceModel> findByCustomerIdAndDates(int customerId, String startDate, String endDate);
*/
}
