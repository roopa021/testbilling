package com.kouchan.simplebilling.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.kouchan.simplebilling.model.CustomerOrderDetails;

@Repository
public interface CustomerOrderDetailsRepo extends JpaRepository<CustomerOrderDetails, Integer> 
{
	
	public CustomerOrderDetails findByOrderId(int i);

	@Query("SELECT m from CustomerOrderDetails m WHERE  m.merchantModel.id=?1  and m.customerModel.id=?2 and m.productListModel.product_id=?3 and m.month=?4")
	public CustomerOrderDetails getCustomerOrderDetails(int merchantID, Long customerID, int productid,
			String billMonth);

	@Query("SELECT m from CustomerOrderDetails m WHERE  m.month=?1 and m.merchantModel.id=?2  and m.customerModel.id=?3")
	public CustomerOrderDetails findbyMonth(String billMonth, int merchantID, Long customerID);

	@Query("SELECT m from CustomerOrderDetails m WHERE  m.month=?1")
	public CustomerOrderDetails findBy1(String billMonth);

	@Query("SELECT m from CustomerOrderDetails m WHERE  m.merchantModel.id=?1  and m.customerModel.id=?2 and  m.month=?3")
	public CustomerOrderDetails getorderSchedule(int merchantId, long customerId, String billMonth);

	@Query("SELECT m from CustomerOrderDetails m WHERE  m.merchantModel.id=?1  and m.customerModel.id=?2 and  m.month=?3")
	public List<CustomerOrderDetails> getorderUpcomingDeliveryDetails(int merchantId, long customerId,
			String billMonth);



}
 