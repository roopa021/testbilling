package com.kouchan.simplebilling.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.kouchan.simplebilling.model.CustomerModel;

@Repository
public interface CustomerRepository extends JpaRepository<CustomerModel, Integer>
{
	
	public CustomerModel findByMobileNumber(String mobileNumber);

	@Query("SELECT m from CustomerModel m WHERE m.mobileNumber =?1 and m.password=?2")
	public CustomerModel findByMobileNumberandPassword(String mobileNumber, String password);

	@Query("SELECT u FROM CustomerModel u WHERE u.id = ?1")
	public CustomerModel findById(Long id);



	
	
	
	
	
}
