package com.kouchan.simplebilling.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.kouchan.simplebilling.model.DeliveryBoyModel;

@Repository
public interface DeliveryBoyRepository extends JpaRepository<DeliveryBoyModel, Long> {

	@Query("SELECT u FROM DeliveryBoyModel u WHERE u.mobileNumber = ?1")
	public DeliveryBoyModel findByMobile(String mobile);

	@Query("SELECT u FROM DeliveryBoyModel u WHERE u.id = ?1 AND  u.mobileNumber = ?2")
	public DeliveryBoyModel findByMerchantIdAndMobileNumber(int id, String mobileNumber);

	@Query("SELECT u FROM DeliveryBoyModel u WHERE u.id = ?1")
	public DeliveryBoyModel findOne(int id);

	/*@Query("SELECT u FROM DeliveryBoyModel u WHERE u.id = ?1 AND  u.groupModel = ?2")
	public DeliveryBoyModel findByMerchantIdAndMobileNumber(long id, Long groupId);
*/
	@Query("SELECT u FROM DeliveryBoyModel u WHERE u.mobileNumber = ?1 AND  u.password = ?2")
	public DeliveryBoyModel findByMobileNumberandPassword(String mobileNumber, String password);

	@Query("SELECT u FROM DeliveryBoyModel u WHERE u.merchantModel.id = ?1")
	public List<DeliveryBoyModel> findById(int merchant_id);

	@Query("SELECT u FROM DeliveryBoyModel u WHERE u.merchantModel.id = ?1 and u.isAvaliable=?2")
	public List<DeliveryBoyModel> findByMerchatIdAndStatus(int merchant_id, byte status);

}
