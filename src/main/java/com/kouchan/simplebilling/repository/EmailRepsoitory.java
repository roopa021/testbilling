package com.kouchan.simplebilling.repository;

import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.kouchan.simplebilling.model.EmailTemplateModel;

@Repository
public interface EmailRepsoitory extends DataTablesRepository<EmailTemplateModel, Integer> {

	/*
	 * @Query("select e from EmailTemplateModel e where e.templateCode = ?1")
	 * EmailTemplateModel findTemplateByCode(String templateCode);
	 */

	@Query("select e from EmailTemplateModel e where e.templateCode=:templateCode")
	EmailTemplateModel findByTemplateCode(@Param("templateCode") String templateCode);

}
