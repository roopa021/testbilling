package com.kouchan.simplebilling.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.kouchan.simplebilling.model.ForgotPasswordModel;

@Repository
public interface ForgotPasswordRepository extends JpaRepository<ForgotPasswordModel, Integer>{

	@Query("SELECT u from ForgotPasswordModel u WHERE u.mobileNumber =?1 and u.otp=?2")
	ForgotPasswordModel findByMobileAndOtp(String mobile, int otp);

}
