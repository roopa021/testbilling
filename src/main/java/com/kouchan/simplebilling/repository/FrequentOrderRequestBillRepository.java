package com.kouchan.simplebilling.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.kouchan.simplebilling.model.FrequentOrderBillModel;

@Repository
public interface FrequentOrderRequestBillRepository extends JpaRepository<FrequentOrderBillModel,Integer>{

	
	@Query("SELECT m from FrequentOrderBillModel m WHERE m.customer_id =?1")
	FrequentOrderBillModel findById(Long customer_id);

	@Query("SELECT m from FrequentOrderBillModel m WHERE m.merchat_id=?1 and m.customer_id =?2")
	FrequentOrderBillModel findByCustomerIdAndMerchnatId(int merchat_id, long customer_id);



}
