package com.kouchan.simplebilling.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.kouchan.simplebilling.model.DeliveryBoyModel;
import com.kouchan.simplebilling.model.FrequentOrderBillModel;
import com.kouchan.simplebilling.model.FrequentOrderRequestModel;

@Repository
public interface FrequentOrderRequestRepository extends JpaRepository<FrequentOrderRequestModel, Long> {



	@Query("SELECT u from FrequentOrderRequestModel u WHERE u.productListModel.product_id=?1")
	FrequentOrderRequestModel findById(int productid);

	@Query("SELECT u from FrequentOrderRequestModel u WHERE u.merchantModel.id =?1 and u.customerModel.id=?2 and u.productListModel.product_id=?3 and u.date=?4")
	FrequentOrderRequestModel findByMerchantProductCustomerID(int merchantId, long customerId, int productid, String date);

	

	

	

}
