package com.kouchan.simplebilling.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.kouchan.simplebilling.model.UserGroupMembersMap;

@Repository
public interface GroupMapRepository extends JpaRepository<UserGroupMembersMap, Long> {

	@Query("select u from UserGroupMembersMap u where u.groupId = ?1  AND u.isDeleted = ?2")
	List<UserGroupMembersMap> findByGroupId(Long groupId, byte activeStatus);

	@Query("select u from UserGroupMembersMap u where u.groupId = ?1 ")
	List<UserGroupMembersMap> findAll(Long groupId);

	@Query("select u from UserGroupMembersMap u where u.groupId = ?1")
	List<UserGroupMembersMap> findByGroupId(Long groupId);


	@Query("select u from UserGroupMembersMap u where u.groupId = ?1")
	List<UserGroupMembersMap> findByGroupId1(Long groupId);


	@Query("select u from UserGroupMembersMap u where u.id=?1 AND u.groupId = ?2")
	List<UserGroupMembersMap> findByGroupIdAndMerchantId(int id, Long groupId);

	@Query("select u from UserGroupMembersMap u where u.id=?1 AND u.groupId = ?2")
	List<UserGroupMembersMap> findByGroupIdAndMerchantId1(Long merchantId, Long groupId);

}
