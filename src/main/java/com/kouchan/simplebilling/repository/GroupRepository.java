package com.kouchan.simplebilling.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.kouchan.simplebilling.model.CustomerModel;
import com.kouchan.simplebilling.model.GroupModel;

@Repository
public interface GroupRepository extends JpaRepository<GroupModel, Long> {

	/*
	 * @Query("select g from GroupModel g where g.groupId = ?1 AND g.groupName= ?2")
	 * GroupModel findByGroupIdAndGroupName(Integer groupId, String groupName);
	 */
	@Query("select u from GroupModel u where u.groupId = ?1 ")
	CustomerModel findOneMobile(String mobileNumber);

	@Query("select u from GroupModel u where u.groupId = ?1 AND u.groupName= ?2")
	GroupModel findByGroupIdAndGroupName(Long groupId, String groupName);

	@Query("select u from GroupModel u where u.groupName = ?1 ")
	GroupModel findByGroupIdAndGroupName(String groupName);
	
	
	//new quries after chaning db

	@Query("select u from GroupModel u where u.merchantId = ?1 AND u.groupName = ?2 ")
	GroupModel findBymerchnatIdAndGroupName(long merchantId, String groupName);

	@Query("select u from GroupModel u where u.merchantId = ?1")
	List<GroupModel> findByGroupMerchnatId(Long merchantId);
    
	@Query("select u from GroupModel u where u.groupId = ?1")
	List<GroupModel> findByGroupId(Long groupId);

	@Query("select u from GroupModel u where u.groupName = ?1 ")
	GroupModel findByGroupName(String groupName);

	@Query("select u from GroupModel u where u.groupId = ?1 AND u.isDeleted = ?2")
	List<GroupModel> findByGroupIdAndIsDeleted(Long groupId, byte activeStatus);

	@Query("select u from GroupModel u where u.groupId = ?1 AND u.isDeleted = ?2")
	List<GroupModel> findByGroupOwnerId(Long groupId, byte activeStatus);

	

}
