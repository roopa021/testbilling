package com.kouchan.simplebilling.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.kouchan.simplebilling.model.InvoiceModel;

public interface InvoiceRepo extends JpaRepository<InvoiceModel, Integer> {

}
