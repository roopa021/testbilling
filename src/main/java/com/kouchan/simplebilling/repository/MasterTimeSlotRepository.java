package com.kouchan.simplebilling.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.kouchan.simplebilling.model.MasterTimeSlotModel;
import com.kouchan.simplebilling.model.UserGroupMembersMap;

@Repository
public interface MasterTimeSlotRepository extends JpaRepository<MasterTimeSlotModel, Integer>{

}
