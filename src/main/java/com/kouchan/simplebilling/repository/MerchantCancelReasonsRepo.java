package com.kouchan.simplebilling.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.kouchan.simplebilling.model.MerchantCancelReasonModel;


@Repository
public interface MerchantCancelReasonsRepo extends JpaRepository<MerchantCancelReasonModel, Integer> {

}
