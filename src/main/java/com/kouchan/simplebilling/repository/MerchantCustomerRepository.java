package com.kouchan.simplebilling.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.kouchan.simplebilling.model.MerchantCustomerModel;


@Repository
public interface MerchantCustomerRepository extends JpaRepository<MerchantCustomerModel, Integer>
{

	@Query("SELECT m from MerchantCustomerModel m WHERE m.merchantModel.id =?1")
	List<MerchantCustomerModel> findById(int id);

}
