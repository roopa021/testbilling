package com.kouchan.simplebilling.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.kouchan.simplebilling.model.MerchantModel;

@Repository
public interface MerchantRepository extends JpaRepository<MerchantModel, Integer> {

	@Query("SELECT m from MerchantModel m WHERE m.mobileNumber =?1")
	MerchantModel findByMobile(String mobile);

	@Query("SELECT m from MerchantModel m WHERE m.id =?1")
	MerchantModel findById(int id);

	@Query("SELECT m from MerchantModel m WHERE m.mobileNumber =?1 and m.password=?2")
	MerchantModel findByMobileNumberandPassword(String mobileNumber, String password);

	@Query("SELECT m from MerchantModel m WHERE m.password =?1")
	MerchantModel findByPassword(String password);

	@Query("SELECT m from MerchantModel m WHERE m.mobileNumber =?1 and m.otp=?2")
	MerchantModel findByMobileAndOtp(String mobile, int otp);


}
