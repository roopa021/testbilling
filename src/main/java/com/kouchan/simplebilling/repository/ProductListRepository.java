package com.kouchan.simplebilling.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.kouchan.simplebilling.model.ProductListModel;

@Repository
public interface ProductListRepository extends JpaRepository<ProductListModel, Integer>
{

	@Query("SELECT m.productName from ProductListModel m WHERE m.merchantModel.id =?1")
	List <ProductListModel> productList(int id);
	
	@Query("SELECT m.subProduct  from ProductListModel m WHERE m.merchantModel.id=?1")
	List<ProductListModel> subProductList(int id);

	@Query("SELECT m from ProductListModel m WHERE m.merchantModel.id=?1 and m.subProduct=?2")
    ProductListModel findby(int merchantId, String subproduct);

	@Query("SELECT m.category from ProductListModel m WHERE m.merchantModel.id=:merchId")
	List<ProductListModel> getAllCategory(@Param(value ="merchId") int merchId);

	@Query("SELECT m.productName from ProductListModel m WHERE m.merchantModel.id=:merchId")
	List<ProductListModel> getAllProducts(@Param(value ="merchId")int merchId);

	@Query("SELECT m from ProductListModel m WHERE m.merchantModel.id=:merchId")
	List<ProductListModel> getAllSubCategory(@Param(value ="merchId") int merchId);

	
	@Query("SELECT m from ProductListModel m WHERE  m.category=:category ")
	List<ProductListModel> getAllCategory1(@Param(value ="category")String category);

	@Query("SELECT m from ProductListModel m WHERE m.merchantModel.id=:merchantId and m.subProduct=:subProduct ")
	List<ProductListModel> subProductList1(@Param(value ="merchantId")int merchantId, @Param(value ="subProduct")String subProduct);

	
	@Query("SELECT m FROM ProductListModel m WHERE m.merchantModel.id = ?1")
	List<ProductListModel> findById(@Param(value ="merchantId")int merchantId);

	@Query("SELECT m from ProductListModel m WHERE  m.merchantModel.id=:merchant_id")
	List<ProductListModel> findBym(@Param(value ="merchant_id")int  merchant_id);

	
}
