package com.kouchan.simplebilling.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.kouchan.simplebilling.model.PurposeModel;

@Repository
public interface PurposeRepository extends JpaRepository<PurposeModel, Integer>{

}
