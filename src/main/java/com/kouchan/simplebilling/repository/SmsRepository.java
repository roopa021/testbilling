package com.kouchan.simplebilling.repository;

import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.kouchan.simplebilling.model.SmsModel;

public interface SmsRepository extends DataTablesRepository <SmsModel, Long> {

	@Query("select s from SmsModel s where s.templateCode=:templateCode")
	SmsModel findByTemplateCode(@Param("templateCode") String templateCode);

}
