package com.kouchan.simplebilling.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.kouchan.simplebilling.model.ToMeetModel;

@Repository
public interface ToMeetRepository extends JpaRepository<ToMeetModel, Integer> {

}
