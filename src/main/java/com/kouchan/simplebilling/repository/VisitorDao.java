package com.kouchan.simplebilling.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.kouchan.simplebilling.model.VisitorModel;


@Repository
public interface VisitorDao extends JpaRepository <VisitorModel,Integer>{

	

	/*@Query("SELECT v from VisitorModel v WHERE v.mobileNumber =?1 and v.otp=?2")
	VisitorModel findByMobileAndOTP(String mobileNumber, int otp);
*/
	@Query("SELECT v FROM VisitorModel v WHERE v.otp=?1")
	VisitorModel findByMobileAndOTP(int otp);

	/*@Query("SELECT v FROM VisitorModel v WHERE v.mobileNumber=:mobileNumber AND v.otp=:otp ")
	VisitorModel findByMobileAndOTP(@Param("mobileNumber")String mobileNumber, @Param("otp"));
	*/

	@Query("SELECT v From VisitorModel v where v.mobileNumber= :mobileNumber and v.otp= :otp")
	VisitorModel findByMobileAndOTP(@Param(value ="mobileNumber")String mobileNumber,@Param(value ="otp") int otp);
}






