package com.kouchan.simplebilling.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.kouchan.simplebilling.model.VisitorOtpModel;

@Repository
public interface VisitorOtpDao extends JpaRepository<VisitorOtpModel, Integer>{

	
	@Query("SELECT v From VisitorOtpModel v where v.mobileNumber= :mobileNumber and v.otp= :otp")
	VisitorOtpModel findByMobileAndOTP(@Param(value ="mobileNumber")String mobileNumber,@Param(value ="otp") int otp);

}
