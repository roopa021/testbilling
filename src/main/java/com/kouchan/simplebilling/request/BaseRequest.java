package com.kouchan.simplebilling.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BaseRequest {
	
    @JsonProperty("customerId")
    private String customerId;
     
    @JsonProperty("mobileNumber")
    private String mobileNumber;


	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}


}
