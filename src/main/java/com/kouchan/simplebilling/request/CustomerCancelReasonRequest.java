package com.kouchan.simplebilling.request;

public class CustomerCancelReasonRequest {
	
	private String cancleReason;
	
	private Long customerId;

	public String getCancleReason() {
		return cancleReason;
	}

	public void setCancleReason(String cancleReason) {
		this.cancleReason = cancleReason;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	
	
	

}
