  package com.kouchan.simplebilling.request;

import java.util.List;

import com.kouchan.simplebilling.model.MerchantCustomerModel;

public class CustomerVO {

	private int id;
	private String name;
	private String mobileNumber;
	private String password;
	private String address;
	private String subproduct;
	private String unit;
	private int merchantId;
	private int productId;
	private String rate;
	private List<MerchantCustomerModel> merchantCustomerModel;

	/*
	 * 
	 * Getter and Setters
	 * 
	 */

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	

	public String getSubproduct() {
		return subproduct;
	}

	public void setSubproduct(String subproduct) {
		this.subproduct = subproduct;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public int getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(int merchantId) {
		this.merchantId = merchantId;
	}
	
	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	
	public String getRate() {
		return rate;
	}

	public void setRate(String rate) {
		this.rate = rate;
	}

	public List<MerchantCustomerModel> getMerchantCustomerModel() {
		return merchantCustomerModel;
	}

	public void setMerchantCustomerModel(List<MerchantCustomerModel> merchantCustomerModel) {
		this.merchantCustomerModel = merchantCustomerModel;
	}
	

}
