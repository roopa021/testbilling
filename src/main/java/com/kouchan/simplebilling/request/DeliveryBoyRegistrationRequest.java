package com.kouchan.simplebilling.request;


public class DeliveryBoyRegistrationRequest {

	private String name;

	private String mobileNumber;

	private int merchantId;

	private Long groupId;

	private String address;

	private String Password;
	
	private byte isAvaliable;
	
	private int deliveryBoyId;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public int getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(int merchantId) {
		this.merchantId = merchantId;
	}

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPassword() {
		return Password;
	}

	public void setPassword(String password) {
		Password = password;
	}

	public byte getIsAvaliable() {
		return isAvaliable;
	}

	public void setIsAvaliable(byte isAvaliable) {
		this.isAvaliable = isAvaliable;
	}

	public int getDeliveryBoyId() {
		return deliveryBoyId;
	}

	public void setDeliveryBoyId(int deliveryBoyId) {
		this.deliveryBoyId = deliveryBoyId;
	}

	@Override
	public String toString() {
		return "DeliveryBoyRegistrationRequest [name=" + name + ", mobileNumber=" + mobileNumber + ", merchantId="
				+ merchantId + ", groupId=" + groupId + ", address=" + address + ", Password=" + Password
				+ ", isAvaliable=" + isAvaliable + ", deliveryBoyId=" + deliveryBoyId + "]";
	}

  
	
}
