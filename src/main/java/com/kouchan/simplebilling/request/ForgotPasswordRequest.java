package com.kouchan.simplebilling.request;

public class ForgotPasswordRequest {
	
	private int merchat_id;
	
	private long customer_id;
	
	private int deliveryBoy_id;
	
	private String mobileNumber;
	
	private String newPassword;
	
	private String confirmPassword;

	public int getMerchat_id() {
		return merchat_id;
	}

	public void setMerchat_id(int merchat_id) {
		this.merchat_id = merchat_id;
	}

	public long getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(long customer_id) {
		this.customer_id = customer_id;
	}

	public int getDeliveryBoy_id() {
		return deliveryBoy_id;
	}

	public void setDeliveryBoy_id(int deliveryBoy_id) {
		this.deliveryBoy_id = deliveryBoy_id;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	@Override
	public String toString() {
		return "ForgotPasswordRequest [merchat_id=" + merchat_id + ", customer_id=" + customer_id + ", deliveryBoy_id="
				+ deliveryBoy_id + ", mobileNumber=" + mobileNumber + ", newPassword=" + newPassword
				+ ", confirmPassword=" + confirmPassword + "]";
	}
	
	
	

	

}
