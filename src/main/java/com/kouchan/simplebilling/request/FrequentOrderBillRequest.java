package com.kouchan.simplebilling.request;

public class FrequentOrderBillRequest {

	private int merchant_Id;

	private int product_Id;

	private Long customer_Id;

	private int deliveryBoyId;

	private int quantity;

	private String date;

	private String timeSlot;

	private int bill_number;

	private String bill_date;

	private String bill_month;

	private byte isgstApplicable;

	private double pergst;

	private int sgst;

	private int cgst;

	public int getMerchant_Id() {
		return merchant_Id;
	}

	public void setMerchant_Id(int merchant_Id) {
		this.merchant_Id = merchant_Id;
	}

	public int getProduct_Id() {
		return product_Id;
	}

	public void setProduct_Id(int product_Id) {
		this.product_Id = product_Id;
	}

	public Long getCustomer_Id() {
		return customer_Id;
	}

	public void setCustomer_Id(Long customer_Id) {
		this.customer_Id = customer_Id;
	}

	public int getDeliveryBoyId() {
		return deliveryBoyId;
	}

	public void setDeliveryBoyId(int deliveryBoyId) {
		this.deliveryBoyId = deliveryBoyId;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTimeSlot() {
		return timeSlot;
	}

	public void setTimeSlot(String timeSlot) {
		this.timeSlot = timeSlot;
	}

	public int getBill_number() {
		return bill_number;
	}

	public void setBill_number(int bill_number) {
		this.bill_number = bill_number;
	}

	public String getBill_date() {
		return bill_date;
	}

	public void setBill_date(String bill_date) {
		this.bill_date = bill_date;
	}

	public String getBill_month() {
		return bill_month;
	}

	public void setBill_month(String bill_month) {
		this.bill_month = bill_month;
	}

	public byte getIsgstApplicable() {
		return isgstApplicable;
	}

	public void setIsgstApplicable(byte isgstApplicable) {
		this.isgstApplicable = isgstApplicable;
	}

	public double getPergst() {
		return pergst;
	}

	public void setPergst(double pergst) {
		this.pergst = pergst;
	}

	public int getSgst() {
		return sgst;
	}

	public void setSgst(int sgst) {
		this.sgst = sgst;
	}

	public int getCgst() {
		return cgst;
	}

	public void setCgst(int cgst) {
		this.cgst = cgst;
	}

}
