package com.kouchan.simplebilling.request;

import java.util.ArrayList;
import java.util.List;

import com.kouchan.simplebilling.dto.CustomerDto;

public class GroupRequest extends BaseRequest {

	private String groupName;

	private Long merchantId;

	List<CustomerDto> listOfCustomer = new ArrayList<CustomerDto>();

	private int deliveryBoyId;

	private byte assign;

	// private MerchantModel merchantModel;

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public Long getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(Long merchantId) {
		this.merchantId = merchantId;
	}

	public List<CustomerDto> getListOfCustomer() {
		return listOfCustomer;
	}

	public void setListOfCustomer(List<CustomerDto> listOfCustomer) {
		this.listOfCustomer = listOfCustomer;
	}

	public int getDeliveryBoyId() {
		return deliveryBoyId;
	}

	public void setDeliveryBoyId(int deliveryBoyId) {
		this.deliveryBoyId = deliveryBoyId;
	}

	public byte getAssign() {
		return assign;
	}

	public void setAssign(byte assign) {
		this.assign = assign;
	}

	@Override
	public String toString() {
		return "GroupRequest [groupName=" + groupName + ", merchantId=" + merchantId + ", listOfCustomer="
				+ listOfCustomer + ", deliveryBoyId=" + deliveryBoyId + ", assign=" + assign + "]";
	}

	
}
