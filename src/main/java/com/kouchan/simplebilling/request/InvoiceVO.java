package com.kouchan.simplebilling.request;

public class InvoiceVO {
	
	private int billId;

	private int merchantId;

	private long customerId;

	private String merchantAddress;

	private String customerName;

	private int billNumber;

	private String billDate;

	private String billMonth;
	
	private String month;

	private int productid;

	private int quantity;

	private int rateperqty;

	private int total;

	private byte isgstApplicable;

	private int pergst;

	private int sgst;

	private int cgst;

	private int igst;

	private int discount;

	private int advancePayment;

	private int totalAmount;

	private int statusBillRise;

	private int statusBillPayment;

	private int m3RefNumber;

	public int getBillId() {
		return billId;
	}

	public void setBillId(int billId) {
		this.billId = billId;
	}

	public int getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(int merchantId) {
		this.merchantId = merchantId;
	}

	public long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}

	public String getMerchantAddress() {
		return merchantAddress;
	}

	public void setMerchantAddress(String merchantAddress) {
		this.merchantAddress = merchantAddress;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public int getBillNumber() {
		return billNumber;
	}

	public void setBillNumber(int billNumber) {
		this.billNumber = billNumber;
	}

	public String getBillDate() {
		return billDate;
	}

	public void setBillDate(String billDate) {
		this.billDate = billDate;
	}

	public String getBillMonth() {
		return billMonth;
	}

	public void setBillMonth(String billMonth) {
		this.billMonth = billMonth;
	}

	public int getProductid() {
		return productid;
	}

	public void setProductid(int productid) {
		this.productid = productid;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getRateperqty() {
		return rateperqty;
	}

	public void setRateperqty(int rateperqty) {
		this.rateperqty = rateperqty;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public byte getIsgstApplicable() {
		return isgstApplicable;
	}

	public void setIsgstApplicable(byte isgstApplicable) {
		this.isgstApplicable = isgstApplicable;
	}

	public int getPergst() {
		return pergst;
	}

	public void setPergst(int pergst) {
		this.pergst = pergst;
	}

	public int getSgst() {
		return sgst;
	}

	public void setSgst(int sgst) {
		this.sgst = sgst;
	}

	public int getCgst() {
		return cgst;
	}

	public void setCgst(int cgst) {
		this.cgst = cgst;
	}

	public int getIgst() {
		return igst;
	}

	public void setIgst(int igst) {
		this.igst = igst;
	}

	public int getDiscount() {
		return discount;
	}

	public void setDiscount(int discount) {
		this.discount = discount;
	}

	public int getAdvancePayment() {
		return advancePayment;
	}

	public void setAdvancePayment(int advancePayment) {
		this.advancePayment = advancePayment;
	}

	public int getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(int totalAmount) {
		this.totalAmount = totalAmount;
	}

	public int getStatusBillRise() {
		return statusBillRise;
	}

	public void setStatusBillRise(int statusBillRise) {
		this.statusBillRise = statusBillRise;
	}

	public int getStatusBillPayment() {
		return statusBillPayment;
	}

	public void setStatusBillPayment(int statusBillPayment) {
		this.statusBillPayment = statusBillPayment;
	}

	public int getM3RefNumber() {
		return m3RefNumber;
	}

	public void setM3RefNumber(int m3RefNumber) {
		this.m3RefNumber = m3RefNumber;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}
	
	
	
	

}
