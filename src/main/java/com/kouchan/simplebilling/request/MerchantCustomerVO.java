package com.kouchan.simplebilling.request;

import com.kouchan.simplebilling.model.CustomerModel;
import com.kouchan.simplebilling.model.MerchantModel;

public class MerchantCustomerVO {

	private int merchcustid;

	private String address;

	private CustomerModel customerModel;

	private MerchantModel merchantModel;

	public int getMerchcustid() {
		return merchcustid;
	}

	public void setMerchcustid(int merchcustid) {
		this.merchcustid = merchcustid;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public CustomerModel getCustomerModel() {
		return customerModel;
	}

	public void setCustomerModel(CustomerModel customerModel) {
		this.customerModel = customerModel;
	}

	public MerchantModel getMerchantModel() {
		return merchantModel;
	}

	public void setMerchantModel(MerchantModel merchantModel) {
		this.merchantModel = merchantModel;
	}

}
