package com.kouchan.simplebilling.request;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class MerchantRegistrationRequest {

	private String name;

	@Pattern(regexp = "[0-9]{10}",message="Invalid Mobile Number")
	private String mobileNumber;

	@Size(min=12,max=12)
	private String aadharNumber;

	@Size(min=6,max=8)
	private String password;

	@Pattern(regexp = "[A-Z]{5}[0-9]{4}[A-Z]{1}",message="Invalid Pan Number")
	private String panNumber;

	private String gstNumber;

	private String address;

	private String created_on;

	private String created_by;

	private byte isDeleted;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getAadharNumber() {
		return aadharNumber;
	}

	public void setAadharNumber(String aadharNumber) {
		this.aadharNumber = aadharNumber;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPanNumber() {
		return panNumber;
	}

	public void setPanNumber(String panNumber) {
		this.panNumber = panNumber;
	}

	public String getGstNumber() {
		return gstNumber;
	}

	public void setGstNumber(String gstNumber) {
		this.gstNumber = gstNumber;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCreated_on() {
		return created_on;
	}

	public void setCreated_on(String created_on) {
		this.created_on = created_on;
	}

	public String getCreated_by() {
		return created_by;
	}

	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}

	public byte getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(byte isDeleted) {
		this.isDeleted = isDeleted;
	}

	

	
}
