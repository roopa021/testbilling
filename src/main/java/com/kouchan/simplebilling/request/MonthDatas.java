package com.kouchan.simplebilling.request;

public class MonthDatas {

	private String day;
	private String status;
	private String quantity;

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public String getStatus() {
		return status;
	}

	public String setStatus(String status) {
		return this.status = status;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	@Override
	public String toString() {
		return "MonthDatas [day=" + day + ", status=" + status + ", quantity=" + quantity + "]";
	}
	

}
