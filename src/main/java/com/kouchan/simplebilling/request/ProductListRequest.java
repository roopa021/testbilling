package com.kouchan.simplebilling.request;

import java.sql.Date;
import java.util.List;

import org.springframework.stereotype.Component;

import com.kouchan.simplebilling.model.MerchantModel;

public class ProductListRequest {

	private String productName;
	private String subProduct;
	private String unit;
	private double rate;
	private String category;
	private Date startdate;
	private Date enddate;
	private int merchantid;
	
	private List<MerchantModel> merchantModel;
	

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getSubProduct() {
		return subProduct;
	}

	public void setSubProduct(String subProduct) {
		this.subProduct = subProduct;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public double getRate() {
		return rate;
	}

	public void setRate(double rate) {
		this.rate = rate;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Date getStartdate() {
		return startdate;
	}

	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}

	public Date getEnddate() {
		return enddate;
	}

	public void setEnddate(Date enddate) {
		this.enddate = enddate;
	}

	public int getMerchantid() {
		return merchantid;
	}

	public void setMerchantid(int merchantid) {
		this.merchantid = merchantid;
	}

	public List<MerchantModel> getMerchantModel() {
		return merchantModel;
	}

	public void setMerchantModel(List<MerchantModel> merchantModel) {
		this.merchantModel = merchantModel;
	}

}
