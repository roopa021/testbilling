package com.kouchan.simplebilling.request;

public class VisitorOtprequest {

	private String mobileNumber;

	private int otp;
	
	private String toMeeet;
	
	private String purpose;

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public int getOtp() {
		return otp;
	}

	public void setOtp(int otp) {
		this.otp = otp;
	}

	public String getToMeeet() {
		return toMeeet;
	}

	public void setToMeeet(String toMeeet) {
		this.toMeeet = toMeeet;
	}

	public String getPurpose() {
		return purpose;
	}

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	@Override
	public String toString() {
		return "VisitorOtprequest [mobileNumber=" + mobileNumber + ", otp=" + otp + ", toMeeet=" + toMeeet
				+ ", purpose=" + purpose + "]";
	}

	
}
