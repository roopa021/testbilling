package com.kouchan.simplebilling.request;

public class VistorRequest {

	private String name;
	
	private String mobileNumber;
	
	private String toMeet;
	
	private String purpose;
	
	private int Otp;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getToMeet() {
		return toMeet;
	}

	public void setToMeet(String toMeet) {
		this.toMeet = toMeet;
	}

	public String getPurpose() {
		return purpose;
	}

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	public int getOtp() {
		return Otp;
	}

	public void setOtp(int otp) {
		Otp = otp;
	}

	@Override
	public String toString() {
		return "VistorRequest [name=" + name + ", mobileNumber=" + mobileNumber + ", toMeet=" + toMeet + ", purpose="
				+ purpose + ", Otp=" + Otp + "]";
	}

	
 
	
}
