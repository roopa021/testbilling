package com.kouchan.simplebilling.response;

import java.util.List;

import com.kouchan.simplebilling.request.MonthDatas;

public class CustomerOrderDetailsResponse {

	private int merchantId;

	private long customerId;

	private String billMonth;

	List<MonthDatas> data;

	public List<MonthDatas> getData() {
		return data;
	}

	public void setData(List<MonthDatas> data) {
		this.data = data;
	}

	public int getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(int merchantId) {
		this.merchantId = merchantId;
	}

	public long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}

	public String getBillMonth() {
		return billMonth;
	}

	public void setBillMonth(String billMonth) {
		this.billMonth = billMonth;
	}

}
