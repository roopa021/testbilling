package com.kouchan.simplebilling.response;

public class DeleteResponse {
	
	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	

}
