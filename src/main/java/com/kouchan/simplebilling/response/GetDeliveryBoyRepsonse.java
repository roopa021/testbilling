package com.kouchan.simplebilling.response;

import java.util.List;

import com.kouchan.simplebilling.dto.DeliveryBoyDto;

public class GetDeliveryBoyRepsonse {
	
	private String status;
	
	private List<Object> data;
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<Object> getData() {
		return data;
	}

	public void setData(List<Object> data) {
		this.data = data;
	}

}
