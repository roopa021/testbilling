package com.kouchan.simplebilling.response;

import java.util.List;

import com.kouchan.simplebilling.coredata.CodeMessage;

public class GlobalErrorResponse {
	
	private String Status; 
	
	private List<CodeMessage> codeMessages;
	
	  public GlobalErrorResponse(final String status, final List<CodeMessage> codeMessages)
	    {
	        this.codeMessages = codeMessages;
	        this. Status = status;
	    }

	  
	    public List<CodeMessage> getCodeMessages()
	    {
	        return codeMessages;
	    }

	    public void setCodeMessages(final List<CodeMessage> codeMessages)
	    {
	        this.codeMessages = codeMessages;
	    }

	    
	    public GlobalErrorResponse()
	    {
	    	
	    }
	}
	


