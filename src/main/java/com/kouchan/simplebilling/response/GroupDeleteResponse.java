package com.kouchan.simplebilling.response;

public class GroupDeleteResponse {

	private String groupId;

	private String status;

	public String getStatus() {
		return status;
	}

	public void setStatus(final String status) {
		this.status = status;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

}
