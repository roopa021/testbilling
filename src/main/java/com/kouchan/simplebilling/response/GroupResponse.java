package com.kouchan.simplebilling.response;

import java.util.List;


import com.kouchan.simplebilling.model.UserGroupMembersMap;

public class GroupResponse {

	private String status;

	private List<Object> data;

	private List<UserGroupMembersMap> data1;

	public String getStatus() {
		return status;
	}

	public void setStatus(final String status) {
		this.status = status;
	}

	public List<Object> getData() {
		return data;
	}

	public void setData(final List<Object> data) {
		this.data = data;
	}

	public List<UserGroupMembersMap> getData1() {
		return data1;
	}

	public void setData1(List<UserGroupMembersMap> data1) {
		this.data1 = data1;
	}

}
