package com.kouchan.simplebilling.response;

public class UpdateGroupNameResponse {
	
	
	private String groupName;
	
	private Long deliveryBoyId;

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public Long getDeliveryBoyId() {
		return deliveryBoyId;
	}

	public void setDeliveryBoyId(Long deliveryBoyId) {
		this.deliveryBoyId = deliveryBoyId;
	}
	
	

}
