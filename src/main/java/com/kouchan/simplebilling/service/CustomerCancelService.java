package com.kouchan.simplebilling.service;

import com.kouchan.simplebilling.exception.CustomerDetailssNotFoundException;
import com.kouchan.simplebilling.exception.SimplebillingException;
import com.kouchan.simplebilling.model.CustomerCancelReasonModel;
import com.kouchan.simplebilling.request.CustomerCancelReasonRequest;

public interface CustomerCancelService 
{

	CustomerCancelReasonModel cancelDelivery(CustomerCancelReasonRequest customerCancelResonRequest) throws  CustomerDetailssNotFoundException;
	

}
