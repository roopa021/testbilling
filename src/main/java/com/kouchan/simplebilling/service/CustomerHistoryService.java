/**
 * 
 */
package com.kouchan.simplebilling.service;

import java.util.List;

import com.kouchan.simplebilling.model.InvoiceModel;

/**
 * @author 
 *
 */
public interface CustomerHistoryService {

	List<InvoiceModel> getHistoryBetweenDate(int customerId, String startDate, String endDate);

}
