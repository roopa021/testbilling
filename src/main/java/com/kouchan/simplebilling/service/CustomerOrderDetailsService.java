package com.kouchan.simplebilling.service;

import com.kouchan.simplebilling.model.CustomerOrderDetails;
import com.kouchan.simplebilling.response.CustomerOrderDetailsResponse;

public interface CustomerOrderDetailsService {

	CustomerOrderDetails getCustomerOrderDetails(int id, Long long1, int productid, String billMonth);

	CustomerOrderDetails getCustomerOrderDetails(String billMonth);

	CustomerOrderDetails getOrderSchedle(int merchantId, long customerId, String billMonth);

	CustomerOrderDetailsResponse upcomingOrderDetails(int merchantId, long customerId, String billMonth);

	CustomerOrderDetailsResponse delivredOrderDetails(int merchantId, long customerId, String billMonth);

	CustomerOrderDetailsResponse canceledOrderDetails(int merchantId, long customerId, String billMonth);

}
