package com.kouchan.simplebilling.service;

import com.kouchan.simplebilling.model.ChangePasswordModel;
import com.kouchan.simplebilling.model.CustomerModel;
import com.kouchan.simplebilling.model.CustomerOrderDetails;
import com.kouchan.simplebilling.request.CustomerVO;

public interface CustomerService 
{

	CustomerModel isCustomerExist(String mobileNumber);
	
	CustomerModel getByCustomerId(int id);
	
	CustomerOrderDetails updateOrderDetails(int i,String date, byte status);
	
	CustomerModel updateCustomerNameAndNumber(CustomerModel customerModel, int id);

	CustomerModel saveNewCustomer(CustomerVO customerVO)throws Exception;
	
	CustomerOrderDetails customerOrderDetailssave(CustomerOrderDetails customerOrderDetails ) throws Exception;

	CustomerModel login(CustomerVO customerVO) throws Exception;

	int generateOtp(String mobile);

	CustomerVO validateOtp(String mobile, String otp);

	CustomerModel updatePassword(CustomerModel customerModel, ChangePasswordModel changePasswordModel) throws Exception;

	CustomerModel getByCustomerId(Long customer_id);

	
	
}
