package com.kouchan.simplebilling.service;



import com.kouchan.simplebilling.model.ChangePasswordModel;
import com.kouchan.simplebilling.model.DeliveryBoyModel;
import com.kouchan.simplebilling.model.MerchantModel;
import com.kouchan.simplebilling.request.DeliveryBoyRegistrationRequest;
import com.kouchan.simplebilling.response.DeleteResponse;
import com.kouchan.simplebilling.response.GetDeliveryBoyRepsonse;

public interface DeliveryBoyService {

	DeliveryBoyModel save(DeliveryBoyModel deliveryBoy, MerchantModel merchnatModel);

	DeliveryBoyModel getByDeliveryBoyId(int id);

	DeliveryBoyModel update(DeliveryBoyModel deliveryBoy, int id);

	DeleteResponse deleteDelivery(int id);

	DeliveryBoyModel getGroupId(int id);

	GetDeliveryBoyRepsonse getAllDeliveryBoys(int merchant_id, DeliveryBoyRegistrationRequest deliveryBoyRegistrationRequest);

	DeliveryBoyModel deliveryBoylogin(DeliveryBoyRegistrationRequest deliveryboyRegrequest);

	DeliveryBoyModel updatePassword(DeliveryBoyModel deliveryBoyModel, ChangePasswordModel changePasswordModel);

	DeliveryBoyModel deliveryBoyAvaliablity(DeliveryBoyRegistrationRequest deliveryBoyRegistrationRequest);



}
