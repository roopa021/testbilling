package com.kouchan.simplebilling.service;

import com.kouchan.simplebilling.data.EmailData;

public interface EmailService 
{

	void sendEmailByTemplateCode(EmailData emailData) throws Exception;
	

}
