package com.kouchan.simplebilling.service;

import com.kouchan.simplebilling.model.CustomerModel;
import com.kouchan.simplebilling.model.DeliveryBoyModel;
import com.kouchan.simplebilling.model.ForgotPasswordModel;
import com.kouchan.simplebilling.model.MerchantModel;
import com.kouchan.simplebilling.request.ForgotPasswordRequest;

public interface ForgotPasswordService {

	ForgotPasswordModel saveOtp(ForgotPasswordModel forgotPasswordModel);

	MerchantModel updatePassword(MerchantModel merchantModel, ForgotPasswordRequest forgotPasswordRequest) throws Exception;

	CustomerModel updatePassword(CustomerModel merchantModel, ForgotPasswordRequest forgotPasswordRequest);

	CustomerModel getByCustomerId(Long customer_id);

	DeliveryBoyModel updatePassword(DeliveryBoyModel merchantModel, ForgotPasswordRequest forgotPasswordRequest);


}
