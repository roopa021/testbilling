package com.kouchan.simplebilling.service;

import com.kouchan.simplebilling.exception.CustomerDetailssNotFoundException;
import com.kouchan.simplebilling.model.FrequentOrderBillModel;
import com.kouchan.simplebilling.request.FrequentOrderBillRequest;

public interface FrequentOrderRequestBillService {

	FrequentOrderBillModel getBillData(FrequentOrderBillRequest frequentOrder);

	FrequentOrderBillModel getCustomerBill(Long customer_id) throws CustomerDetailssNotFoundException;

	FrequentOrderBillModel updateBillStatus(FrequentOrderBillModel frequentOrderBillModel, int merchat_id);

}
