package com.kouchan.simplebilling.service;

import com.kouchan.simplebilling.model.FrequentOrderRequestModel;

public interface FrequentOrderRequestService {

	FrequentOrderRequestModel save(FrequentOrderRequestModel frequentOrderRequestModel);

}
