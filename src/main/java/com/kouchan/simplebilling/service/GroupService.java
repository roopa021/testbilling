package com.kouchan.simplebilling.service;

import com.kouchan.simplebilling.exception.CustomerDetailssNotFoundException;
import com.kouchan.simplebilling.request.GroupRequest;
import com.kouchan.simplebilling.response.GroupResponse;

public interface GroupService {

	GroupResponse createUserGroup(GroupRequest groupRequest) throws CustomerDetailssNotFoundException;

	GroupResponse addMembersToGroup(Long groupId, GroupRequest groupRequest) throws CustomerDetailssNotFoundException;



}
