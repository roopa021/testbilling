package com.kouchan.simplebilling.service;

import java.util.List;

import com.kouchan.simplebilling.exception.CustomerDetailssNotFoundException;
import com.kouchan.simplebilling.exception.GroupNotFoundException;
import com.kouchan.simplebilling.model.CustomerModel;
import com.kouchan.simplebilling.model.GroupModel;
import com.kouchan.simplebilling.request.GroupRequest;
import com.kouchan.simplebilling.response.GroupDeleteResponse;
import com.kouchan.simplebilling.response.GroupResponse;

public interface GroupService2 {

	//GroupResponse saveUserGroupService(long merchnatId, String groupName, List<CustomerModel> users);

	GroupResponse addMembersToGroupService2(Long valueOf, Long groupId, List<CustomerModel> users) throws CustomerDetailssNotFoundException;

	GroupResponse updateUserGroupNameService(Long groupId, String mobileNumber, String groupName,Long merchantId) throws GroupNotFoundException;

	GroupResponse getGroupDetailsService(Long merchantId, Long groupId) throws GroupNotFoundException;

	GroupResponse getAllGroupMemberDetailsService(Long groupId, Long merchantId) throws GroupNotFoundException;

	GroupDeleteResponse deleteUserGroupService(Long groupId);

	GroupResponse saveUserGroupService(Long valueOf, String groupName, int deliveryBoyId,
			List<CustomerModel> customerModels, GroupRequest groupRequest) throws CustomerDetailssNotFoundException;

	List<GroupModel> getAllGroupNames(Long merchant_id);

	GroupResponse assignDeliveryBoy(Long groupId, Long merchantId, GroupRequest groupRequest);




}
