package com.kouchan.simplebilling.service;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.kouchan.simplebilling.request.InvoiceVO;

public interface InvoiceService {

	String generateInvoice(InvoiceVO invoiceVO) throws JsonParseException, JsonMappingException, IOException;

	String udapteDeliveryStatus(InvoiceVO invoiceVO);

}
