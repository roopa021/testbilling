package com.kouchan.simplebilling.service;

import java.text.ParseException;
import java.util.Date;

import com.kouchan.simplebilling.model.MasterTimeSlotModel;
import com.kouchan.simplebilling.request.MasterTimeSlotRequest;

public interface MasterTimeSlotService {

	Date stringToDate(String startDate) throws ParseException;

	MasterTimeSlotModel createMasterSlot(int merchantId, Date startDate, MasterTimeSlotRequest masterTimeSlotRequest);

}
