package com.kouchan.simplebilling.service;

import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Service;

import com.kouchan.simplebilling.model.CustomerModel;
import com.kouchan.simplebilling.model.MerchantCustomerModel;

@Service
public interface MerchantCustomerService {

	MerchantCustomerModel save(MerchantCustomerModel merchantCustomerModel);

	Set<CustomerModel> getMerchnatDetails(int id);

}
