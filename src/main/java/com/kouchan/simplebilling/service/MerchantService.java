package com.kouchan.simplebilling.service;

import com.kouchan.simplebilling.model.ChangePasswordModel;
import com.kouchan.simplebilling.model.MerchantModel;
import com.kouchan.simplebilling.request.MerchantRegistrationRequest;
import com.kouchan.simplebilling.response.DeleteResponse;

public interface MerchantService {

	MerchantModel save(MerchantModel merchant) throws Exception;

	MerchantModel getByMerchantId(int id);

	MerchantModel update12(MerchantModel merchantModel, int id) throws Exception;

	MerchantModel isCustomerExist(String mobileNumber);

	MerchantModel getMerchnatDetails(int id);

	DeleteResponse deleteMerchant(int id);

	MerchantModel findById(int merchantId);

	MerchantModel login(MerchantRegistrationRequest merchantRegrequest) throws Exception;

	MerchantModel updatePassword(MerchantModel merchantModel, ChangePasswordModel changePasswordModel) throws Exception;

	MerchantModel generateOtp(String mobile);



}
