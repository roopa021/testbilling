package com.kouchan.simplebilling.service;

import javax.activation.DataSource;

public interface NotificationEventService {

	boolean emailGeneratedReport(DataSource mailData, String string);

}
