package com.kouchan.simplebilling.service;

import java.util.List;

import com.kouchan.simplebilling.model.MerchantModel;
import com.kouchan.simplebilling.model.ProductListModel;

public interface ProductListService {

	List<ProductListModel> findAllMerchantproducts(int id);

	List<ProductListModel> getSubproductName(int id);

	ProductListModel saveProducts(ProductListModel product, int merchantId);

	List<ProductListModel> getAllProducts(int merchId, String category);

	MerchantModel getByMerchantId(int id);

	List<ProductListModel> deleteProduct(int merchantId, String subProduct);

	List<ProductListModel> getAllProductDetails(int merchant_id);

	List<ProductListModel> getproductNames(int id);


}
