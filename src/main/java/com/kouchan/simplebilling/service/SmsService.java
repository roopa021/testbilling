package com.kouchan.simplebilling.service;

public interface SmsService {

	void sendOtpToVisitor(String mobileNumber, int otp);

	void sendSucessMsg(String mobile);

	
}
