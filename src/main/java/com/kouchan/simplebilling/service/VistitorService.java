package com.kouchan.simplebilling.service;



import com.kouchan.simplebilling.model.VisitorModel;
import com.kouchan.simplebilling.model.VisitorOtpModel;
import com.kouchan.simplebilling.request.VistorRequest;

public interface VistitorService {

	VisitorModel save(VisitorModel visitorModel);

	VisitorModel validateOtp(VistorRequest vistorRequest);

	VisitorModel validateOtp(String mobileNumber, int otp);

	VisitorOtpModel saveOtp(VisitorOtpModel visitorOtpModel);



}
