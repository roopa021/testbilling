package com.kouchan.simplebilling.serviceImp;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kouchan.simplebilling.coredata.CodeMessage;
import com.kouchan.simplebilling.exception.CustomerDetailssNotFoundException;
import com.kouchan.simplebilling.exception.MerchantNotFoundException;
import com.kouchan.simplebilling.exception.SimplebillingException;
import com.kouchan.simplebilling.model.CustomerCancelReasonModel;
import com.kouchan.simplebilling.model.CustomerModel;
import com.kouchan.simplebilling.repository.CustomerCancelReasonRepo;
import com.kouchan.simplebilling.repository.CustomerRepository;
import com.kouchan.simplebilling.request.CustomerCancelReasonRequest;
import com.kouchan.simplebilling.service.CustomerCancelService;

@Service
public class CustomerCancelReasonServiceImpl implements CustomerCancelService {
	
	
	@Autowired
	CustomerCancelReasonRepo customerCancelReasonRepo;
	
	@Autowired
	private CustomerRepository customerRepository;


	@Override
	public CustomerCancelReasonModel cancelDelivery(CustomerCancelReasonRequest customerCancelResonRequest)
			throws  CustomerDetailssNotFoundException {
		CustomerCancelReasonModel customerCancleResonModel=new CustomerCancelReasonModel();
		 CustomerModel customerExit=customerRepository.findById(customerCancelResonRequest.getCustomerId());
		 if(customerExit==null)
		 {
			 final List<CodeMessage>list=new ArrayList<>();
			 list.add(new CodeMessage("302", "Customer Does't Exit"));
			 throw new MerchantNotFoundException(list);
		 }
		 customerCancleResonModel.setCustomerId(customerCancelResonRequest.getCustomerId());
		 customerCancleResonModel.setCustomerReason(customerCancelResonRequest.getCancleReason());
		return customerCancelReasonRepo.save(customerCancleResonModel);
	}
	
	

}
