package com.kouchan.simplebilling.serviceImp;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.google.gson.Gson;
import com.kouchan.simplebilling.coredata.CodeMessage;
import com.kouchan.simplebilling.exception.BillingException;
import com.kouchan.simplebilling.model.CustomerOrderDetails;
import com.kouchan.simplebilling.repository.CustomerOrderDetailsRepo;
import com.kouchan.simplebilling.request.MonthDatas;
import com.kouchan.simplebilling.response.CustomerOrderDetailsResponse;
import com.kouchan.simplebilling.service.CustomerOrderDetailsService;

@Service
public class CustomerOrderDetailsServiceImpl implements CustomerOrderDetailsService {

	@Autowired
	CustomerOrderDetailsRepo customerOrderDetailsRepo;

	@Override
	public CustomerOrderDetails getCustomerOrderDetails(int merchantID, Long customerID, int productid,
			String billMonth) {
		CustomerOrderDetails customerOrderDetails = customerOrderDetailsRepo.getCustomerOrderDetails(merchantID,
				customerID, productid, billMonth);
		if (customerOrderDetails == null) {
			final List<CodeMessage> list = new ArrayList<>();
			list.add(new CodeMessage("302", " InValid Data"));
			throw new BillingException(list);
		}

		return customerOrderDetails;

	}

	@Override
	public CustomerOrderDetails getCustomerOrderDetails(String billMonth) {

		CustomerOrderDetails c1 = customerOrderDetailsRepo.findBy1(billMonth);
		return c1;
	}

	@Override
	public CustomerOrderDetails getOrderSchedle(int merchantId, long customerId, String billMonth) {
		CustomerOrderDetails customerOrderDetails = customerOrderDetailsRepo.getorderSchedule(merchantId, customerId,
				billMonth);
		if (customerOrderDetails == null) {
			final List<CodeMessage> list = new ArrayList<>();
			list.add(new CodeMessage("302", " InValid Data"));
			throw new BillingException(list);
		}
		return customerOrderDetails;
	}

	@Override
	public CustomerOrderDetailsResponse upcomingOrderDetails(int merchantId, long customerId, String billMonth) {
		Gson gson = new Gson();
		CustomerOrderDetails customerOrderDetails = customerOrderDetailsRepo.getorderSchedule(merchantId, customerId,
				billMonth);
		if(customerOrderDetails==null)
		{
			final List<CodeMessage> list = new ArrayList<>();
			list.add(new CodeMessage("302", " InValid Data"));
			throw new BillingException(list);
		}
        String s=customerOrderDetails.getMonthYear();
		TypeReference<List<MonthDatas>> typeRefList = new TypeReference<List<MonthDatas>>() {};
		List<MonthDatas>l1=new ArrayList<MonthDatas>();
	
		String s1=" ";
		List<MonthDatas> monthDatasList = gson.fromJson(customerOrderDetails.getMonthYear(), typeRefList.getType());
		if (monthDatasList != null /* && monthDatasList.isEmpty() */) {
			for (MonthDatas monthDatas : monthDatasList) {
				if (monthDatas.getStatus().equals("0")) {
					
					l1.add(monthDatas);
				}
				}
			}
		CustomerOrderDetailsResponse response=new CustomerOrderDetailsResponse();
		response.setData(l1);
		response.setBillMonth(billMonth);
		response.setCustomerId(customerId);
		response.setMerchantId(merchantId);
		
		return response;
		
		
	}

	@Override
	public CustomerOrderDetailsResponse delivredOrderDetails(int merchantId, long customerId, String billMonth) {
		Gson gson = new Gson();
		CustomerOrderDetails customerOrderDetails = customerOrderDetailsRepo.getorderSchedule(merchantId, customerId,
				billMonth);
		if(customerOrderDetails==null)
		{
			final List<CodeMessage> list = new ArrayList<>();
			list.add(new CodeMessage("302", " InValid Data"));
			throw new BillingException(list);
		}
        String s=customerOrderDetails.getMonthYear();
		TypeReference<List<MonthDatas>> typeRefList = new TypeReference<List<MonthDatas>>() {};
		List<MonthDatas>l1=new ArrayList<MonthDatas>();
	
		String s1=" ";
		List<MonthDatas> monthDatasList = gson.fromJson(customerOrderDetails.getMonthYear(), typeRefList.getType());
		if (monthDatasList != null /* && monthDatasList.isEmpty() */) {
			for (MonthDatas monthDatas : monthDatasList) {
				if (monthDatas.getStatus().equals("1")) {
					
					l1.add(monthDatas);
				}
				}
			}
		CustomerOrderDetailsResponse response=new CustomerOrderDetailsResponse();
		response.setData(l1);
		response.setBillMonth(billMonth);
		response.setCustomerId(customerId);
		response.setMerchantId(merchantId);
		
		return response;
	}

	@Override
	public CustomerOrderDetailsResponse canceledOrderDetails(int merchantId, long customerId, String billMonth) {
		Gson gson = new Gson();
		CustomerOrderDetails customerOrderDetails = customerOrderDetailsRepo.getorderSchedule(merchantId, customerId,
				billMonth);
		if(customerOrderDetails==null)
		{
			final List<CodeMessage> list = new ArrayList<>();
			list.add(new CodeMessage("302", " InValid Data"));
			throw new BillingException(list);
		}
        String s=customerOrderDetails.getMonthYear();
		TypeReference<List<MonthDatas>> typeRefList = new TypeReference<List<MonthDatas>>() {};
		List<MonthDatas>l1=new ArrayList<MonthDatas>();
	
		String s1=" ";
		List<MonthDatas> monthDatasList = gson.fromJson(customerOrderDetails.getMonthYear(), typeRefList.getType());
		if (monthDatasList != null /* && monthDatasList.isEmpty() */) {
			for (MonthDatas monthDatas : monthDatasList) {
				if (monthDatas.getStatus().equals("2")) {
					
					l1.add(monthDatas);
				}
				}
			}
		CustomerOrderDetailsResponse response=new CustomerOrderDetailsResponse();
		response.setData(l1);
		response.setBillMonth(billMonth);
		response.setCustomerId(customerId);
		response.setMerchantId(merchantId);
		
		return response;
	}

}
