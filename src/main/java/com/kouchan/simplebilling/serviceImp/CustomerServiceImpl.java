package com.kouchan.simplebilling.serviceImp;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kouchan.simplebilling.coredata.CodeMessage;
import com.kouchan.simplebilling.exception.BillingException;
import com.kouchan.simplebilling.exception.BusinessException;
import com.kouchan.simplebilling.exception.ErrorCodes;
import com.kouchan.simplebilling.model.ChangePasswordModel;
import com.kouchan.simplebilling.model.CustomerModel;
import com.kouchan.simplebilling.model.CustomerOrderDetails;
import com.kouchan.simplebilling.model.MerchantModel;
import com.kouchan.simplebilling.repository.CustomerOrderDetailsRepo;
import com.kouchan.simplebilling.repository.CustomerRepository;
import com.kouchan.simplebilling.request.CustomerVO;
import com.kouchan.simplebilling.service.CustomerService;
import com.kouchan.simplebilling.utils.EncryptDescrypt;

@Service
public class CustomerServiceImpl implements CustomerService {
	@Autowired
	private CustomerRepository customerRepository;

	@Autowired
	private CustomerOrderDetailsRepo customerOrderDetailsRepo;

	byte customerNotAvailable = 2;

	MerchantModel merchantModel = new MerchantModel();
	CustomerOrderDetails customerOrderDetails = new CustomerOrderDetails();

	@Override
	public CustomerModel isCustomerExist(String mobileNumber) {
		System.out.println("mobile Number"+mobileNumber);
		CustomerModel customerModel = customerRepository.findByMobileNumber(mobileNumber);
		//String Mobile=customerModel.getMobileNumber();
		if (customerModel != null) {
			final List<CodeMessage> list = new ArrayList<>();
			list.add(new CodeMessage("302", "Customer Already exit"));
		}
		return customerModel;
	}

	@Override
	public CustomerModel saveNewCustomer(CustomerVO customerVO) throws Exception {
		CustomerModel customerModel = new CustomerModel();
		EncryptDescrypt e = new EncryptDescrypt();

		customerModel.setName(customerVO.getName());
		customerModel.setMobileNumber(customerVO.getMobileNumber());
		// customerModel.setPassword(e.encrypt(customerVO.getPassword()));
		/* without encrypting a password */
	//	customerModel.setPassword(e.encrypt((customerVO.getPassword())));
		customerModel.setPassword(((customerVO.getPassword())));
		customerModel.setSubproduct(customerVO.getSubproduct());
		customerModel.setUnit(customerVO.getUnit());

		CustomerModel customer = customerRepository.save(customerModel);
		return customer;

	}

	@Override
	public CustomerOrderDetails customerOrderDetailssave(CustomerOrderDetails customerOrderDetails) throws Exception {
		return customerOrderDetailsRepo.save(customerOrderDetails);

	}

	@Override
	public CustomerModel getByCustomerId(int id) {
		return customerRepository.findOne(id);
	}

	@Override
	public CustomerModel updateCustomerNameAndNumber(CustomerModel customerModel, int id) {

		CustomerModel customerModelobj = findById(id);
		customerModelobj.setName(customerModel.getName());
		customerModelobj.setMobileNumber(customerModel.getMobileNumber());
		return customerRepository.save(customerModelobj);

	}

	private CustomerModel findById(int id) {
		return customerRepository.findOne(id);
	}

	@Override
	public CustomerOrderDetails updateOrderDetails(int i, String date, byte status) {
		CustomerOrderDetails orderDetail = customerOrderDetailsRepo.findByOrderId(i);
		if(orderDetail==null)
		{
			final List<CodeMessage> list = new ArrayList<>();
			list.add(new CodeMessage("302", "No order schedule found !!"));
			throw new BillingException(list);
		}
		String listClone = orderDetail.getMonthYear();
	
		String oldChar = "{" + "\"" + "day" + "\"" + ":" + "\"" + date + "\"" + "," + "\"" + "status" + "\"" + ":"
				+ "\"" + "0" + "\"" + "," + "\"" + "quantity" + "\"" + ":" + "\"" + "1" + "\"" + "}";

		String newChar = "{" + "\"" + "day" + "\"" + ":" + "\"" + date + "\"" + "," + "\"" + "status" + "\"" + ":"
				+ "\"" + "1" + "\"" + "," + "\"" + "quantity" + "\"" + ":" + "\"" + "1" + "\"" + "}";

		String customerNotAvailable= "{" + "\"" + "day" + "\"" + ":" + "\"" + date + "\"" + "," + "\"" + "status" + "\"" + ":"
				+ "\"" + "2" + "\"" + "," + "\"" + "quantity" + "\"" + ":" + "\"" + "2" + "\"" + "}";

	
		
		CustomerOrderDetails save = new CustomerOrderDetails();
		if(status==1)
		{
		System.out.println("listclone:: " + listClone);
		String replacedString = listClone.replace(oldChar, newChar);
		System.out.println("replaced" + replacedString);
		orderDetail.setMonthYear(replacedString);
		}

		if(status==2)
		{
			String replacedStringNew = listClone.replace(oldChar, customerNotAvailable);
			System.out.println("replaced" + replacedStringNew);
			orderDetail.setMonthYear(replacedStringNew);
		}
		save = customerOrderDetailsRepo.save(orderDetail);
		return save;
	}

	@Override
	public CustomerModel login(CustomerVO customerVO) throws Exception {
		CustomerModel customerModel = null;
		EncryptDescrypt e = new EncryptDescrypt();
		CustomerModel customerMob = customerRepository.findByMobileNumber(customerVO.getMobileNumber());
		if (customerMob == null) {
			throw new BusinessException(ErrorCodes.MOBNOTEXIST.name(), ErrorCodes.MOBNOTEXIST.value());
		}
		
		//String password =  e.decrypt(customerMob.getPassword());
		String password =  (customerMob.getPassword());

		String enteredPass = customerVO.getPassword();
		//String p = e.encrypt(customerVO.getPassword());
		String p = (customerVO.getPassword());

		if (password.equals(enteredPass)) {
			customerModel = customerRepository.findByMobileNumberandPassword(customerVO.getMobileNumber(), p);

		} else {
			throw new BusinessException(ErrorCodes.PASSWORD.name(), ErrorCodes.PASSWORD.value());
		}
		return customerModel;
	}

	@Override
	public int generateOtp(String mobile) {
		// String otp = new DecimalFormat("000000").format(new
		// Random().nextInt(999999));
		int otp = (int) (Math.random() * 9000) + 1000;
		return otp;
	}

	@Override
	public CustomerVO validateOtp(String mobile, String otp) {
		// UserVO userVO = userService.validateOtp(mobile, otp);
		CustomerVO userVO = null; // line to delete
		CustomerVO customerVO = null;
		if (userVO != null) {
		// customerVO = findCustomerByUserId(userVO.getId());
		}
		return customerVO;
	}

	@Override
	public CustomerModel updatePassword(CustomerModel customerModel, ChangePasswordModel changePasswordModel)
			throws Exception {
		CustomerModel m1;
		EncryptDescrypt e = new EncryptDescrypt();
		Long m2 = changePasswordModel.getCustomer_id();
		m1 = getByCustomerId(m2);
		//m1.setPassword(e.encrypt(changePasswordModel.getNewPassword()));
		m1.setPassword((changePasswordModel.getNewPassword()));

		return customerRepository.save(m1);
	}

	@Override
	public CustomerModel getByCustomerId(Long customer_id) {
		return customerRepository.findById(customer_id);
	}

	
}
