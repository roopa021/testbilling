package com.kouchan.simplebilling.serviceImp;

import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.stereotype.Service;

import com.kouchan.simplebilling.data.EmailData;


@Service
public class DefaultEmailConfig {

	public void sendEmail(final EmailData emailData) throws Exception {
		try {
			/*
			 * final String host="smtp.gmail.com"; final String user =
			 * "totest48@gmail.com"; final String pass = "kouchan@48";
			 * 
			 * 
			 * final String to = emailData.getMailTo(); final String from =
			 * emailData.getMailFrom(); final String subject =
			 * emailData.getMailSubject(); final String messageText =
			 * emailData.getMailContent(); final boolean sessionDebug = false;
			 */

			String host = "smtp.gmail.com";
			final String user = "totest48@gmail.com";
			final String pass = "kouchan@48";
			String to = "roopapatil021@gmail.com";
			String from = "totest48@gmail.com";
			String subject = "Wel-Come to Simple BIlling";
			String messageText = "U R Password is ****";
			boolean sessionDebug = false;

			final Properties props = System.getProperties();

			props.put("mail.smtp.starttles.enable", "true");
			props.put("mail.smtp.host", host);
			props.put("mail.smtp.port", "587");
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.starttls.required", "true");

			// java.security.Security.addProvider(new
			// com.sun.net.ssl.internal.ssl.Provider());

			final Session session = Session.getDefaultInstance(props, null);
			session.setDebug(sessionDebug);

			final MimeMessage msg = new MimeMessage(session);
			msg.setFrom(new InternetAddress(from));
			final InternetAddress[] address = { new InternetAddress(to) };
			msg.setRecipients(Message.RecipientType.TO, address);
			msg.setSubject(subject);
			msg.setSentDate(new Date());
			msg.setText(messageText);

			final Transport transport = session.getTransport("smtp");
			System.setProperty("https.protocols", "TLSv1,TLSv1.1,TLSv1.2");
			transport.connect(host, user, pass);
			transport.sendMessage(msg, msg.getAllRecipients());
			transport.close();
			System.out.println("mail sent successfully");
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}
}
