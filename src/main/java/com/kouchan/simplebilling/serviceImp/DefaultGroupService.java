package com.kouchan.simplebilling.serviceImp;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.kouchan.simplebilling.exception.CustomerDetailssNotFoundException;
import com.kouchan.simplebilling.model.CustomerModel;
import com.kouchan.simplebilling.populator.Populator;
import com.kouchan.simplebilling.request.GroupRequest;
import com.kouchan.simplebilling.response.GroupResponse;
import com.kouchan.simplebilling.service.GroupService;
import com.kouchan.simplebilling.service.GroupService2;

@Component(value = "DefaultGroupService")
public class DefaultGroupService implements GroupService {

	/*
	 * @Autowired private GroupRepository groupRepository;
	 */

	@Autowired
	@Qualifier(value = "defaultUserGroupCreationReversePopulator")
	private Populator<GroupRequest, List<CustomerModel>> userGroupCreationPopulator;

	@Autowired
	@Qualifier(value = "defaultAddMembersToGroupReversePopulator")
	private Populator<GroupRequest, List<CustomerModel>> addMembersToGroupPopulator;

	@Autowired
	private GroupService2 groupService2;

	@Override
	public GroupResponse createUserGroup(final GroupRequest groupRequest) throws CustomerDetailssNotFoundException {
		final List<CustomerModel> customerModels = new ArrayList<CustomerModel>();
		userGroupCreationPopulator.populate(groupRequest, customerModels);
		int deliveryBoyId = groupRequest.getDeliveryBoyId();
		final GroupResponse groupResponse = groupService2.saveUserGroupService(
				Long.valueOf(groupRequest.getMerchantId()), groupRequest.getGroupName(), deliveryBoyId, customerModels,groupRequest);
		return groupResponse;

	}

	@Override
	public GroupResponse addMembersToGroup(final Long groupId, final GroupRequest groupRequest) throws CustomerDetailssNotFoundException {

		final List<CustomerModel> customerModels = new ArrayList<CustomerModel>();
		addMembersToGroupPopulator.populate(groupRequest, customerModels);
		final GroupResponse groupResponse = groupService2
				.addMembersToGroupService2(Long.valueOf(groupRequest.getMerchantId()), groupId, customerModels);
		return groupResponse;
	}

}
