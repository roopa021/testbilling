package com.kouchan.simplebilling.serviceImp;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kouchan.simplebilling.coredata.CodeMessage;
import com.kouchan.simplebilling.exception.BillingException;
import com.kouchan.simplebilling.exception.BusinessException;
import com.kouchan.simplebilling.exception.ErrorCodes;
import com.kouchan.simplebilling.exception.MerchantNotFoundException;
import com.kouchan.simplebilling.model.MerchantModel;
import com.kouchan.simplebilling.model.ProductListModel;
import com.kouchan.simplebilling.repository.MerchantRepository;
import com.kouchan.simplebilling.repository.ProductListRepository;
import com.kouchan.simplebilling.service.ProductListService;

@Service
public class DefaultProductListServiceImpl implements ProductListService {

	@Autowired
	private ProductListRepository productListRepository;

	@Autowired
	private MerchantRepository merchantRepository;
	byte inactiveStatus = 1;

	@Override
	public ProductListModel saveProducts(ProductListModel product, int merchantId) {
		MerchantModel merchatExit = merchantRepository.findOne(merchantId);
		if (merchatExit == null || merchatExit.getIsDeleted() == inactiveStatus) {
			final List<CodeMessage> list = new ArrayList<>();
			list.add(new CodeMessage("302", "Merchant Does't Exit"));
			throw new MerchantNotFoundException(list);
		}
		return productListRepository.save(product);
	}

	@Override
	public List<ProductListModel> findAllMerchantproducts(int id) {

		List<ProductListModel> products = productListRepository.productList(id);
		return products;
	}

	@Override
	public List<ProductListModel> getSubproductName(int id) {
		List<ProductListModel> subProductName = productListRepository.subProductList(id);
		return subProductName;
	}

	@Override
	public List<ProductListModel> getAllProducts(int merchId, String category) {
		int id = merchId;
		// ProductListModel l=productListRepository.findOne(id);
		// System.out.println("-----"+l.toString());
		List<ProductListModel> productlist = productListRepository.subProductList(id);

		if (productlist == null) {
			throw new BusinessException(ErrorCodes.MID.name(), ErrorCodes.MID.value());
		}

		if (category.equals("category")) {
			List<ProductListModel> allCategory = productListRepository.subProductList1(merchId, category);
			return allCategory;
		} else if (category.equals("productName")) {
			List<ProductListModel> allProducts = productListRepository.subProductList1(merchId, category);
			return allProducts;
		} else if (category.equals("subProduct")) {
			List<ProductListModel> allSubProducts = productListRepository.subProductList1(merchId, category);
			return allSubProducts;
		} else {
			throw new BusinessException(ErrorCodes.MID.name(), ErrorCodes.MID.value());
		}
	}

	@Override
	public List<ProductListModel> deleteProduct(int merchantId, String subProduct) {

		List<ProductListModel> productlist = productListRepository.subProductList1(merchantId, subProduct);

		if (productlist == null) {
			throw new BusinessException(ErrorCodes.MID.name(), ErrorCodes.MID.value());
		}
		if (subProduct.equals("subProduct")) {
			List<ProductListModel> allSubProducts = productListRepository.getAllSubCategory(merchantId);
			return allSubProducts;
		}

		else {
			throw new BusinessException(ErrorCodes.MID.name(), ErrorCodes.MID.value());
		}
	}

	@Override
	public MerchantModel getByMerchantId(int id) {
		MerchantModel merchantExit=merchantRepository.findOne(id);
		if(merchantExit==null)
		{
			final List<CodeMessage> list = new ArrayList<>();
			list.add(new CodeMessage("302", "Yet Merchant Not Created Products !! "));
			throw new MerchantNotFoundException(list);
		}
		return merchantExit;
	}

	@Override
	public List<ProductListModel> getAllProductDetails(int merchant_id) {		
		List<ProductListModel> allProducts = productListRepository.findById(merchant_id);
		if (allProducts.isEmpty()) {
			final List<CodeMessage> list = new ArrayList<>();
			list.add(new CodeMessage("302", "Yet Merchant Not Created Products !! "));
			throw new BillingException(list);
		}
		
		return allProducts;
	}

	@Override
	public List<ProductListModel> getproductNames(int id) {
		List<ProductListModel> productName = productListRepository.productList(id);
		return productName;
	}

}
