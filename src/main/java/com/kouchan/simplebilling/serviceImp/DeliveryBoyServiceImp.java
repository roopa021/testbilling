package com.kouchan.simplebilling.serviceImp;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kouchan.simplebilling.coredata.CodeMessage;
import com.kouchan.simplebilling.dto.DeliveryBoyDto;
import com.kouchan.simplebilling.exception.BusinessException;
import com.kouchan.simplebilling.exception.DeliveryBoyAlreadyExist;
import com.kouchan.simplebilling.exception.DeliveryBoyNotFoundException;
import com.kouchan.simplebilling.exception.ErrorCodes;
import com.kouchan.simplebilling.exception.MerchantAlreadyExistException;
import com.kouchan.simplebilling.model.ChangePasswordModel;
import com.kouchan.simplebilling.model.DeliveryBoyModel;
import com.kouchan.simplebilling.model.MerchantModel;
import com.kouchan.simplebilling.repository.DeliveryBoyRepository;
import com.kouchan.simplebilling.repository.GroupMapRepository;
import com.kouchan.simplebilling.request.DeliveryBoyRegistrationRequest;
import com.kouchan.simplebilling.response.DeleteResponse;
import com.kouchan.simplebilling.response.GetDeliveryBoyRepsonse;
import com.kouchan.simplebilling.service.DeliveryBoyService;

@Service
public class DeliveryBoyServiceImp implements DeliveryBoyService {

	@Autowired
	private DeliveryBoyRepository deliveryBoyRepository;

	@Autowired
	GroupMapRepository userGroupMapRepository;

	DeleteResponse deleteResponse;

	byte activeStatus = 0;
	byte inactiveStatus = 1;

	List<Object> sucessList;

	// NOTE: Group assignment during Delivery Boy is commented
	@Override
	public DeliveryBoyModel save(DeliveryBoyModel deliveryBoy, MerchantModel merchnatModel/* , Long groupId */) {
		long id = deliveryBoy.getDeliveryBoyId();
		// GroupModel d1 = null;
		String mobileNumber = deliveryBoy.getMobileNumber();

		DeliveryBoyModel deliveryBoyExit = deliveryBoyRepository.findByMobile(mobileNumber);
		if (deliveryBoyExit != null) {
			final List<CodeMessage> list = new ArrayList<>();
			list.add(new CodeMessage("302", "DeliveryBoy already Exit with this mobile number",
					deliveryBoy.getMobileNumber()));
			throw new DeliveryBoyAlreadyExist(list);
		}

		DeliveryBoyModel deliveryBoyExit1 = deliveryBoyRepository.findByMerchantIdAndMobileNumber(merchnatModel.getId(),
				mobileNumber);
		if (deliveryBoyExit1 != null) {
			final List<CodeMessage> list = new ArrayList<>();
			list.add(new CodeMessage("302", "Merchnat Already created this delivery Boy ",
					deliveryBoy.getMobileNumber()));
			throw new MerchantAlreadyExistException(list);
		}

		// DeliveryBoyModel d1=deliveryBoyRepository.findByMerchantIdAndMobileNumber(id,
		// groupId);

		return deliveryBoyRepository.save(deliveryBoy);
	}

	@Override
	public DeliveryBoyModel getByDeliveryBoyId(int id) {
		return deliveryBoyRepository.findOne(id);
	}

	@Override
	public DeliveryBoyModel update(DeliveryBoyModel deliveryBoy, int id) {
		DeliveryBoyModel d1 = findById(id);
		d1.setName(deliveryBoy.getName());
		d1.setMobileNumber(deliveryBoy.getMobileNumber());
		return deliveryBoyRepository.save(d1);
	}

	public DeliveryBoyModel findById(int id) {
		return deliveryBoyRepository.findOne(id);
	}

	@Override
	public DeleteResponse deleteDelivery(int id) {
		DeliveryBoyModel d1 = findById(id);
		if (d1 == null || d1.getIsDeleted() == inactiveStatus) {
			final List<CodeMessage> list = new ArrayList<>();
			list.add(new CodeMessage("302", "DeliveryBoy Not Found"));
			throw new DeliveryBoyNotFoundException(list);
		}

		if (String.valueOf(id).equals(String.valueOf(d1.getDeliveryBoyId())) && d1.getIsDeleted() == activeStatus) {
			d1.setIsDeleted(inactiveStatus);
			deliveryBoyRepository.save(d1);
		}

		return null;
	}

	@Override
	public DeliveryBoyModel getGroupId(int id) {
		return deliveryBoyRepository.findOne(id);
	}

	@Override
	public GetDeliveryBoyRepsonse getAllDeliveryBoys(int merchant_id,
			DeliveryBoyRegistrationRequest deliveryBoyRegistrationRequest) {
		sucessList = new ArrayList<>();
		final DeliveryBoyDto deliveryBoyDto = new DeliveryBoyDto();
		GetDeliveryBoyRepsonse getDeliveryBoyRepsonse = new GetDeliveryBoyRepsonse();
		List<DeliveryBoyModel> avalibleDeliveryboys = new ArrayList<DeliveryBoyModel>();
		List<DeliveryBoyModel> unAvalibleDeliveryboys = new ArrayList<DeliveryBoyModel>();

		List<DeliveryBoyModel> deliveryBoys = deliveryBoyRepository.findById(merchant_id);

		for (final DeliveryBoyModel d : deliveryBoys) {
			if (d.getIsAvaliable() == activeStatus) {

				avalibleDeliveryboys.add(d);
				deliveryBoyDto.setAvalibleDeliveryboys(avalibleDeliveryboys);
			}

			else {
				unAvalibleDeliveryboys.add(d);
				deliveryBoyDto.setUnAvalibleDeliveryboys(unAvalibleDeliveryboys);

			}
		}

		sucessList.add(deliveryBoyDto);

		getDeliveryBoyRepsonse.setStatus("Sucess");

		getDeliveryBoyRepsonse.setData(sucessList);

		return getDeliveryBoyRepsonse;
	}

	@Override
	public DeliveryBoyModel deliveryBoylogin(DeliveryBoyRegistrationRequest deliveryboyRegrequest) {
		DeliveryBoyModel deliveryBoylogin = null;

		DeliveryBoyModel deliveryBoyMob = deliveryBoyRepository.findByMobile(deliveryboyRegrequest.getMobileNumber());
		if (deliveryBoyMob == null) {
			throw new BusinessException(ErrorCodes.MOBNOTEXIST.name(), ErrorCodes.MOBNOTEXIST.value());
		}
		String password = deliveryBoyMob.getPassword();
		String enteredPass = deliveryboyRegrequest.getPassword();
		if (password.equals(enteredPass)) {
			deliveryBoylogin = deliveryBoyRepository.findByMobileNumberandPassword(
					deliveryboyRegrequest.getMobileNumber(), deliveryboyRegrequest.getPassword());

		} else {
			throw new BusinessException(ErrorCodes.PASSWORD.name(), ErrorCodes.PASSWORD.value());
		}

		return deliveryBoylogin;
	}

	@Override
	public DeliveryBoyModel updatePassword(DeliveryBoyModel deliveryBoyModel, ChangePasswordModel changePasswordModel) {
		DeliveryBoyModel m1;
		int m2 = changePasswordModel.getId();
		m1 = getByDeliveryBoyId(m2);
		m1.setPassword(changePasswordModel.getNewPassword());
		return deliveryBoyRepository.save(m1);
	}

	@Override
	public DeliveryBoyModel deliveryBoyAvaliablity(DeliveryBoyRegistrationRequest deliveryBoyRegistrationRequest) {
		DeliveryBoyModel deliveryBoyExit = deliveryBoyRepository
				.findOne(deliveryBoyRegistrationRequest.getDeliveryBoyId());
		if (deliveryBoyExit == null) {
			final List<CodeMessage> list = new ArrayList<>();
			list.add(new CodeMessage("302", "Delivery Boy Does't Exit"));
			throw new DeliveryBoyNotFoundException(list);
		}
		deliveryBoyExit.setIsAvaliable(inactiveStatus);
		deliveryBoyRepository.save(deliveryBoyExit);

		return deliveryBoyExit;
	}

}
