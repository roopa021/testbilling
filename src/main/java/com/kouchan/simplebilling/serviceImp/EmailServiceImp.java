package com.kouchan.simplebilling.serviceImp;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import com.kouchan.simplebilling.data.EmailData;
import com.kouchan.simplebilling.service.EmailService;

@Service
public class EmailServiceImp implements EmailService{
	
    @Autowired
    private JavaMailSender sender;
	
	@Override
    public void sendEmailByTemplateCode(final EmailData emailData) throws Exception {

        final MimeMessage message = sender.createMimeMessage();
        String emailBody=emailData.getMailBody();
        /*if(emailData.getModel()!=null) {
        	emailBody=emailTemplateMatcher.replace(emailData.getMailBody(), emailData.getModel());
        }*/
        message.setFrom(new InternetAddress(emailData.getMailFrom()));
        message.setRecipients(Message.RecipientType.TO, InternetAddress.parse("" + emailData.getMailTo() + ""));
        message.setSubject(emailData.getMailSubject());
        message.setRecipients(Message.RecipientType.CC, InternetAddress.parse("" + emailData.getMailCc() + ""));


        final Multipart multipart = new MimeMultipart();
        final BodyPart messageBodyPart = new MimeBodyPart();

        messageBodyPart.setContent(emailBody, "text/html"); // 5
        multipart.addBodyPart(messageBodyPart);

        if (emailData.getAttachments() != null) {
            for (final Object obj : emailData.getAttachments()) {
                if (obj instanceof DataSource) {
                    final BodyPart attachmentBodyPart = new MimeBodyPart();
                    attachmentBodyPart.setDataHandler(new DataHandler((DataSource) obj));
                    attachmentBodyPart.setFileName(emailData.getAttachetMentName());
                    multipart.addBodyPart(attachmentBodyPart);
                }

            }
        }
        message.setContent(multipart);
     
        sender.send(message);

    }


}
