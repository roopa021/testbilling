package com.kouchan.simplebilling.serviceImp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kouchan.simplebilling.model.ChangePasswordModel;
import com.kouchan.simplebilling.model.CustomerModel;
import com.kouchan.simplebilling.model.DeliveryBoyModel;
import com.kouchan.simplebilling.model.ForgotPasswordModel;
import com.kouchan.simplebilling.model.MerchantModel;
import com.kouchan.simplebilling.repository.CustomerRepository;
import com.kouchan.simplebilling.repository.DeliveryBoyRepository;
import com.kouchan.simplebilling.repository.ForgotPasswordRepository;
import com.kouchan.simplebilling.repository.MerchantRepository;
import com.kouchan.simplebilling.request.ForgotPasswordRequest;
import com.kouchan.simplebilling.service.ForgotPasswordService;
import com.kouchan.simplebilling.utils.EncryptDescrypt;

@Service
public class ForgotPasswordServiceImp implements ForgotPasswordService {

	@Autowired
	private ForgotPasswordRepository forgotPasswordRepository;

	@Autowired
	private MerchantRepository merchantRepository;

	@Autowired
	private CustomerRepository customerRepository;

	@Autowired
	private DeliveryBoyRepository deliveryBoyRepository;

	@Override
	public ForgotPasswordModel saveOtp(ForgotPasswordModel forgotPasswordModel) {

		return forgotPasswordRepository.save(forgotPasswordModel);
	}

	@Override
	public MerchantModel updatePassword(MerchantModel merchantModel, ForgotPasswordRequest forgotPasswordRequest)
			throws Exception {
		EncryptDescrypt e = new EncryptDescrypt();
		MerchantModel m1;
		int m2 = forgotPasswordRequest.getMerchat_id();
		m1 = findById(m2);
		// m1.setPassword(e.encrypt(changePasswordModel.getNewPassword()));
		m1.setPassword((forgotPasswordRequest.getNewPassword()));
		System.out.println("m1-----" + m1);
		return merchantRepository.save(m1);

	}

	public MerchantModel findById(int id) {
		return merchantRepository.findOne(id);
	}

	@Override
	public CustomerModel updatePassword(CustomerModel merchantModel, ForgotPasswordRequest forgotPasswordRequest) {
		// int id=(int)forgotPasswordRequest.getCustomer_id();
		CustomerModel customerModelobj = getByCustomerId(forgotPasswordRequest.getCustomer_id());
		// customerModelobj.setMobileNumber(customerModel.getMobileNumber());
		customerModelobj.setPassword(forgotPasswordRequest.getNewPassword());
		return customerRepository.save(customerModelobj);

	}

	@Override
	public CustomerModel getByCustomerId(Long customer_id) {
		return customerRepository.findById(customer_id);
	}

	@Override
	public DeliveryBoyModel updatePassword(DeliveryBoyModel merchantModel,
			ForgotPasswordRequest forgotPasswordRequest) {
		DeliveryBoyModel d1 = findByDeliveryBoyId(forgotPasswordRequest.getDeliveryBoy_id());
		d1.setPassword(forgotPasswordRequest.getNewPassword());
		return deliveryBoyRepository.save(d1);
	}

	public DeliveryBoyModel findByDeliveryBoyId(int id) {
		return deliveryBoyRepository.findOne(id);
	}

}
