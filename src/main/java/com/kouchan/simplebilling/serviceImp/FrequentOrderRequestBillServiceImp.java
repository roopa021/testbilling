package com.kouchan.simplebilling.serviceImp;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kouchan.simplebilling.coredata.CodeMessage;
import com.kouchan.simplebilling.exception.BillingException;
import com.kouchan.simplebilling.exception.CustomerDetailssNotFoundException;
import com.kouchan.simplebilling.exception.MerchantAlreadyExistException;
import com.kouchan.simplebilling.exception.SimplebillingException;
import com.kouchan.simplebilling.model.CustomerModel;
import com.kouchan.simplebilling.model.FrequentOrderBillModel;
import com.kouchan.simplebilling.model.FrequentOrderRequestModel;
import com.kouchan.simplebilling.model.ProductListModel;
import com.kouchan.simplebilling.repository.CustomerRepository;
import com.kouchan.simplebilling.repository.FrequentOrderRequestBillRepository;
import com.kouchan.simplebilling.repository.FrequentOrderRequestRepository;
import com.kouchan.simplebilling.repository.ProductListRepository;
import com.kouchan.simplebilling.request.FrequentOrderBillRequest;
import com.kouchan.simplebilling.service.FrequentOrderRequestBillService;

@Service
public class FrequentOrderRequestBillServiceImp implements FrequentOrderRequestBillService {

	@Autowired
	private FrequentOrderRequestBillRepository frequentOrderRequestBillRepository;

	@Autowired
	ProductListRepository productListRepository;

	@Autowired
	private FrequentOrderRequestRepository frequentOrderRequestRepository;

	@Autowired
	private CustomerRepository customerRepository;

	byte activeStatus = 0;
	byte inactiveStatus = 1;

	// To-Do: Two times bill getting generated ;-(
	// Discount concept

	@Override
	public FrequentOrderBillModel getBillData(FrequentOrderBillRequest frequentOrder) {
		ProductListModel productList = productListRepository.findOne(frequentOrder.getProduct_Id());

		FrequentOrderBillModel billModel = new FrequentOrderBillModel();
		String date=frequentOrder.getBill_date();
		int merchantId = frequentOrder.getMerchant_Id();
		int productid = frequentOrder.getProduct_Id();
		long customerId = frequentOrder.getCustomer_Id();
		int billNumber = frequentOrder.getBill_number();
		FrequentOrderRequestModel fr = frequentOrderRequestRepository.findByMerchantProductCustomerID(merchantId,
				customerId, productid,date);
		if (fr == null) {
			final List<CodeMessage> list = new ArrayList<>();
			list.add(new CodeMessage("302", "Invalid Credential"));
			throw new BillingException(list);
		}
		FrequentOrderBillModel fb = frequentOrderRequestBillRepository.findOne(billNumber);
		/*
		 * if(fb==null) { final List<CodeMessage> list = new ArrayList<>(); list.add(new
		 * CodeMessage("302", "bill number loop")); throw new MerchantAlre t); }
		 */
		int s = frequentOrder.getIsgstApplicable();
		int qty = fr.getQuantity();
		Double amt = productList.getRate();
		if (s == activeStatus) {
			int total = (int) (qty * amt);
			billModel.setTotal(total);
			billModel.setTotalAmount(total);
		} else {
			double gst = frequentOrder.getPergst();
			int total = (int) (qty * amt);
			// billModel.setPergst(frequentOrder.getGst());
			int total1 = (int) (qty * gst * amt);
			int totalAmount = total + total1;
			billModel.setTotalAmount(totalAmount);
			billModel.setTotal(total);
		}
		CustomerModel c = customerRepository.findById(customerId);
		billModel.setCustomerName(c.getName());
		billModel.setCustomerNumber(c.getMobileNumber());
		billModel.setMerchat_id(frequentOrder.getMerchant_Id());
		billModel.setCustomer_id(frequentOrder.getCustomer_Id());
		billModel.setIsgstApplicable(frequentOrder.getIsgstApplicable());
		billModel.setPergst(frequentOrder.getPergst());
		billModel.setBillNumber(frequentOrder.getBill_number());
		billModel.setBillMonth(frequentOrder.getBill_month());
		billModel.setBillDate(frequentOrder.getBill_date());
		FrequentOrderBillModel b = frequentOrderRequestBillRepository.save(billModel);
		return b;

	}

	@Override
	public FrequentOrderBillModel getCustomerBill(Long customer_id) throws CustomerDetailssNotFoundException {
		final FrequentOrderBillModel f = frequentOrderRequestBillRepository.findById(customer_id);
		if (f == null) {
			final List<CodeMessage> list = new ArrayList<>();
			list.add(new CodeMessage("302", "Customer Does't Not Exit"));
			throw new CustomerDetailssNotFoundException(list);
		}
		return f;
	}

	@Override
	public FrequentOrderBillModel updateBillStatus(FrequentOrderBillModel frequentOrderBillModel, int merchat_id) {
		FrequentOrderBillModel f1 = findById(merchat_id);
		long customer_id = frequentOrderBillModel.getCustomer_id();
		FrequentOrderBillModel bill = frequentOrderRequestBillRepository.findByCustomerIdAndMerchnatId(merchat_id,
				customer_id);
		if (bill == null) {
			final List<CodeMessage> list = new ArrayList<>();
			list.add(new CodeMessage("302", "Invalid Credential !! "));
			throw new BillingException(list);
		}
		if(bill.getBillStatus()==inactiveStatus)
		{
			final List<CodeMessage> list = new ArrayList<>();
			list.add(new CodeMessage("302", "Bill Already Paid "));
			throw new BillingException(list);
		}
		if(bill.getBillStatus()==activeStatus) {
		bill.setBillStatus(inactiveStatus);}
		FrequentOrderBillModel f = frequentOrderRequestBillRepository.save(bill);
		return f;
	}
	
	public FrequentOrderBillModel findById(int merchat_id) {
		return frequentOrderRequestBillRepository.findOne(merchat_id);
	}

}
