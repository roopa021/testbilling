package com.kouchan.simplebilling.serviceImp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kouchan.simplebilling.model.FrequentOrderRequestModel;
import com.kouchan.simplebilling.repository.FrequentOrderRequestRepository;
import com.kouchan.simplebilling.repository.MerchantRepository;
import com.kouchan.simplebilling.service.FrequentOrderRequestService;

@Service
public class FrequentOrderRequestServiceImp implements FrequentOrderRequestService{

	@Autowired
	private FrequentOrderRequestRepository frequentOrderRequestRepository;

	
	
	@Override
	public FrequentOrderRequestModel save(FrequentOrderRequestModel frequentOrderRequestModel) {
	
		
		return frequentOrderRequestRepository.save(frequentOrderRequestModel);
	}
}
