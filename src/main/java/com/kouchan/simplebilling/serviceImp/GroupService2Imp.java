package com.kouchan.simplebilling.serviceImp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kouchan.simplebilling.coredata.CodeMessage;
import com.kouchan.simplebilling.dto.AddMembersToGroupDto;
import com.kouchan.simplebilling.dto.GroupDetailsDto;
import com.kouchan.simplebilling.dto.GroupDto;
import com.kouchan.simplebilling.dto.GroupMemberDetailDto;
import com.kouchan.simplebilling.exception.BillingException;
import com.kouchan.simplebilling.exception.CustomerDetailssNotFoundException;
import com.kouchan.simplebilling.exception.GroupAlreadyExitException;
import com.kouchan.simplebilling.exception.GroupNotFoundException;
import com.kouchan.simplebilling.exception.MerchantNotFoundException;
import com.kouchan.simplebilling.model.CustomerModel;
import com.kouchan.simplebilling.model.DeliveryBoyModel;
import com.kouchan.simplebilling.model.GroupModel;
import com.kouchan.simplebilling.model.MerchantModel;
import com.kouchan.simplebilling.model.UserGroupMembersMap;
import com.kouchan.simplebilling.repository.CustomerRepository;
import com.kouchan.simplebilling.repository.DeliveryBoyRepository;
import com.kouchan.simplebilling.repository.GroupMapRepository;
import com.kouchan.simplebilling.repository.GroupRepository;
import com.kouchan.simplebilling.repository.MerchantRepository;
import com.kouchan.simplebilling.request.GroupRequest;
import com.kouchan.simplebilling.response.GroupDeleteResponse;
import com.kouchan.simplebilling.response.GroupResponse;
import com.kouchan.simplebilling.response.UpdateGroupNameResponse;
import com.kouchan.simplebilling.service.GroupService2;

@Service
public class GroupService2Imp implements GroupService2 {

	@Autowired
	private GroupRepository groupRepository;

	@Autowired
	private CustomerRepository customerRepository;

	@Autowired
	private GroupMapRepository groupMapRepository;

	@Autowired
	private MerchantRepository merchantRepository;

	@Autowired
	private DeliveryBoyRepository deliveryBoyRepository;

	GroupResponse groupResponse;

	List<Object> sucessList;

	byte activeStatus = 0;
	byte inactiveStatus = 1;
	GroupDeleteResponse deleteResponse;

	byte individual = 1;
	byte group = 0;

	@Transactional(rollbackOn = Exception.class)
	@Override
	public GroupResponse saveUserGroupService(Long merchnatId, String groupName, int deliveryBoyId,
			List<CustomerModel> users, GroupRequest groupRequest) throws CustomerDetailssNotFoundException {

		final List<CustomerModel> listOfRegisterUsers = new ArrayList<CustomerModel>();
		final List<CustomerModel> listOfUnRegisterUsers = new ArrayList<CustomerModel>();

		List<UserGroupMembersMap> listOfGroupMembers = new ArrayList<UserGroupMembersMap>();

		final List<HashMap<String, String>> listOfUsers = new ArrayList<>();
		final List<HashMap<String, String>> listOfUnregUsers = new ArrayList<>();

		CustomerModel userDetails;
		DeliveryBoyModel deliveryBoyModel = null;
		GroupModel userGroup = new GroupModel();
		final UserGroupMembersMap userGroupMap = new UserGroupMembersMap();
		final GroupDto groupResponseDTO = new GroupDto();

		sucessList = new ArrayList<>();
		groupResponse = new GroupResponse();

		Long i = merchnatId;
		int m = i.intValue();

		MerchantModel mId = merchantRepository.findById(m);
		if (mId == null) {
			final List<CodeMessage> list = new ArrayList<>();
			list.add(new CodeMessage("Merchant Not Found"));
			throw new MerchantNotFoundException(list);
		}
		if (groupRequest.getAssign() == 1) {
			CustomerModel c = customerRepository.findByMobileNumber(groupRequest.getMobileNumber());
			userGroup.setGroupName(c.getName());
			userGroup.setMerchantId(merchnatId);
			groupRepository.save(userGroup);

			userGroupMap.setGroupMemberMobile(c.getMobileNumber());
			userGroupMap.setGroupMemberId(c.getId());
			userGroupMap.setMerchnatId(merchnatId);
			userGroupMap.setGroupMemberName(c.getName());
			userGroupMap.setGroupId(userGroup.getGroupId());
			userGroupMap.setIsAssign(individual);
			groupMapRepository.save(userGroupMap);

		} else {

			// GroupModel groupModel = groupRepository.findByGroupIdAndGroupName(groupId,
			// groupName);
			GroupModel groupModel = groupRepository.findBymerchnatIdAndGroupName(merchnatId, groupName);
			if (groupModel != null) {
				final List<CodeMessage> list = new ArrayList<>();
				list.add(new CodeMessage("This group name is already exit...Please enter another name"));
				throw new GroupAlreadyExitException(list);
			}

			for (final CustomerModel user : users) {
				userDetails = customerRepository.findByMobileNumber(user.getMobileNumber());
				if (userDetails == null) {
					listOfUnRegisterUsers.add(user);
				} else {
					listOfRegisterUsers.add(userDetails);
				}

			}
			if (listOfRegisterUsers.isEmpty()) {

				final List<CodeMessage> list = new ArrayList<>();
				list.add(new CodeMessage("Customer is Not Regstered in Simple Billing Application"));
				throw new CustomerDetailssNotFoundException(list);
			}
			userGroup.setMerchantId(merchnatId);
			userGroup.setGroupId(merchnatId);
			userGroup.setGroupName(groupName);
			userGroup.setIsDeleted(activeStatus);
			userGroup = groupRepository.save(userGroup);

			for (final CustomerModel user : listOfRegisterUsers) {
				// userGroupMap.setGroupMemberId(userGroup.getGroupId());
				// userGroup.setMerchantId(userGroup.getMerchantId());
				userGroupMap
						.setGroupMemberId((long) customerRepository.findByMobileNumber(user.getMobileNumber()).getId());
				userGroupMap.setGroupMemberName(user.getName());
				userGroupMap.setGroupMemberMobile(user.getMobileNumber());
				userGroupMap.setMerchnatId(merchnatId);
				// ssuserGroupMap.setGroupId(user.getId());
				userGroupMap.setIsDeleted(activeStatus);
				userGroupMap.setGroupId(userGroup.getGroupId());
				// userGroupMap.setDeliverBoyId(deliveryBoyId);
				// userGroupMap.setGroupNameId(user.getId());
				groupMapRepository.save(userGroupMap);
			}
			listOfGroupMembers = groupMapRepository.findByGroupId(userGroup.getGroupId());
			groupResponseDTO.setGroupId(userGroup.getGroupId());
			groupResponseDTO.setMerchantId(userGroup.getMerchantId());
			groupResponseDTO.setGroupName(userGroup.getGroupName());
			listOfGroupMembers.parallelStream().forEach(l -> {
				final HashMap<String, String> li = new HashMap<>();
				li.put("groupMemberId", String.valueOf(l.getGroupMemberId()));
				li.put("DeliverBoyId", String.valueOf(l.getDeliverBoyId()));
				li.put("groupMemberName", l.getGroupMemberName());
				li.put("groupMemberMobileNumber", l.getGroupMemberMobile());
				li.put("merchantId", String.valueOf(l.getMerchnatId()));
				li.put("groupId", String.valueOf(l.getGroupId()));
				listOfUsers.add(li);

			});

			listOfUnRegisterUsers.parallelStream().forEach(l -> {
				final HashMap<String, String> listUnreg = new HashMap<>();
				listUnreg.put("groupMemberId", "null");
				listUnreg.put("groupMemberName", l.getName());
				listUnreg.put("groupMemberMobileNumber", l.getMobileNumber());
				listOfUnregUsers.add(listUnreg);
			});
			if (!listOfUsers.isEmpty() && !listOfUnregUsers.isEmpty()) {
				groupResponseDTO.setListOfRegUsers(listOfUsers);
				groupResponseDTO.setListOfUnRegUsers(listOfUnregUsers);
				sucessList.add(groupResponseDTO);
				groupResponse.setStatus("Success");
				groupResponse.setData(sucessList);
				return groupResponse;
			} else if (!listOfRegisterUsers.isEmpty() && listOfUnRegisterUsers.isEmpty()) {
				groupResponseDTO.setListOfRegUsers(listOfUsers);
				sucessList.add(groupResponseDTO);
				groupResponse.setStatus("Success");
				groupResponse.setData(sucessList);
				return groupResponse;
			}
		}
		return null;
	}

	@Transactional(rollbackOn = Exception.class)
	@Override
	public GroupResponse addMembersToGroupService2(Long merchnatId, Long groupId, List<CustomerModel> users) throws CustomerDetailssNotFoundException {
		final List<CustomerModel> listOfRegisterUsers = new ArrayList<CustomerModel>();
		final List<CustomerModel> listOfUnRegisterUsers = new ArrayList<CustomerModel>();

		final List<HashMap<String, String>> listOfUsers = new ArrayList<>();
		final List<HashMap<String, String>> listOfUnregUsers = new ArrayList<>();

		groupResponse = new GroupResponse();
		final UserGroupMembersMap userGroupMap = new UserGroupMembersMap();

		final AddMembersToGroupDto repsonseDTO = new AddMembersToGroupDto();
		sucessList = new ArrayList<>();

		CustomerModel userDetails;
		for (final CustomerModel user : users) {
			userDetails = customerRepository.findByMobileNumber(user.getMobileNumber());
			if (userDetails == null) {
				listOfUnRegisterUsers.add(user);
			} else {
				listOfRegisterUsers.add(userDetails);
			}
		}
		if (listOfUnRegisterUsers.isEmpty()) {
			final List<CodeMessage> list = new ArrayList<>();
			list.add(new CodeMessage("Customer is Not Regstered in Simple Billing Application"));
			throw new CustomerDetailssNotFoundException(list);
		}

		final GroupModel userGroup = groupRepository.findOne(groupId);
		if (userGroup == null) {
			final List<CodeMessage> list = new ArrayList<>();
			list.add(new CodeMessage("Group Not Exit !!!"));
			throw new MerchantNotFoundException(list);
		}
		/*
		 * final List<UserGroupMembersMap> listOfGroupMembers =
		 * groupMapRepository.findByGroupId(groupId); final
		 * List<UserGroupMembersMap>l23=groupMapRepository.findAll(groupId); if
		 * (String.valueOf(groupId).equals(String.valueOf(userGroup.getGroupId()))) {
		 * for (final CustomerModel reguser : listOfRegisterUsers) { for (final
		 * UserGroupMembersMap userGroupMap2 : l23) { if
		 * ((reguser.getMobileNumber()).equals(userGroupMap2.getGroupMemberMobile())) {
		 * System.out.println("-------------");
		 * 
		 * if (userGroupMap2.getIsDeleted() == inactiveStatus) {
		 * userGroupMap2.setIsDeleted(activeStatus);
		 * GroupMapRepository.save(userGroupMap2); }
		 * 
		 * 
		 * // userGroupMap.setGroupId(userGroup.getGroupId());
		 * userGroupMap.setGroupMemberId(customerRepository.findByMobileNumber(reguser.
		 * getMobileNumber()).getId()); userGroupMap.setMerchnatId(merchnatId);
		 * userGroupMap.setGroupMemberName(reguser.getName());
		 * userGroupMap.setGroupMemberMobile(reguser.getMobileNumber());
		 * userGroupMap.setIsDeleted(activeStatus);
		 * userGroupMap.setGroupId(userGroup.getGroupId());
		 * groupMapRepository.save(userGroupMap); }
		 * 
		 * }
		 */
		final List<UserGroupMembersMap> listOfGroupMembers = groupMapRepository.findByGroupId(groupId);

		if (String.valueOf(groupId).equals(String.valueOf(userGroup.getGroupId()))) {
			for (final CustomerModel reguser : listOfRegisterUsers) {
				for (final UserGroupMembersMap userGroupMap2 : listOfGroupMembers) {
					if ((reguser.getMobileNumber()).equals(userGroupMap2.getGroupMemberMobile())) {
						if (userGroupMap2.getIsDeleted() == inactiveStatus) {
							userGroupMap2.setIsDeleted(activeStatus);
							groupMapRepository.save(userGroupMap2);
						}

					} else {
						userGroupMap.setGroupId(userGroup.getGroupId());
						userGroupMap.setGroupMemberId(
								(long) customerRepository.findByMobileNumber(reguser.getMobileNumber()).getId());
						userGroupMap.setGroupMemberName(reguser.getName());
						userGroupMap.setMerchnatId(merchnatId);
						userGroupMap.setGroupMemberMobile(reguser.getMobileNumber());
						userGroupMap.setIsDeleted(activeStatus);
						groupMapRepository.save(userGroupMap);
					}
				}
			}

		}
		final List<UserGroupMembersMap> listOfMembers = groupMapRepository.findByGroupId(groupId);

		listOfMembers.parallelStream().forEach(l -> {
			final HashMap<String, String> list = new HashMap<>();
			// list.put("groupMemberId", String.valueOf(l.getGroupMemberId()));
			list.put("groupMemberName", l.getGroupMemberName());
			list.put("groupMemberMobileNumber", l.getGroupMemberMobile());
			listOfUsers.add(list);

		});

		listOfUnRegisterUsers.parallelStream().forEach(l -> {
			final HashMap<String, String> listUnreg = new HashMap<>();
			// listUnreg.put("groupMemberId", "null");
			listUnreg.put("groupMemberName", l.getName());
			listUnreg.put("groupMemberMobileNumber", l.getMobileNumber());
			listOfUnregUsers.add(listUnreg);
		});

		if (!listOfUsers.isEmpty() && !listOfUnregUsers.isEmpty()) {

			repsonseDTO.setListOfRegUsers(listOfUsers);
			repsonseDTO.setListOfUnRegUsers(listOfUnregUsers);
			sucessList.add(repsonseDTO);
			groupResponse.setStatus("Success");
			groupResponse.setData(sucessList);
			return groupResponse;
		} else if (!listOfRegisterUsers.isEmpty() && listOfUnRegisterUsers.isEmpty()) {

			repsonseDTO.setListOfRegUsers(listOfUsers);
			sucessList.add(repsonseDTO);
			groupResponse.setStatus("OK");
			groupResponse.setData(sucessList);
			return groupResponse;
		}
		return null;
	}

	@Transactional(rollbackOn = Exception.class)
	@Override
	public GroupResponse updateUserGroupNameService(Long groupId, String mobileNumber, String groupName,
			Long merchantId) throws GroupNotFoundException {
		sucessList = new ArrayList<>();
		groupResponse = new GroupResponse();
		GroupModel userGroupName = null;
		final UpdateGroupNameResponse updateGroupNameResponse = new UpdateGroupNameResponse();
		final List<GroupModel> userGroupModels = groupRepository.findByGroupOwnerId(groupId, activeStatus);
		for (final GroupModel userGroupModel : userGroupModels) {
			if (userGroupModel.getGroupName().equals(groupName) && userGroupModel.getIsDeleted() == activeStatus) {
				final List<CodeMessage> list = new ArrayList<>();
				list.add(new CodeMessage("Group Name simliar !!"));
				throw new GroupAlreadyExitException(list);

			}
		}

		final GroupModel userGroup = groupRepository.findOne(groupId);
		if (userGroup == null) {
			final List<CodeMessage> list = new ArrayList<>();
			list.add(new CodeMessage("Group Not Exit..!!!"));
			throw new GroupNotFoundException(list);
		}
		if (String.valueOf(groupId).equals(String.valueOf(userGroup.getGroupId()))) {
			userGroup.setGroupName(groupName);
			userGroupName = groupRepository.save(userGroup);
		}

		updateGroupNameResponse.setGroupName(userGroupName.getGroupName());
		// updateGroupNameResponse.setDeliveryBoyId(deliveryBoyId);
		sucessList.add(updateGroupNameResponse);
		groupResponse.setStatus("Success");
		groupResponse.setData(sucessList);
		return groupResponse;

	}

	@Transactional(rollbackOn = Exception.class)
	@Override
	public GroupResponse getAllGroupMemberDetailsService(Long groupId, Long merchantId) throws GroupNotFoundException {

		final List<HashMap<String, String>> listOfUsers = new ArrayList<>();
		groupResponse = new GroupResponse();
		sucessList = new ArrayList<>();

		final GroupDto groupResponseDTO = new GroupDto();

		final GroupModel userGroup = groupRepository.findOne(groupId);
		if (userGroup == null || userGroup.getIsDeleted() == inactiveStatus) {
			final List<CodeMessage> list = new ArrayList<>();
			list.add(new CodeMessage("Group Does't Exit"));
			throw new GroupNotFoundException(list);
		}
		final List<UserGroupMembersMap> listOfMembers = groupMapRepository.findAll(groupId);

		listOfMembers.parallelStream().forEach(l -> {
			final HashMap<String, String> list = new HashMap<>();
			list.put("groupMemberId", String.valueOf(l.getGroupMemberId()));
			list.put("groupMemberName", l.getGroupMemberName());
			list.put("groupMemberMobileNumber", l.getGroupMemberMobile());
			listOfUsers.add(list);

		});

		groupResponseDTO.setGroupId(userGroup.getGroupId());
		groupResponseDTO.setGroupName(userGroup.getGroupName());
		groupResponseDTO.setMerchantId(merchantId);
		// groupResponseDTO.setGroupCreatedDate(userGroup.getCreatedDate().toString());
		groupResponseDTO.setListOfRegUsers(listOfUsers);
		// groupResponseDTO.setListOfRegUsers(listOfUsers);
		sucessList.add(groupResponseDTO);
		groupResponse.setStatus("Success");
		groupResponse.setData(sucessList);
		return groupResponse;

	}

	@Override
	public GroupResponse getGroupDetailsService(Long merchantId, Long groupId) throws GroupNotFoundException {

		groupResponse = new GroupResponse();
		GroupDetailsDto groupDetailsDto;
		List<UserGroupMembersMap> listOfGroupMembersMaps = new ArrayList<UserGroupMembersMap>();

		final List<GroupModel> listGroupModels = groupRepository.findByGroupOwnerId(groupId, activeStatus);
		sucessList = new ArrayList<>();

		if (listGroupModels.isEmpty()) {
			final List<CodeMessage> list = new ArrayList<>();
			list.add(new CodeMessage("Group Not Found"));
			throw new GroupNotFoundException(list);
		}

		for (final GroupModel userGroupModel : listGroupModels) {
			groupDetailsDto = new GroupDetailsDto();
			groupDetailsDto.setGroupId(userGroupModel.getGroupId());
			groupDetailsDto.setGroupName(userGroupModel.getGroupName());
			final ArrayList<GroupMemberDetailDto> memberList = new ArrayList<GroupMemberDetailDto>();
			listOfGroupMembersMaps = groupMapRepository.findByGroupId(userGroupModel.getGroupId(), activeStatus);

			for (int i = 0; i < listOfGroupMembersMaps.size(); i++) {
				final GroupMemberDetailDto detailDto = new GroupMemberDetailDto();
				detailDto.setGroupMemberId("" + listOfGroupMembersMaps.get(i).getGroupMemberId());
				detailDto.setGroupMemberMobile(listOfGroupMembersMaps.get(i).getGroupMemberMobile());
				detailDto.setGroupMemberName(listOfGroupMembersMaps.get(i).getGroupMemberName());
				memberList.add(detailDto);
			}
			groupDetailsDto.setListOfRegUsers(memberList);
			sucessList.add(groupDetailsDto);

		}

		groupResponse.setStatus("Success");
		groupResponse.setData(sucessList);

		return groupResponse;

	}

	@Override
	public GroupDeleteResponse deleteUserGroupService(Long groupId) {
		final GroupModel userGroup = groupRepository.findOne(groupId);
		// List<UserGroupMembersMap
		// user1=groupMapRepository.findByGroupId1(groupId,mwe);
		deleteResponse = new GroupDeleteResponse();
		if (userGroup == null) {
			final List<CodeMessage> list = new ArrayList<>();
			list.add(new CodeMessage("Group Not Exit..!!!"));
			throw new MerchantNotFoundException(list);
		}

		if (String.valueOf(groupId).equals(String.valueOf(userGroup.getGroupId()))
				&& userGroup.getIsDeleted() == activeStatus) {
			userGroup.setIsDeleted(inactiveStatus);
			// user1.setIsDeleted(inactiveStatus);
			// groupMapRepository.save(user1);
			groupRepository.save(userGroup);
		}
		final GroupModel userGroupDetails = groupRepository.findOne(userGroup.getGroupId());
		if (String.valueOf(inactiveStatus).equals(String.valueOf(userGroupDetails.getIsDeleted()))) {
			deleteResponse.setStatus("Success");
			deleteResponse.setGroupId(String.valueOf(groupId));
			return deleteResponse;
		}
		return null;

	}

	@Override
	public List<GroupModel> getAllGroupNames(Long merchant_id) {

		Long i = merchant_id;
		int m = i.intValue();

		List<GroupModel> g = groupRepository.findByGroupMerchnatId(merchant_id);
		//GroupModel g1=groupRepository.findOne(merchant_id);
		
		/*if (g1 == null) {
			final List<CodeMessage> list = new ArrayList<>();
			list.add(new CodeMessage(" Merchant Does't Exit"));
			throw new MerchantNotFoundException(list);
		}*/
		if (g.isEmpty()) {
			final List<CodeMessage> list = new ArrayList<>();
			list.add(new CodeMessage("No group found"));
			throw new MerchantNotFoundException(list);
		}

		return g;
	}

	@Override
	public GroupResponse assignDeliveryBoy(Long groupId, Long merchantId, GroupRequest groupRequest) {
		// int m1 = merchantId.intValue();
		groupResponse = new GroupResponse();
		long delveryboyId = Long.valueOf(groupRequest.getDeliveryBoyId());
		List<UserGroupMembersMap> listOfGroupMembers = new ArrayList<UserGroupMembersMap>();
		List<UserGroupMembersMap> s = new ArrayList<UserGroupMembersMap>();
		listOfGroupMembers = groupMapRepository.findByGroupId(groupId);
		List<Long> namesList = new ArrayList<>();

		DeliveryBoyModel d = deliveryBoyRepository.findOne(groupRequest.getDeliveryBoyId());
		// MerchantModel m=merchantRepository.findOne(m1);
		GroupModel g = groupRepository.findOne(groupId);

		if (d == null || g == null || d.getIsDeleted() == inactiveStatus || g.getIsDeleted() == inactiveStatus) {
			final List<CodeMessage> list = new ArrayList<>();
			list.add(new CodeMessage("Invalid credential"));
			throw new BillingException(list);
		}
		
		if(d.getIsAvaliable()==inactiveStatus)
		{
			final List<CodeMessage>list=new ArrayList<>();
			list.add(new CodeMessage("Delivery Boy Is Not Avaliable"));
			throw new BillingException(list);
		}
		for (UserGroupMembersMap u : listOfGroupMembers) {
			namesList.add(u.setDeliverBoyId(delveryboyId));
			groupMapRepository.save(u);
			s.add(u);

		}
		groupResponse.setData1(s);
		groupResponse.setStatus("Sucess");
		return groupResponse;

	}

}
