package com.kouchan.simplebilling.serviceImp;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.google.gson.Gson;
import com.kouchan.simplebilling.model.CustomerModel;
import com.kouchan.simplebilling.model.CustomerOrderDetails;
import com.kouchan.simplebilling.model.InvoiceModel;
import com.kouchan.simplebilling.model.MerchantModel;
import com.kouchan.simplebilling.repository.CustomerOrderDetailsRepo;
import com.kouchan.simplebilling.repository.InvoiceRepo;
import com.kouchan.simplebilling.request.InvoiceVO;
import com.kouchan.simplebilling.request.MonthDatas;
import com.kouchan.simplebilling.service.CustomerOrderDetailsService;
import com.kouchan.simplebilling.service.CustomerService;
import com.kouchan.simplebilling.service.InvoiceService;
import com.kouchan.simplebilling.service.MerchantService;

@Service
public class InvoiceServiceImpl implements InvoiceService {

	@Autowired
	MerchantService merchantService;

	@Autowired
	CustomerService customerService;

	@Autowired
	CustomerOrderDetailsService customerOrderDetailsService;

	@Autowired
	InvoiceRepo invoiceRepo;

	@Autowired
	private CustomerOrderDetailsRepo customerOrderDetailsRepo;

	@Override
	public String generateInvoice(InvoiceVO invoiceVO) throws JsonParseException, JsonMappingException, IOException {

		Gson gson = new Gson();

		MerchantModel mmodel = merchantService.findById(invoiceVO.getMerchantId());

		CustomerModel cmodel = customerService.getByCustomerId(invoiceVO.getCustomerId());

		CustomerOrderDetails customerOrderDetails = customerOrderDetailsService.getCustomerOrderDetails(mmodel.getId(),
				cmodel.getId(), invoiceVO.getProductid(), invoiceVO.getBillMonth());
        String s=customerOrderDetails.getMonthYear();
		TypeReference<List<MonthDatas>> typeRefList = new TypeReference<List<MonthDatas>>() {
		};
	
		
		int totalQuantity = 0;
	    int totalAmount = 0;
		List<MonthDatas> monthDatasList = gson.fromJson(customerOrderDetails.getMonthYear(), typeRefList.getType());
		if (monthDatasList != null /* && monthDatasList.isEmpty() */) {
			for (MonthDatas monthDatas : monthDatasList) {
				if (monthDatas.getStatus().equals("1")) {
					totalQuantity = totalQuantity + Integer.parseInt(monthDatas.getQuantity());
				}
			}
		}
		totalAmount = (int) (totalQuantity * (customerOrderDetails.getRate()));
		InvoiceModel invoiceModel = new InvoiceModel();
		invoiceModel.setMerchantId(mmodel.getId());
		invoiceModel.setMerchantAddress(mmodel.getAddress());
		invoiceModel.setCustomerName(cmodel.getName());
		// invoiceModel.setCustomerNumber((cmodel.getMobileNumber()));
		invoiceModel.setCustomerNumber(cmodel.getMobileNumber());
		invoiceModel.setCustomerId(cmodel.getId());
		invoiceModel.setBillNumber(invoiceModel.getBillId());
		invoiceModel.setBillMonth(invoiceVO.getBillMonth());
		invoiceModel.setProductid(invoiceVO.getProductid());
		invoiceModel.setQuantity(totalQuantity);
		invoiceModel.setRateperqty(customerOrderDetails.getRate());
		invoiceModel.setTotal((double) totalAmount);
		invoiceModel.setTotalAmount(totalAmount);
		invoiceRepo.save(invoiceModel);
		if (invoiceModel.getBillId() > 0) {
			return "Succes";
		}
		return "Failure";
	}

	@Override
	public String udapteDeliveryStatus(InvoiceVO invoiceVO) {
		Gson gson = new Gson();
		String s = " ";
		MerchantModel mmodel = merchantService.findById(invoiceVO.getMerchantId());

		CustomerModel cmodel = customerService.getByCustomerId(invoiceVO.getCustomerId());

		CustomerOrderDetails customerOrderDetails = customerOrderDetailsService.getOrderSchedle(mmodel.getId(),
				cmodel.getId(), invoiceVO.getBillMonth());

		TypeReference<List<MonthDatas>> typeRefList = new TypeReference<List<MonthDatas>>() {
		};

		
		List<MonthDatas> monthDatasList = gson.fromJson(customerOrderDetails.getMonthYear(), typeRefList.getType());
		List<CustomerOrderDetails>c=new ArrayList<CustomerOrderDetails>();
		if (monthDatasList != null /* && monthDatasList.isEmpty() */)
		{
			for (MonthDatas monthDatas : monthDatasList) 
			{
                  //System.out.println(monthDatas);
				if (monthDatas.getDay().equals(invoiceVO.getBillDate())) 
				{
					monthDatas.setStatus("1");
					System.out.println(s.toString());
					c.addAll((Collection<? extends CustomerOrderDetails>) monthDatas);

				}

			}
		}
		System.out.println("c"+c);
	//	customerOrderDetails.setMonthYear(s);
		customerOrderDetailsRepo.save(c);
		if (customerOrderDetails.getOrderId() > 0)
		{
			return "Succes";
		}
		return "Failure";
	}

}
