package com.kouchan.simplebilling.serviceImp;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kouchan.simplebilling.model.MasterTimeSlotModel;
import com.kouchan.simplebilling.repository.MasterTimeSlotRepository;
import com.kouchan.simplebilling.request.MasterTimeSlotRequest;
import com.kouchan.simplebilling.service.MasterTimeSlotService;

@Service
public class MasterTimeSlotServiceImp implements MasterTimeSlotService {

	@Autowired
	private MasterTimeSlotRepository masterTimeSlotRepository;

	public Date stringToDate(final String date) throws ParseException {
		final SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		final Date dateString = formatter.parse(date);
		return dateString;
	}

	@Override
	public MasterTimeSlotModel createMasterSlot(int merchantId, Date startDate,
			MasterTimeSlotRequest masterTimeSlotRequest) {
		MasterTimeSlotModel m = new MasterTimeSlotModel();
		//m.setStartDate((java.sql.Date) startDate);
		m.setMerchantId(merchantId);
		m.setTimeSlot("12-2");
		MasterTimeSlotModel m2 = masterTimeSlotRepository.save(m);
		return m2;
	}
}
