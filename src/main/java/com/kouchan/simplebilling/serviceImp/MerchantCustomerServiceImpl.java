package com.kouchan.simplebilling.serviceImp;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kouchan.simplebilling.coredata.CodeMessage;
import com.kouchan.simplebilling.exception.MerchantNotFoundException;
import com.kouchan.simplebilling.model.CustomerModel;
import com.kouchan.simplebilling.model.MerchantCustomerModel;
import com.kouchan.simplebilling.model.MerchantModel;
import com.kouchan.simplebilling.repository.MerchantCustomerRepository;
import com.kouchan.simplebilling.repository.MerchantRepository;
import com.kouchan.simplebilling.service.MerchantCustomerService;

@Service
public class MerchantCustomerServiceImpl implements MerchantCustomerService {

	@Autowired
	private MerchantCustomerRepository merchantCustomerRepository;
	
	@Autowired
	private MerchantRepository merchantRepository;
	byte inactiveStatus = 1;

	@Override
	public MerchantCustomerModel save(MerchantCustomerModel merchantCustomerModel) {
		return merchantCustomerRepository.save(merchantCustomerModel);
	}

	@Override
	public Set<CustomerModel> getMerchnatDetails(int id) {
		List<MerchantCustomerModel> m = merchantCustomerRepository.findById(id);
		
		MerchantModel merchatExit = merchantRepository.findOne(id);
		if (merchatExit == null || merchatExit.getIsDeleted() == inactiveStatus) {
			final List<CodeMessage> list = new ArrayList<>();
			list.add(new CodeMessage("302", "Merchant Does't Exit"));
			throw new MerchantNotFoundException(list);
		}
		
		List<CustomerModel> listOfGroupMembers = new ArrayList<CustomerModel>();
		for (MerchantCustomerModel u : m) {
			listOfGroupMembers.add(u.getCustomerModel());
		   String l=u.getAddress();
		}
		Set<CustomerModel> s = new LinkedHashSet<CustomerModel>(listOfGroupMembers);  
		return s;
	}

}
