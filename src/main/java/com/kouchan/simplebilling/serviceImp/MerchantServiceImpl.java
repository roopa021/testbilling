package com.kouchan.simplebilling.serviceImp;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kouchan.simplebilling.coredata.CodeMessage;
import com.kouchan.simplebilling.exception.BusinessException;
import com.kouchan.simplebilling.exception.ErrorCodes;
import com.kouchan.simplebilling.exception.MerchantAlreadyExistException;
import com.kouchan.simplebilling.exception.MerchantNotFoundException;
import com.kouchan.simplebilling.model.ChangePasswordModel;
import com.kouchan.simplebilling.model.MerchantModel;
import com.kouchan.simplebilling.repository.MerchantRepository;
import com.kouchan.simplebilling.request.MerchantRegistrationRequest;
import com.kouchan.simplebilling.response.DeleteResponse;
import com.kouchan.simplebilling.service.MerchantService;
import com.kouchan.simplebilling.utils.EncryptDescrypt;

@Service
public class MerchantServiceImpl implements MerchantService {

	@Autowired
	private MerchantRepository merchantRepository;

	/*@Autowired
	private SMSService sMSService;
*/
	DeleteResponse deleteResponse;
	byte activeStatus = 0;
	byte inactiveStatus = 1;

	@Override
	public MerchantModel save(MerchantModel merchant) throws Exception {
		String mobile = merchant.getMobileNumber();
		String password = merchant.getPassword();
		EncryptDescrypt e = new EncryptDescrypt();
		MerchantModel merchantExit = merchantRepository.findByMobile(mobile);
		if (merchantExit != null) {
			final List<CodeMessage> list = new ArrayList<>();
			list.add(new CodeMessage("302", "Merchant alredy Exist with this mobile number",
					merchant.getMobileNumber()));
			throw new MerchantAlreadyExistException(list);
		}
		
		// sMSService.findSmsFormarByName(merchant.getMobileNumber());/* ,
		// merchant.getForgetOTP() */
	//	merchant.setPassword(e.encrypt(password));
		return merchantRepository.save(merchant);
	}

	@Override
	public MerchantModel getByMerchantId(int id) {
		return merchantRepository.findOne(id);
	}

	@Override
	public MerchantModel update12(MerchantModel merchantModel, int id) throws Exception {
		MerchantModel m1 = findById(id);
		EncryptDescrypt e = new EncryptDescrypt();
		if (m1.getIsDeleted() == inactiveStatus) {
			final List<CodeMessage> list = new ArrayList<>();
			list.add(new CodeMessage("302", "Merchant Does't Exit"));
			throw new MerchantNotFoundException(list);
		}
		m1.setName(merchantModel.getName());
		m1.setMobileNumber(merchantModel.getMobileNumber());
		m1.setAadharNumber(merchantModel.getAadharNumber());
		m1.setAddress(merchantModel.getAddress());
		m1.setGstNumber(merchantModel.getGstNumber());
		m1.setPanNumber(merchantModel.getPanNumber());
		m1.setPassword(e.encrypt(merchantModel.getPassword()));
		return merchantRepository.save(m1);
	}

	public MerchantModel findById(int id) {
		return merchantRepository.findOne(id);
	}

	@Override
	public MerchantModel isCustomerExist(String mobileNumber) {
		MerchantModel model = merchantRepository.findByMobile(mobileNumber);
		return model;
	}

	@Override
	public MerchantModel getMerchnatDetails(int merchatId) {
		final MerchantModel m = merchantRepository.findById(merchatId);
		if (m == null) {
			final List<CodeMessage> list = new ArrayList<>();
			list.add(new CodeMessage("302", "Merchant Not Found"));
			throw new MerchantNotFoundException(list);
		}
		return m;
	}

	@Override
	public DeleteResponse deleteMerchant(int id) {
		MerchantModel m = getByMerchantId(id);
		if (m == null || m.getIsDeleted() == inactiveStatus) {
			final List<CodeMessage> list = new ArrayList<>();
			list.add(new CodeMessage("302", "Merchant Not Found"));
			throw new MerchantNotFoundException(list);
		}
		if (String.valueOf(id).equals(String.valueOf(m.getId())) && m.getIsDeleted() == activeStatus) {
			m.setIsDeleted(inactiveStatus);
			merchantRepository.save(m);
		}
		
		return null;
	}

	@Override
	public MerchantModel login(MerchantRegistrationRequest merchantRegrequest) throws Exception {
		MerchantModel merchantlogin = null;
		EncryptDescrypt e = new EncryptDescrypt();
		MerchantModel merchantMob = merchantRepository.findByMobile(merchantRegrequest.getMobileNumber());
		if (merchantMob == null) {
			throw new BusinessException(ErrorCodes.MOBNOTEXIST.name(), ErrorCodes.MOBNOTEXIST.value());
		}
	//	String password = e.decrypt(merchantMob.getPassword());
		String password = (merchantMob.getPassword());
		String enteredPass = merchantRegrequest.getPassword();
	//	String p = e.encrypt(merchantRegrequest.getPassword());
		String p = (merchantRegrequest.getPassword());
		if (password.equals(enteredPass)) {
			merchantlogin = merchantRepository.findByMobileNumberandPassword(merchantRegrequest.getMobileNumber(), p);

		} else {
			throw new BusinessException(ErrorCodes.PASSWORD.name(), ErrorCodes.PASSWORD.value());
		}
		return merchantlogin;

	}

	@Override
	public MerchantModel updatePassword(MerchantModel merchantModel, ChangePasswordModel changePasswordModel)
			throws Exception {
		EncryptDescrypt e = new EncryptDescrypt();
		MerchantModel m1;
		int m2 = changePasswordModel.getId();
		m1 = findById(m2);
	//	m1.setPassword(e.encrypt(changePasswordModel.getNewPassword()));
		m1.setPassword((changePasswordModel.getNewPassword()));
		return merchantRepository.save(m1);
	}

	@Override
	public MerchantModel generateOtp(String mobile) {
		int otp = (int) (Math.random() * 9000) + 1000;
		MerchantModel m=merchantRepository.findByMobile(mobile);
		if (m == null || m.getIsDeleted() == inactiveStatus) 
		{
			final List<CodeMessage> list = new ArrayList<>();
			list.add(new CodeMessage("302", "Merchant Not Found"));
			throw new MerchantNotFoundException(list);
		}
		m.setOtp(otp);
		return merchantRepository.save(m);
	}

	
}
