package com.kouchan.simplebilling.serviceImp;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.activation.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kouchan.simplebilling.data.EmailData;
import com.kouchan.simplebilling.model.EmailTemplateModel;
import com.kouchan.simplebilling.repository.EmailRepsoitory;
import com.kouchan.simplebilling.service.EmailService;
import com.kouchan.simplebilling.service.NotificationEventService;

@Service
public class NotificationEventServiceImp implements NotificationEventService{

	
	 private static final String VISITOR_CODE = "visitor-code";

	
    private static final Logger LOG = Logger.getLogger(NotificationEventServiceImp.class);

    @Autowired
	private EmailService emailService;
    
    @Autowired
    private EmailRepsoitory emailRepo;

    @Override
    public boolean emailGeneratedReport(final DataSource dataSource, final String reportName)  {
       // Validate.notNull(dataSource, "Report file should not empty");
        final EmailData emailData = populateReportEmailData(dataSource, reportName);
     
        try {
            emailService.sendEmailByTemplateCode(emailData);
            LOG.info("Email send successfully");
            return true;
        } catch (final Exception e) {
            LOG.error("Send email failed" + e);
        }
        return false;
    }

    private EmailData populateReportEmailData(final DataSource dataSource, final String reportName) {
        final EmailTemplateModel emailDetailModel = getEmailDetailTemplate(VISITOR_CODE);
        final EmailData emailData = new EmailData();
        emailData.setMailSubject(emailDetailModel.getSubject());
        emailData.setMailTo(emailDetailModel.getTo());
        emailData.setMailFrom(emailDetailModel.getFrom());
        emailData.setMailReplyTo(emailDetailModel.getReplyTo());
        emailData.setMailBody(emailDetailModel.getBody());
        emailData.setMailCc(emailDetailModel.getcC());
        final List<Object> attachments = new ArrayList<>();
        attachments.add(dataSource);
        emailData.setAttachments(attachments);
        emailData.setAttachetMentName(reportName);
        final Map<String, Object> model = new HashMap<String, Object>();
        model.put("reportType", reportName);
        emailData.setModel(model);
        return emailData;

    }

	private EmailTemplateModel getEmailDetailTemplate(String templateCode) {
		return emailRepo.findByTemplateCode(templateCode);
	}

}
