package com.kouchan.simplebilling.serviceImp;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.activation.DataSource;
import javax.mail.util.ByteArrayDataSource;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.owasp.esapi.ESAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
import com.kouchan.simplebilling.model.VisitorModel;
import com.kouchan.simplebilling.repository.VisitorDao;
import com.kouchan.simplebilling.service.NotificationEventService;
import com.kouchan.simplebilling.service.ReportGenService;

@Service
public class ReportGenServiceImp implements ReportGenService {

	@Autowired
	private VisitorDao visitordao;

	@Autowired
	private NotificationEventService notificationEventService;

	public void emailvistorDetials() {
		
		final boolean isForEmailReport = true;
		final String[] headerList = getHeaderListVistorDetailsReport();
		final String fileTitle = "KOU-CHAN VISITOR'S REGISTER ";
		try {
			final DataSource mailData = createPdfReport(null, headerList,
					vistorDataList(visitordao.findAll()), fileTitle, isForEmailReport);
			notificationEventService.emailGeneratedReport(mailData, "KOU-CHAN VISITOR'S REGISTER");
		} catch (DocumentException | IOException e) {
			e.printStackTrace();
		}
	}
	
	
	public void createExcelReport(final HttpServletResponse response, final String[] headerList,
			final List<Object[]> dataList, final String workSheetName) {

		final HSSFWorkbook workBook = new HSSFWorkbook();
		final HSSFSheet workSheet = workBook.createSheet(workSheetName);

		final HSSFFont rowFont = workBook.createFont();
		rowFont.setFontHeightInPoints((short) 11);
		rowFont.setFontName("Calibri");

		// Row Style
		final HSSFCellStyle rowCellStyle = workBook.createCellStyle();
		rowCellStyle.setFont(rowFont);

		final HSSFFont headerFont = workBook.createFont();
		headerFont.setFontHeightInPoints((short) 11);
		headerFont.setFontName("Calibri");

		this.fillExcelHeader(headerList, workBook, workSheet, headerFont);

		// short rowNumber1 = (short) (workSheet.getLastRowNum() + 1);

		final HSSFFont rowFontTableHeaderBold = workBook.createFont();
		rowFontTableHeaderBold.setFontHeightInPoints((short) 11);
		rowFontTableHeaderBold.setFontName("Calibri");
		rowFontTableHeaderBold.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);

		// Header Style
		final HSSFCellStyle headerCellStyleTotal = workBook.createCellStyle();
		headerCellStyleTotal.setFont(headerFont);
		headerCellStyleTotal.setFillForegroundColor(HSSFColor.GREY_50_PERCENT.index);
		headerCellStyleTotal.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

		// Row Style
		final HSSFCellStyle rowCellStyleTotal = workBook.createCellStyle();
		rowCellStyleTotal.setFont(rowFontTableHeaderBold);

		this.fillData(workBook, dataList, workSheet);

		final short roweachTablefooter = (short) (workSheet.getLastRowNum() + 1);
		final HSSFRow eachTablefooter = workSheet.createRow(roweachTablefooter);

		final HSSFFont rowFontTotalFooterBold = workBook.createFont();
		rowFontTotalFooterBold.setFontHeightInPoints((short) 11);
		rowFontTotalFooterBold.setFontName("Calibri");
		rowFontTotalFooterBold.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);

		// Header Style
		final HSSFCellStyle headerCellStyleTotalFooterBold = workBook.createCellStyle();
		headerCellStyleTotalFooterBold.setFont(headerFont);
		headerCellStyleTotalFooterBold.setFillForegroundColor(HSSFColor.GREY_50_PERCENT.index);
		headerCellStyleTotalFooterBold.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

		// Row Style
		final HSSFCellStyle rowCellStyleTotalFooterBold = workBook.createCellStyle();
		rowCellStyleTotalFooterBold.setFont(rowFontTotalFooterBold);

		final HSSFCell rowEachTableCell0 = eachTablefooter.createCell(0);
		rowEachTableCell0.setCellStyle(rowCellStyleTotalFooterBold);

		final HSSFCell rowEachTableCell1 = eachTablefooter.createCell(1);
		rowEachTableCell1.setCellStyle(rowCellStyleTotalFooterBold);

		final HSSFCell rowEachTableCell2 = eachTablefooter.createCell(2);
		rowEachTableCell2.setCellStyle(rowCellStyleTotalFooterBold);

		final HSSFCell rowEachTableCell3 = eachTablefooter.createCell(3);
		rowEachTableCell3.setCellStyle(rowCellStyleTotalFooterBold);
		final short roweachTablefooterSpace = (short) (workSheet.getLastRowNum() + 1);
		final HSSFRow row = workSheet.createRow(roweachTablefooterSpace);

		// For Label
		final HSSFCell nationalIdCell = row.createCell(0);
		nationalIdCell.setCellStyle(rowCellStyle);

		final Map<String, String> fileExportStatusMap = new HashMap<>();
		createExcelFile(workBook, workSheetName, fileExportStatusMap, response);

	}

	
	public DataSource createPdfReport(final HttpServletResponse response, final String[] headerList,
			final List<Object[]> list, final String fileTitle, final boolean isForEmail)
			throws DocumentException, IOException {

		final ByteArrayOutputStream byteArray = new ByteArrayOutputStream();
		final Document pdfDocument = new Document(PageSize.A4);
		final PdfWriter writer = null;
		openPdf(byteArray, pdfDocument, writer);
		final PdfPTable table1 = new PdfPTable(1);
		table1.setWidthPercentage(100);
		table1.addCell(getCell(fileTitle, PdfPCell.ALIGN_CENTER));
		pdfDocument.add(table1);
		final PdfPTable table = new PdfPTable(headerList.length);
		table.setWidthPercentage(100.0f);
		// table.setWidths(new float[] { 3.0f, 5.0f, 3.0f, 2.0f, 3.0f, 2.0f });
		table.setSpacingBefore(10);
		// table.addCell(new Phrase(String.format("Page %d of",
		// writer.getPageNumber()), font));
		// define table header cell
		createPdfCell(headerList, table);
		// write table row data
		writeTableRowdata(table, list);
		pdfDocument.add(table);

		pdfDocument.close();
		return writeAndUploadFile(byteArray, fileTitle, response, isForEmail);
		

	}

	
	public void writeTableRowdata(final PdfPTable table, final List<Object[]> list) {
		for (final Object[] dataRow : list) {
			for (int i = 0; i < dataRow.length; i++) {
				table.addCell(getStringValue(dataRow[i]));
			}
		}
	}

	
	public void createPdfCell(final String[] headerList, final PdfPTable table) {
		final PdfPCell cell = new PdfPCell();
		cell.setBackgroundColor(BaseColor.GRAY);
		cell.setPadding(5);

		final Font font = FontFactory.getFont(FontFactory.HELVETICA);
		font.setColor(BaseColor.WHITE);

		for (final String headerName : headerList) {
			cell.setPhrase(new Phrase(headerName, font));
			table.addCell(cell);
		}
	}

	public DataSource writeAndUploadFile(final ByteArrayOutputStream byteOutStream, final String title,
			final HttpServletResponse response, final boolean isForEmail) throws IOException {
		try {
			final String folderName = null;
			String fileName = null;
			if (title != null) {
				final byte[] bytes = byteOutStream.toByteArray();
				final ByteArrayInputStream in = new ByteArrayInputStream(bytes);
				if (isForEmail) {
					// only prepare dataSorce for mail Attachment
					return new ByteArrayDataSource(bytes, "application/pdf");
				} else {
					response.setContentType("application/pdf");
					response.setHeader("Content-Disposition",
							"attachment;filename=\"" + ESAPI.encoder().encodeForURL(title + ".pdf") + "\"");
					response.setContentType(ESAPI.encoder().encodeForURL("application/pdf"));
					response.setContentLength(bytes.length);
					final ServletOutputStream outputStream = response.getOutputStream();
					writeToOutputStream(in, outputStream);
					outputStream.close();
				}
			} else {
				final byte[] bytes = byteOutStream.toByteArray();
				final ByteArrayInputStream in = new ByteArrayInputStream(bytes);
				response.setContentType("application/pdf");
				final String headerKey = "Content-Disposition";
				final String headerValue = String.format("attachment; filename=\"%s\"", "test");
				response.setHeader(headerKey, headerValue);
				fileName = String.format("%s/%s.pdf", folderName, "test");
				response.setContentType(ESAPI.encoder().encodeForURL("application/pdf"));
				response.setContentLength(bytes.length);
				writeToOutputStream(in, response.getOutputStream());
				writeFileAsStream(response, fileName, folderName);
			}

		} catch (final Exception e) {
			e.printStackTrace();
		} finally {
			try {
				byteOutStream.flush();
				byteOutStream.close();
			} catch (final Exception e) {

			}
		}
		return null;
	}
	
	public void openPdf(final ByteArrayOutputStream byteArray, final Document pdfDocument, PdfWriter writer) {
		try {
			writer = PdfWriter.getInstance(pdfDocument, byteArray);
			final HeaderFooterPageEventForPdf event = new ReportGenServiceImp.HeaderFooterPageEventForPdf();
			writer.setPageEvent(event);
			pdfDocument.open();
		} catch (final DocumentException ex) {
			ex.printStackTrace();
		} catch (final Exception ex) {
			ex.printStackTrace();
		}
	}

	
	public PdfPCell getCell(final String text, final int alignment) {
		final PdfPCell cell = new PdfPCell(new Phrase(text));
		cell.setPadding(0);
		cell.setHorizontalAlignment(alignment);
		cell.setBorder(PdfPCell.NO_BORDER);
		return cell;
	}

	/**
	 * Fill excel header.
	 *
	 * @param headerList
	 *            the header list
	 * @param workBook
	 *            the work book
	 * @param workSheet
	 *            the work sheet
	 * @param headerFont
	 *            the header font
	 */
	private void fillExcelHeader(final String[] headerList, final HSSFWorkbook workBook, final HSSFSheet workSheet,
			final HSSFFont headerFont) {

		// Header Style
		final HSSFCellStyle headerCellStyle = workBook.createCellStyle();
		headerCellStyle.setFont(headerFont);
		headerCellStyle.setFillForegroundColor(HSSFColor.GREY_50_PERCENT.index);
		headerCellStyle.setFillPattern(headerCellStyle.SOLID_FOREGROUND);

		final HSSFFont rowFontTableHeaderBold1 = workBook.createFont();
		rowFontTableHeaderBold1.setFontHeightInPoints((short) 11);
		rowFontTableHeaderBold1.setFontName("Calibri");
		rowFontTableHeaderBold1.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		final HSSFCellStyle rowCellStyleTotal1 = workBook.createCellStyle();
		rowCellStyleTotal1.setFont(rowFontTableHeaderBold1);
		final HSSFRow row1 = workSheet.createRow(0);
		final HSSFCell nationalIdCell1 = row1.createCell(0);
		nationalIdCell1.setCellStyle(rowCellStyleTotal1);

		// Header
		final HSSFRow headerRow = workSheet.createRow(1);
		HSSFCell headerCell = null;

		for (int i = 0; i < headerList.length; i++) {
			headerCell = headerRow.createCell(i);
			headerCell.setCellValue(headerList[i]);
			final HSSFFont rowFontTableHeaderBold = workBook.createFont();
			rowFontTableHeaderBold.setFontHeightInPoints((short) 11);
			rowFontTableHeaderBold.setFontName("Calibri");
			rowFontTableHeaderBold.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);

			// Header Style
			final HSSFCellStyle headerCellStyleTableHeader = workBook.createCellStyle();
			headerCellStyleTableHeader.setFont(headerFont);
			headerCellStyleTableHeader.setFillForegroundColor(HSSFColor.GREY_50_PERCENT.index);
			headerCellStyleTableHeader.setFillPattern(headerCellStyle.SOLID_FOREGROUND);

			// Row Style
			final HSSFCellStyle rowCellStyleTotal = workBook.createCellStyle();
			rowCellStyleTotal.setFont(rowFontTableHeaderBold);
			headerCell.setCellStyle(rowCellStyleTotal);
		}
	}

	
	private void createExcelFile(final HSSFWorkbook workBook, final String fileName,
			final Map<String, String> fileExportStatusMap, final HttpServletResponse response) {
		String folderName = null;
		FileOutputStream fileOut = null;
		try {

			folderName = createFolder();
			response.setContentType("application/xls");
			final String headerKey = "Content-Disposition";
			final String headerValue = String.format("attachment; filename=\"%s.xls\"", fileName);
			response.setHeader(headerKey, headerValue);
			final String xlsxFileName = String.format("%s/%s.xls", folderName, fileName);
			fileOut = new FileOutputStream(xlsxFileName);
			workBook.write(fileOut);
			fileOut.flush();
			fileOut.close();
			fileExportStatusMap.put(fileName + ".xls", "200");
			writeFileAsStream(response, xlsxFileName, folderName);
		} catch (final Exception ex) {
			fileExportStatusMap.put(fileName + ".xls", "400" + " " + ex.getMessage());
		}
	}

	
	public String createFolder() throws Exception {
		final String folderName = String.format("%s/%s", getRepoFolder(), "DOCSEXPORT");
		final File file = new File(folderName);
		if (!file.isDirectory()) {
			final boolean folderCreated = file.mkdir();
			if (!folderCreated) {
				throw new Exception(String.format("%s - Folder creation failed", folderName));
			}
		}
		return folderName;
	}

	
	protected String getRepoFolder() {
		return "C:/";
	}

	
	private void deleteDirectory(final String folderPath) {
		if ((folderPath != null && !folderPath.isEmpty())) {
			final File directory = new File(folderPath);
			final File[] files = directory.listFiles();
			for (int index = 0; index < files.length; ++index) {
				files[index].delete();
			}
			directory.delete();
		}
	}

	
	public void writeFileAsStream(final HttpServletResponse response, final String fileName, final String folderName) {
		OutputStream out = null;
		InputStream in = null;
		try {
			if (fileName.contains(".pdf")) {
				out = response.getOutputStream();
				if (fileName != null && !fileName.isEmpty()) {
					final File file = new File(fileName);
					in = new FileInputStream(file);

					response.setHeader("Content-Disposition",
							"attachment;filename=\"" + ESAPI.encoder().encodeForURL(file.getName()) + "\"");
					response.setContentType(ESAPI.encoder().encodeForURL("application/pdf"));
					response.setContentLength((int) file.length());
					writeToOutputStream(in, out);
				}
			} else {
				out = response.getOutputStream();
				if (fileName != null && !fileName.isEmpty()) {
					final File file = new File(fileName);
					in = new FileInputStream(file);
					response.setHeader("Content-Disposition",
							"attachment;filename=\"" + ESAPI.encoder().encodeForURL(file.getName()) + "\"");
					response.setContentType(ESAPI.encoder().encodeForURL("application/xls"));
					response.setContentLength((int) file.length());
					writeToOutputStream(in, out);
					// response.reset();

				}
			}
		} catch (final Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (out != null) {
					out.close();
				}
				deleteDirectory(folderName);
			} catch (final Exception e) {
				e.printStackTrace();
			}
		}
	}

	
	private void writeToOutputStream(final InputStream inputStream, final OutputStream out) throws IOException {
		final byte[] buf = new byte[1024];
		int count = 0;
		while ((count = inputStream.read(buf)) >= 0) {
			out.write(buf, 0, count);
		}
	}

	
	private void fillData(final HSSFWorkbook workBook, final List<Object[]> dataList, final HSSFSheet workSheet) {
		for (final Object[] dataRow : dataList) {
			final short rowNumber = (short) (workSheet.getLastRowNum() + 1);
			final HSSFRow row = workSheet.createRow(rowNumber);
			for (int i = 0; i < dataRow.length; i++) {
				final HSSFCell nationalIdCell = row.createCell(i);
				nationalIdCell.setCellStyle(getCellStyle(dataRow[i], workBook));
				nationalIdCell.setCellValue(getStringValue(dataRow[i]));
			}
		}
	}

	
	private String getStringValue(final Object object) {
		String value = "";
		if (object == null) {
			return value;
		} else if (object instanceof Date) {
			final Date date = (Date) object;
			value = new SimpleDateFormat("dd-MM-yyyy").format(date);
		} else {
			value = object.toString();
		}
		return value;
	}

	
	private HSSFCellStyle getCellStyle(final Object object, final HSSFWorkbook workBook) {
		HSSFCellStyle cellStyle = null;
		if (object == null) {
			return cellStyle;
		} else if (object instanceof Date) {
			cellStyle = workBook.createCellStyle();
			final CreationHelper createHelper = workBook.getCreationHelper();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("yyyy/MM/dd"));
		} else {
			cellStyle = workBook.createCellStyle();
			final HSSFFont rowFont = workBook.createFont();
			rowFont.setFontHeightInPoints((short) 11);
			rowFont.setFontName("Calibri");
			cellStyle.setFont(rowFont);
		}
		return cellStyle;
	}

	static class HeaderFooterPageEventForPdf extends PdfPageEventHelper {

		
		@Override
		public void onStartPage(final PdfWriter writer, final Document document) {

		}

		
		@Override
		public void onEndPage(final PdfWriter writer, final Document document) {
			final Rectangle page = document.getPageSize();
			final PdfPTable foot = new PdfPTable(1);
			final Phrase p = new Phrase();
			final PdfPCell footCell = new PdfPCell(p);
			footCell.setBorder(0);
			footCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			foot.addCell(footCell);
			foot.setTotalWidth(page.getWidth());
		}
	}

	

	
	
	
	private String[] getHeaderListVistorDetailsReport() {
		final String[] headerList = { "Mobile Number", " Name", "To Meet",
				"Purpose ","Visit Date","Visit Time" };
		return headerList;
	}

	public List<Object[]> vistorDataList(final List<VisitorModel> allVistor) {
		final List<Object[]> dataList = new ArrayList<>();
		for (final VisitorModel vistor : allVistor) {
			
			final Object[] rowDataArray = { vistor.getMobileNumber(), vistor.getName(),vistor.getToMeet(), vistor.getPurpose(),vistor.getVisitDate(),vistor.getVisitTime()};
			dataList.add(rowDataArray);
		}
		return dataList;
	}

	
	public static List<Date> getDatesBetweenDates(Date startDate, Date endDate) {
		List<Date> datesInRange = new ArrayList<>();
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(startDate);
		Calendar endCalendar = new GregorianCalendar();
		endCalendar.setTime(endDate);
		while (calendar.before(endCalendar)) {
			Date result = calendar.getTime();
			datesInRange.add(result);
			calendar.add(Calendar.DATE, 1);
		}
		return datesInRange;
	}

	public Map<String, Date> getWeekStartEndDate() {
		final Map<String, Date> weekDifference = new HashMap<>();
		final Calendar c = Calendar.getInstance();
		c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		final Date startDate = c.getTime();
		c.add(Calendar.DATE, 6);
		final Date endDate = c.getTime();
		weekDifference.put("startDate", startDate);
		weekDifference.put("endDate", endDate);
		return weekDifference;
	}
}
