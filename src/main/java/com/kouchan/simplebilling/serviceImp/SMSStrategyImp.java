package com.kouchan.simplebilling.serviceImp;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.kouchan.simplebilling.service.SMSStrategy;

@Component(value = "defaultAlertSolutionSmsStrategy")
public class SMSStrategyImp implements SMSStrategy {

	@Autowired
	Environment env;

	@Override
	public void sendSms(final String sms) {
		final HttpHeaders registerMessageRequest = new HttpHeaders();
		registerMessageRequest.setContentType(MediaType.APPLICATION_JSON);

		final HashMap<String, String> baseSMS = new HashMap<>();
		baseSMS.put("apikey", env.getProperty("alertsolutions.apiKey"));
		baseSMS.put("senderID", env.getProperty("alertsolutions.senderId"));
		baseSMS.put("method", "sms.json");
		baseSMS.put("json", sms);
		final RestTemplate sendUser = new RestTemplate();

		String s = sendUser.getForObject(env.getProperty("alertsolutions.uri"), String.class, baseSMS);
		System.out.println("sms imp" + s.toString());
	}

}
