package com.kouchan.simplebilling.serviceImp;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.kouchan.simplebilling.alertsolutions.messages.Sms;
import com.kouchan.simplebilling.alertsolutions.messages.SmsData;
import com.kouchan.simplebilling.constant.SMSCONSTANTS;
import com.kouchan.simplebilling.model.SmsModel;
import com.kouchan.simplebilling.repository.SmsRepository;
import com.kouchan.simplebilling.service.SMSStrategy;
import com.kouchan.simplebilling.service.SmsService;

@Service
public class SmsServiceImp implements SmsService {

	@Autowired
	@Qualifier(value = "defaultAlertSolutionSmsStrategy")
	SMSStrategy smsStrategy;

	@Autowired
	Environment env;

	@Autowired
	private SmsRepository smsRepository;

	@Override
	public void sendOtpToVisitor(String mobileNumber, int otp) {
		String message = findSmsFormarByName(SMSCONSTANTS.REGISTRATION_OTP);
		message = String.format(message, otp);
		final Sms sms = new Sms();
		final List<Sms> listOfSms = new ArrayList<>();
		sms.setTo(mobileNumber);
		sms.setSender(env.getProperty("alertsolutions.senderId"));
		sms.setMessage(message);
		listOfSms.add(sms);
		final SmsData smsData = new SmsData();
		smsData.setSms(listOfSms);
		final Gson gson = new Gson();
		final String json = gson.toJson(smsData);
		smsStrategy.sendSms(json);

	}

	private String findSmsFormarByName(String name) {
		final SmsModel sms = smsRepository.findByTemplateCode(name);
		return sms.getFormat();
	}

	@Override
	public void sendSucessMsg(String mobile) {
		String message = findSmsFormarByName(SMSCONSTANTS.VISIT_MSG);
		message = String.format(message);
		final Sms sms = new Sms();
		final List<Sms> listOfSms = new ArrayList<>();
		sms.setTo(mobile);
		sms.setSender(env.getProperty("alertsolutions.senderId"));
		sms.setMessage(message);
		listOfSms.add(sms);
		final SmsData smsData = new SmsData();
		smsData.setSms(listOfSms);
		final Gson gson = new Gson();
		final String json = gson.toJson(smsData);
		smsStrategy.sendSms(json);
		
	}

}
