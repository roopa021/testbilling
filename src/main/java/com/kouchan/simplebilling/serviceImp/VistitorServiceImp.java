package com.kouchan.simplebilling.serviceImp;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kouchan.simplebilling.model.VisitorModel;
import com.kouchan.simplebilling.model.VisitorOtpModel;
import com.kouchan.simplebilling.repository.VisitorDao;
import com.kouchan.simplebilling.repository.VisitorOtpDao;
import com.kouchan.simplebilling.request.VistorRequest;
import com.kouchan.simplebilling.service.SmsService;
import com.kouchan.simplebilling.service.VistitorService;

@Service
public class VistitorServiceImp implements VistitorService {

	@Autowired
	private VisitorDao visitorServicedao;

	@Autowired
	private SmsService smsService;

	@Autowired
	private VisitorOtpDao visitorOtpDao;

	@Override
	public VisitorModel save(VisitorModel visitorModel) {
		int otp = (int) (Math.random() * 9000) + 1000;
		String name = visitorModel.getName();

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		LocalDate date = LocalDate.now();
		String visitDate = (dtf.format(date)).toString();

		DateTimeFormatter dt = DateTimeFormatter.ofPattern("HH:mm:ss");
		LocalTime time = LocalTime.now();
		String visitTime = dt.format(time).toString();

		visitorModel.setOtp(otp);
		visitorModel.setName(name);
		visitorModel.setVisitDate(visitDate);
		visitorModel.setVisitTime(visitTime);

		/*SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss"); 
		Date date = new Date();
	    System.out.println(formatter.format(date));
		visitorModel.setOtp(otp);
		visitorModel.setName(name);
		visitorModel.setVisitDate(date.toString());
	*/
		
		
		VisitorModel v1 = visitorServicedao.save(visitorModel);
		System.out.println("---" + visitorModel.toString());
		 smsService.sendOtpToVisitor(visitorModel.getMobileNumber(), otp);

		return v1;
	}

	@Override
	public VisitorModel validateOtp(VistorRequest vistorRequest) {
		VisitorModel v = visitorServicedao.findByMobileAndOTP(vistorRequest.getMobileNumber(), vistorRequest.getOtp());

		return v;
	}

	@Override
	public VisitorModel validateOtp(String mobileNumber, int otp) {
		// TODO Auto-generated method stub
		return visitorServicedao.findByMobileAndOTP(mobileNumber, otp);
	}

	@Override
	public VisitorOtpModel saveOtp(VisitorOtpModel visitorOtpModel) {
		int otp = (int) (Math.random() * 9000) + 1000;
		visitorOtpModel.setOtp(otp);
		VisitorOtpModel v1 = visitorOtpDao.save(visitorOtpModel);
		smsService.sendOtpToVisitor(visitorOtpModel.getMobileNumber(), otp);

		return v1;

	}

}
