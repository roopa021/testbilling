package com.kouchan.simplebilling.utils;

import com.kouchan.simplebilling.response.CommonResponse;
import com.kouchan.simplebilling.response.Error;

public class RestUtils {

	public static CommonResponse wrapObjectForSuccess(Object object) {
		CommonResponse response = new CommonResponse();
		response.setStatus("Success");
		response.setResultObject(object);
		return response;
	}

	public static CommonResponse wrapObjectForFailure(Object object, String errorCode, String errorMsg) {
		CommonResponse response = new CommonResponse();
		response.setResultObject(object);
		response.setStatus("Failure");
		Error error = new Error();
		error.setCode(errorCode);
		error.setMessage(errorMsg);
		response.setError(error);
		return response;
	}
	

	public static CommonResponse wrapObjectForOTP(Object object, String errorCode, String errorMsg) {
		CommonResponse response = new CommonResponse();
		response.setResultObject(object);
		response.setStatus("sucess");
		/*Error error = new Error();
		error.setCode(errorCode);
		error.setMessage(errorMsg);
		response.setError(error);*/
		return response;
	}

	public static CommonResponse loginSuccess(String msg) {
		CommonResponse response = new CommonResponse();
		response.setStatus(msg);
		return response;
	}
	
	
}
