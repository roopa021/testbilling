package com.kouchan.simplebilling.validator;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


import com.kouchan.simplebilling.request.MerchantRegistrationRequest;

@Component("mechantRegisterRequestValidator")
public class MechantRegisterRequestValidator implements Validator {

	@Override
	public boolean supports(Class<?> arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void validate(Object arg0, Errors errors) {
	    final MerchantRegistrationRequest merchantRegistrationRequest = (MerchantRegistrationRequest) arg0;
        final String mobile = merchantRegistrationRequest.getMobileNumber();
        final String name = merchantRegistrationRequest.getName();
        final String aadharNumber = merchantRegistrationRequest.getAadharNumber();
        final String password = merchantRegistrationRequest.getPassword();

        if (StringUtils.isEmpty(mobile)) {
            errors.rejectValue("mobile", "105", "Mobile Number Should Not Be Blank");
        }
        
        if (StringUtils.isEmpty(name)) {

            errors.rejectValue("Name", "106", "Name Should Not Be Blank");
        }

        if (StringUtils.isEmpty(password)) {

            errors.rejectValue("password", "error.code.password.notNull", "validation.password.notNull");

        }/* else if (StringUtils.length(password) < 6 || StringUtils.length(password) > 255) {

            errors.rejectValue("password", "error.code.password.length", "validation.password.length");
        }*/
        if(StringUtils.isEmpty(aadharNumber))
        {
        	errors.rejectValue("aadharNumber","107","Aadhar number Should Not Be Blank");
        }
    }		
	}


